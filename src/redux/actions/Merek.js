import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_MEREK } from "./index";

// GET SATUAN
export const getMerek = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/merek`);
    dispatch({ type: GET_MEREK, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

// create satuan
export const tambahMerek = (data) => async (dispatch) => {
  try {
    const add = await axios.post(`${BASE_URL}/merek`, data);
    dispatch(getMerek());
    ToastSuccess.fire({
      icon: "success",
      title: "Merek berhasil ditambahkan",
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateMerek = (id, merek) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/merek/${id}`, { merek });
    dispatch(getMerek());
    ToastSuccess.fire({
      icon: "success",
      title: "Merek berhasil dirubah",
    });
  } catch (error) {
    console.log(error);
  }
};

export const setActiveMerek = (id, status) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/merek/status/${id}`, {
      status,
    });
    dispatch(getMerek());
    if (status === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Merek Active",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Merek Non Active",
      });
    }
  } catch (error) {
    console.log(error);
  }
};
