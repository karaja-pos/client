import axios from "axios";
import jwt from "jsonwebtoken";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_ALL_TOKEN, GET_USERS, USER_LOGIN, USER_LOGOUT } from "./index";

export const checkUser = (token) => (dispatch) => {
  const decode = jwt.verify(token, "7qvt6t2738");
  dispatch({ type: USER_LOGIN, data: decode });
};

export const userLogin = (username, password) => async (dispatch) => {
  try {
    const login = await axios.post(`${BASE_URL}/users/login`, {
      username,
      password,
    });

    if (login.data.status == "error") {
      ToastSuccess.fire({
        icon: "error",
        title: login.data.data,
      });
    } else {
      localStorage.setItem("session", login.data.data);
      dispatch(checkUser(login.data.data));
    }
  } catch (error) {
    console.log(error);
  }
};

export const userLogOut = () => (dispatch) => {
  try {
    localStorage.removeItem("session");
    dispatch({ type: USER_LOGOUT });
    document.location.reload();
  } catch (error) {
    console.log(error);
  }
};

export const getAllUsers = () => async (dispatch) => {
  try {
    const user = await axios.get(`${BASE_URL}/users`);
    dispatch({ type: GET_USERS, data: user.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const getAllToken = () => async (dispatch) => {
  try {
    const user = await axios.get(`${BASE_URL}/token/all/data`);
    dispatch({ type: GET_ALL_TOKEN, data: user.data.data });
  } catch (error) {
    console.log(error);
  }
};
