import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { CART_TOTAL, CLEAR_ORDER, GET_BARANG_KASIR } from "./index";

export const getBarangToko = (id_toko) => async (dispatch) => {
  try {
    const items = await axios.get(`${BASE_URL}/kasir/${id_toko}`);
    dispatch({ type: GET_BARANG_KASIR, data: items.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const transaksiPenjualan = (data) => async (dispatch) => {
  try {
    console.log(data);
    const trx = await axios.post(`${BASE_URL}/kasir`, data);
    console.log(trx);
    ToastSuccess.fire({
      icon: "success",
      title: "Transaksi penjualan berhasil",
    });
    dispatch({ type: CLEAR_ORDER });
    dispatch({ type: CART_TOTAL });
  } catch (error) {
    console.log(error);
  }
};
