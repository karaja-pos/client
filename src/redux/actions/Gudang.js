import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import {
  GET_FAKTUR_PEMBELIAN,
  GET_FAKTUR_BYNOMOR,
  GET_PENGIRIMAN,
  GET_ITEMS_PENGIRIMAN,
} from "./index";

// PEMBELIAN
export const getFakturPembelian = () => async (dispatch) => {
  try {
    const faktur = await axios.get(`${BASE_URL}/gudang`);
    dispatch({ type: GET_FAKTUR_PEMBELIAN, data: faktur.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const getFakturByNomor = (nomor) => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/gudang/faktur/${nomor}`);
    dispatch({ type: GET_FAKTUR_BYNOMOR, data: data.data.data });
    dispatch(getFakturPembelian());
  } catch (error) {
    console.log(error);
  }
};

export const simpanFakturPembelian = (data, data1) => async (dispatch) => {
  try {
    const simpan = await axios.post(`${BASE_URL}/gudang/faktur`, {
      faktur: data,
      pembelian: data1,
    });
    dispatch(getFakturPembelian());
  } catch (error) {
    console.log(error);
  }
};

export const hapusFakturById = (no_faktur) => async (dispatch) => {
  try {
    const del = await axios.delete(`${BASE_URL}/gudang/faktur/${no_faktur}`);
    ToastSuccess.fire({
      icon: "success",
      title: `Nomor Faktur ${no_faktur} berhasil dihapus`,
    });
    dispatch(getFakturPembelian());
  } catch (error) {
    console.log(error);
  }
};

// PENGIRIMAN

export const getPengiriman = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/gudang/pengiriman`);
    dispatch({ type: GET_PENGIRIMAN, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const getHistoryPengiriman =
  (id_toko, tgl_start, tgl_end, jenis) => async (dispatch) => {
    try {
      const data = await axios.get(
        `${BASE_URL}/gudang/pengiriman/history?id_toko=${id_toko}&tgl_start=${tgl_start}&tgl_end=${tgl_end}&jenis=${jenis}`
      );
      dispatch({ type: GET_PENGIRIMAN, data: data.data.data });
    } catch (error) {
      console.log(error);
    }
  };

export const getPengirimanByToko = (id_toko) => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/gudang/pengiriman/${id_toko}`);
    dispatch({ type: GET_PENGIRIMAN, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const kirimBarang = (data, barang) => async (dispatch) => {
  try {
    const kirim = await axios.post(`${BASE_URL}/gudang/pengiriman`, {
      data,
      barang,
    });
    ToastSuccess.fire({
      icon: "success",
      title: `Barang dalam pengiriman`,
    });
    dispatch(getPengiriman());
  } catch (error) {
    console.log(error);
  }
};

export const getListItemPengiriman =
  (no_pengiriman, done) => async (dispatch) => {
    try {
      const item = await axios.get(
        `${BASE_URL}/gudang/pengiriman/items/${no_pengiriman}`
      );
      dispatch({ type: GET_ITEMS_PENGIRIMAN, data: item.data.data });
      done(false, item.data.data);
    } catch (error) {
      done(true, []);
      console.log(error);
    }
  };

// export const getListItemPengirimanStatus = (no_pengiriman, done) => async (
//   dispatch
// ) => {
//   try {
//     const item = await axios.get(
//       `${BASE_URL}/gudang/pengiriman/status/${no_pengiriman}`
//     );
//     done(false, item.data.data);
//     // dispatch({ type: GET_ITEMS_PENGIRIMAN, data: item.data.data });
//   } catch (error) {
//     done(true, []);
//     console.log(error);
//   }
// };

export const terimaPengirimanByToko = (id, data) => async (dispatch) => {
  try {
    const terima = await axios.put(
      `${BASE_URL}/gudang/pengiriman/toko/${id}`,
      data
    );

    if (terima.data.status !== "Updated") {
      ToastSuccess.fire({
        icon: "error",
        title: `${data.nama_brg} belum di mapping di tokon ini`,
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: `${data.nama_brg} sudah diterima`,
      });
    }

    dispatch(getPengiriman());
    dispatch(
      getListItemPengiriman(data.no_pengiriman, () => console.log("Success"))
    );
  } catch (error) {
    console.log(error);
  }
};

export const hapusPengiriman = (id) => async (dispatch) => {
  try {
    const del = await axios.delete(`${BASE_URL}/gudang/pengiriman/${id}`);
    dispatch(getPengiriman());
  } catch (error) {
    console.log(error);
  }
};

export const cetakPengiriman = (no_pengiriman, done) => async (dispatch) => {
  try {
    const cetak = await axios.get(
      `${BASE_URL}/gudang/pengiriman/cetak/${no_pengiriman}`
    );
    done(false, cetak.data.data);
  } catch (error) {
    console.log(error);
  }
};
