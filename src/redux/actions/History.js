import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_HISTORY_PENJUALAN } from ".";

export const getHistoryPenjualan =
  (id_toko, tgl_start, tgl_end, jenis) => async (dispatch) => {
    try {
      const get = await axios.get(
        `${BASE_URL}/history/transaksi?id_toko=${id_toko}&tgl_start=${tgl_start}&tgl_end=${tgl_end}&jenis=${jenis}`
      );
      if (get.data.status === "Success") {
        dispatch({ type: GET_HISTORY_PENJUALAN, data: get.data.data });
      }
    } catch (error) {
      console.log(error);
    }
  };
