import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_SATUAN } from "./index";

// GET SATUAN
export const getSatuan = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/satuan`);
    dispatch({ type: GET_SATUAN, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

// create satuan
export const tambahSatuan = (data) => async (dispatch) => {
  try {
    const add = await axios.post(`${BASE_URL}/satuan`, data);
    dispatch(getSatuan());
    ToastSuccess.fire({
      icon: "success",
      title: "Satuan berhasil ditambahkan",
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateSatuan = (id, nama_satuan) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/satuan/${id}`, { nama_satuan });
    dispatch(getSatuan());
    ToastSuccess.fire({
      icon: "success",
      title: "Satuan berhasil dirubah",
    });
  } catch (error) {
    console.log(error);
  }
};

export const setActiveSatuan = (id, status) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/satuan/status/${id}`, {
      status,
    });
    dispatch(getSatuan());
    if (status === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Satuan Active",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Satuan Non Active",
      });
    }
  } catch (error) {
    console.log(error);
  }
};
