import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_KREDITUR } from "./index";

export const getKreditur = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/kreditur`);
    dispatch({ type: GET_KREDITUR, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const simpanKreditur = (data) => async (dispatch) => {
  try {
    const addToko = await axios.post(`${BASE_URL}/kreditur`, data);
    if (addToko.data.code == 201) {
      dispatch(getKreditur());
      ToastSuccess.fire({
        icon: "success",
        title: "Kreditur berhasil disimpan",
      });
    } else {
      ToastSuccess.fire({
        icon: "Error",
        title: "Kreditur gagal disimpan",
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const updateKrediturById = (data, id) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/kreditur/${id}`, data);
    ToastSuccess.fire({
      icon: "success",
      title: "Kreditur berhasil dirubah!",
    });
    dispatch(getKreditur());
  } catch (error) {
    console.log(error);
  }
};

export const updateStatusKreditur = (id, status) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/kreditur/status/${id}`, {
      status,
    });
    if (status === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Kreditur dibuka",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Kreditur ditutup",
      });
    }

    dispatch(getKreditur());
  } catch (error) {
    console.log(error);
  }
};
