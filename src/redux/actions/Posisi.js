import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_POSISI } from "./index";

export const getPosisi = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/posisi`);
    if (data.data.code) {
      dispatch({ type: GET_POSISI, data: data.data.data });
    }
  } catch (error) {
    console.log(error);
  }
};

export const tambahPoisi = (data) => async (dispatch) => {
  try {
    const tambah = await axios.post(`${BASE_URL}/posisi`, data);
    if (tambah.data.code === 201) {
      dispatch(getPosisi());
      ToastSuccess.fire({
        icon: "success",
        title: "Posisi berhasil ditambahkan",
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const blokirPosisi = (id, status) => async (dispatch) => {
  try {
    const blokir = await axios.put(`${BASE_URL}/posisi/blokir/${id}`, {
      blokir: status,
    });

    console.log(blokir);
    dispatch(getPosisi());
    if (status === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Blokir",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Buka Blokir",
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const updatePosisi = (id, data) => async (dispatch) => {
  try {
    await axios.put(`${BASE_URL}/posisi/${id}`, data);
    dispatch(getPosisi());
    ToastSuccess.fire({
      icon: "success",
      title: "Update berhasil",
    });
  } catch (error) {
    console.log(error);
  }
};
