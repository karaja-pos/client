import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import {
  GET_BARANG,
  GET_STOK_TOKO,
  GET_BARANG_TOKO,
  SET_SELECTED_BARANG,
  SET_ID_BARANG,
} from "./index";

// axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`
// GET SATUAN
export const getBarang = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/barang`);
    dispatch({ type: GET_BARANG, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const getBarangToko = (id) => async (dispatch) => {
  try {
    const barang = await axios.get(`${BASE_URL}/barang/barang-toko/${id}`);
    dispatch({ type: GET_BARANG_TOKO, data: barang.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const getStokToko = (id) => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/barang/toko/${id}`);
    dispatch({ type: GET_STOK_TOKO, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

// create barang
export const tambahBarang = (data) => async (dispatch) => {
  try {
    console.log(data);
    const add = await axios.post(`${BASE_URL}/barang`, data);
    console.log(add.data);
    dispatch(getBarang());
    ToastSuccess.fire({
      icon: "success",
      title: "Barang berhasil ditambahkan",
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateBarang = (id, data) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/barang/${id}`, data, {
      headers: { "content-type": "multipart/form-data" },
    });
    dispatch(getBarang());
    ToastSuccess.fire({
      icon: "success",
      title: "Barang berhasil dirubah",
    });
  } catch (error) {
    console.log(error);
  }
};

export const insertBarangToToko = (id, data) => async (dispatch) => {
  try {
    const addData = data.map((item) => {
      return {
        id_barang: item.id,
        id_toko: id,
        user: "",
      };
    });
    const add = await axios.post(`${BASE_URL}/barang/to-toko/${id}`, {
      data: addData,
    });
    dispatch(addSelectedBarang([]));
    dispatch(getBarangToko(id));
    dispatch(getStokToko(id));
  } catch (error) {
    console.log(error);
  }
};

export const setTotalHarga = (id, id_toko, harga) => async (dispatch) => {
  try {
    const setHarga = await axios.put(`${BASE_URL}/barang/harga-jual/${id}`, {
      harga,
    });
    dispatch(addSelectedBarang([]));
    dispatch(getBarangToko(id_toko));
    dispatch(getStokToko(id_toko));
  } catch (error) {
    console.log(error);
  }
};

export const setStokAkhir = (id, id_toko, stok_akhir) => async (dispatch) => {
  try {
    const setStok = await axios.put(`${BASE_URL}/barang/stok-akhir/${id}`, {
      stok_akhir,
    });
    dispatch(addSelectedBarang([]));
    dispatch(getBarangToko(id_toko));
    dispatch(getStokToko(id_toko));
  } catch (error) {
    console.log(error);
  }
};

export const setDiskonBarang = (id, id_toko, diskon) => async (dispatch) => {
  try {
    const setStok = await axios.put(`${BASE_URL}/barang/diskon/${id}`, {
      diskon,
    });
    dispatch(addSelectedBarang([]));
    dispatch(getBarangToko(id_toko));
    dispatch(getStokToko(id_toko));
  } catch (error) {
    console.log(error);
  }
};

export const addSelectedBarang = (data) => (dispatch) => {
  dispatch({ type: SET_SELECTED_BARANG, data });
};

export const setBarangId = (id) => (dispatch) => {
  dispatch({ type: SET_ID_BARANG, data: id });
};

//   export const setActiveMerek = (id, status) => async (dispatch) => {
//     try {
//       const update = await axios.put(`${BASE_URL}/merek/status/${id}`, {
//         status,
//       });
//       dispatch(getMerek());
//       if (status === "Y") {
//         ToastSuccess.fire({
//           icon: "success",
//           title: "Merek Active",
//         });
//       } else {
//         ToastSuccess.fire({
//           icon: "success",
//           title: "Merek Non Active",
//         });
//       }
//     } catch (error) {
//       console.log(error);
//     }
//   };
