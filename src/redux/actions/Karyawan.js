import axios from "axios";
import { data } from "jquery";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_KARYAWAN, GET_KARYAWAN_BY_TOKO, karyawan_toko } from "./index";

// GET SATUAN
export const getKaryawan = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/karyawan`);
    dispatch({ type: GET_KARYAWAN, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

// create barang
export const tambahKaryawan = (data) => async (dispatch) => {
  try {
    const add = await axios.post(`${BASE_URL}/karyawan`, data, {
      headers: { "content-type": "multipart/form-data" },
    });
    console.log(add.data);
    dispatch(getKaryawan());
    ToastSuccess.fire({
      icon: "success",
      title: "Karyawan berhasil ditambahkan",
    });
  } catch (error) {
    console.log(error);
  }
};

export const blokirKaryawan = (id, status) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/karyawan/blokir/${id}`, {
      status,
    });
    dispatch(getKaryawan());
    if (status === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Karyawan BLokir",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Karyawan Active",
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const updateKaryawan = (id, data) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/karyawan/${id}`, data);
    dispatch(getKaryawan());
    ToastSuccess.fire({
      icon: "success",
      title: "Karyawan berhasil diupdate",
    });
  } catch (error) {
    console.log(error);
  }
};

export const getKaryawanByToko = (id) => async (dispatch) => {
  try {
    const karyawan = await axios.get(`${BASE_URL}/karyawan/${id}`);
    dispatch({ type: GET_KARYAWAN_BY_TOKO, data: karyawan.data.data });
  } catch (error) {
    console.log(error);
  }
};

// export const updateBarang = (id, data) => async (dispatch) => {
//   try {
//     const update = await axios.put(`${BASE_URL}/barang/${id}`, data, {
//       headers: { "content-type": "multipart/form-data" },
//     });
//     dispatch(getBarang());
//     ToastSuccess.fire({
//       icon: "success",
//       title: "Barang berhasil dirubah",
//     });
//   } catch (error) {
//     console.log(error);
//   }
// };

//   export const setActiveMerek = (id, status) => async (dispatch) => {
//     try {
//       const update = await axios.put(`${BASE_URL}/merek/status/${id}`, {
//         status,
//       });
//       dispatch(getMerek());
//       if (status === "Y") {
//         ToastSuccess.fire({
//           icon: "success",
//           title: "Merek Active",
//         });
//       } else {
//         ToastSuccess.fire({
//           icon: "success",
//           title: "Merek Non Active",
//         });
//       }
//     } catch (error) {
//       console.log(error);
//     }
//   };
