import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_TOKO } from "./index";

export const getToko = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/toko`);
    dispatch({ type: GET_TOKO, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const simpanToko = (data) => async (dispatch) => {
  try {
    const addToko = await axios.post(`${BASE_URL}/toko`, data);
    if (addToko.data.code == 201) {
      dispatch(getToko());
      ToastSuccess.fire({
        icon: "success",
        title: "Toko berhasil disimpan",
      });
    } else {
      ToastSuccess.fire({
        icon: "Error",
        title: "Toko gagal disimpan",
      });
    }
    dispatch(getToko());
  } catch (error) {
    console.log(error);
  }
};

export const updateTokoById = (data, id) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/toko/${id}`, data);
    ToastSuccess.fire({
      icon: "success",
      title: "Toko berhasil dirubah!",
    });
    dispatch(getToko());
  } catch (error) {
    console.log(error);
  }
};

export const updateStatusToko = (id, status) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/toko/status/${id}`, { status });
    if (status === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Toko dibuka",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Toko ditutup",
      });
    }

    dispatch(getToko());
  } catch (error) {
    console.log(error);
  }
};
