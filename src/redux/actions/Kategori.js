import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_KATEGORI } from "./index";

// GET SATUAN
export const getKategori = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/kategori`);
    dispatch({ type: GET_KATEGORI, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

// create satuan
export const tambahKategori = (data) => async (dispatch) => {
  try {
    const add = await axios.post(`${BASE_URL}/kategori`, data);
    dispatch(getKategori());
    ToastSuccess.fire({
      icon: "success",
      title: "Kategori berhasil ditambahkan",
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateKategori = (id, kategori) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/kategori/${id}`, { kategori });
    dispatch(getKategori());
    ToastSuccess.fire({
      icon: "success",
      title: "Kategori berhasil dirubah",
    });
  } catch (error) {
    console.log(error);
  }
};

export const setActiveKategori = (id, status) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/kategori/status/${id}`, {
      status,
    });
    dispatch(getKategori());
    if (status === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Kategori Active",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Kategori Non Active",
      });
    }
  } catch (error) {
    console.log(error);
  }
};
