import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_LAPORAN_KREDIT, GET_LAPORAN_TUNAI } from "./index";

export const getLaporan =
  (tgl_start, tgl_end, toko, kategori, jenis, done) => async (dispatch) => {
    // console.log(tgl_start, tgl_end, toko, kategori, jenis);
    try {
      switch (jenis) {
        case "TUNAI": {
          const dataTunai = await axios.get(
            `${BASE_URL}/laporan/tunai?toko=${toko}&tgl_start=${tgl_start}&tgl_end=${tgl_end}&kategori=${kategori}`
          );

          const TOTAL_TUNAI = dataTunai.data.data.reduce(
            (a, b) =>
              (a = parseInt(a) + parseInt(b.harga_diskon * b.jumlah_item)),
            0
          );
          done();
          return dispatch({
            type: GET_LAPORAN_TUNAI,
            data: {
              data: dataTunai.data.data,
              total: TOTAL_TUNAI,
            },
          });
        }

        case "NONTUNAI": {
          const dataNonTunai = await axios.get(
            `${BASE_URL}/laporan/nontunai?toko=${toko}&tgl_start=${tgl_start}&tgl_end=${tgl_end}&kategori=${kategori}`
          );
          const TOTAL_NONTUNAI = dataNonTunai.data.data.reduce(
            (a, b) =>
              (a = parseInt(a) + parseInt(b.harga_diskon * b.jumlah_item)),
            0
          );
          done();
          return dispatch({
            type: GET_LAPORAN_TUNAI,
            data: {
              data: dataNonTunai.data.data,
              total: TOTAL_NONTUNAI,
            },
          });
        }

        case "KREDIT": {
          const dataKredit = await axios.get(
            `${BASE_URL}/laporan/kredit?toko=${toko}&tgl_start=${tgl_start}&tgl_end=${tgl_end}&kategori=${kategori}`
          );
          const TOTAL_KREDIT = dataKredit.data.data.reduce(
            (a, b) =>
              (a = parseInt(a) + parseInt(b.harga_diskon * b.jumlah_item)),
            0
          );
          done();
          return dispatch({
            type: GET_LAPORAN_KREDIT,
            data: {
              data: dataKredit.data.data,
              total: TOTAL_KREDIT,
            },
          });
        }

        case "REKAPITULASI": {
          const dataRekap = await axios.get(
            `${BASE_URL}/laporan/tunai?toko=${toko}&tgl_start=${tgl_start}&tgl_end=${tgl_end}&kategori=${kategori}`
          );
          const TOTAL_TUNAI = dataRekap.data.data.reduce(
            (a, b) =>
              (a = parseInt(a) + parseInt(b.harga_diskon * b.jumlah_item)),
            0
          );
          done();
          return dispatch({
            type: GET_LAPORAN_TUNAI,
            data: {
              data: dataRekap.data.data,
              total: TOTAL_TUNAI,
            },
          });
        }

        default:
          break;
      }
    } catch (error) {
      console.log(error);
    }
  };
