import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_PENARIKAN } from "./index";

export const getPenarikan = () => async (dispatch) => {
  try {
    const penarikan = await axios.get(`${BASE_URL}/penarikan/all`);
    dispatch({ type: GET_PENARIKAN, data: penarikan.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const tolakPenarikan = (id) => async (dispatch) => {
  try {
    const tolak = await axios.put(`${BASE_URL}/penarikan/tolak/${id}`);
    if (tolak.data.status === "Updated") {
      ToastSuccess.fire({
        icon: "success",
        title: "Tolak berhasil",
      });
      dispatch(getPenarikan());
    }
  } catch (error) {
    ToastSuccess.fire({
      icon: "error",
      title: "tolak gagal",
    });
  }
};

export const handleSetuju = (id) => async (dispatch) => {
  try {
    const setuju = await axios.put(`${BASE_URL}/penarikan/setuju/${id}`);
    console.log(setuju);
    if (setuju.data.status === "Error") {
      ToastSuccess.fire({
        icon: "error",
        title: `${setuju.data.data}`,
      });
    }

    if (setuju.data.status === "Updated") {
      ToastSuccess.fire({
        icon: "success",
        title: "Success",
      });
    }
    dispatch(getPenarikan());
  } catch (error) {
    ToastSuccess.fire({
      icon: "error",
      title: "gagal setuju",
    });
  }
};
