const { SET_SIDEBAR } = require("./index");

export const setSidebarToggle = (toggle) => (dispatch) => {
  dispatch({ type: SET_SIDEBAR, data: toggle });
};
