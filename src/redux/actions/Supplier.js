import axios from "axios";
import { BASE_URL } from "utils";
import { ToastSuccess } from "utils/notifications";
import { GET_SUPPLIER } from "./index";

// GET SUPPLIER
export const getSupplier = () => async (dispatch) => {
  try {
    const data = await axios.get(`${BASE_URL}/supplier`);
    dispatch({ type: GET_SUPPLIER, data: data.data.data });
  } catch (error) {
    console.log(error);
  }
};

export const tambahSupplier = (data) => async (dispatch) => {
  try {
    const add = await axios.post(`${BASE_URL}/supplier`, data);
    dispatch(getSupplier());
    ToastSuccess.fire({
      icon: "success",
      title: "Supplier berhasil ditambahkan",
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateSupplier = (id, data) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/supplier/${id}`, data);
    dispatch(getSupplier());
    ToastSuccess.fire({
      icon: "success",
      title: "Supplier berhasil diupdate",
    });
  } catch (error) {
    console.log(error);
  }
};

export const blokirSupplier = (id, blokir) => async (dispatch) => {
  try {
    const update = await axios.put(`${BASE_URL}/supplier/blokir/${id}`, {
      blokir,
    });
    dispatch(getSupplier());
    if (blokir === "Y") {
      ToastSuccess.fire({
        icon: "success",
        title: "Blokir Supplier",
      });
    } else {
      ToastSuccess.fire({
        icon: "success",
        title: "Buka Supplier",
      });
    }
  } catch (error) {
    console.log(error);
  }
};
