import { GET_MEREK } from "../actions";

const intialState = {
  merek: [],
};

const Merek = (state = intialState, action) => {
  switch (action.type) {
    case GET_MEREK: {
      return {
        ...state,
        merek: action.data,
      };
    }

    default:
      return state;
  }
};

export default Merek;
