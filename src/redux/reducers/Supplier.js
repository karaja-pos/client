import { GET_SUPPLIER } from "../actions";

const intialState = {
  supplier: [],
};

const Supplier = (state = intialState, action) => {
  switch (action.type) {
    case GET_SUPPLIER: {
      return {
        ...state,
        supplier: action.data,
      };
    }

    default:
      return state;
  }
};

export default Supplier;
