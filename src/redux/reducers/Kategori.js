import { GET_KATEGORI } from "../actions";

const intialState = {
  kategori: [],
};

const Kategori = (state = intialState, action) => {
  switch (action.type) {
    case GET_KATEGORI: {
      return {
        ...state,
        kategori: action.data,
      };
    }

    default:
      return state;
  }
};

export default Kategori;
