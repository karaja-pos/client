import { GET_KREDITUR } from "../actions";

const intialState = {
  kreditur: [],
};

const Kreditur = (state = intialState, action) => {
  switch (action.type) {
    case GET_KREDITUR: {
      return {
        ...state,
        kreditur: action.data,
      };
    }

    default:
      return state;
  }
};

export default Kreditur;
