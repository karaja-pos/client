import { GET_HISTORY_PENJUALAN } from "../actions";

const intialState = {
  penjualan: [],
};

const History = (state = intialState, action) => {
  switch (action.type) {
    case GET_HISTORY_PENJUALAN: {
      return {
        ...state,
        penjualan: action.data,
      };
    }

    default:
      return state;
  }
};

export default History;
