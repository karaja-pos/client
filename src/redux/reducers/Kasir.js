import {
  GET_BARANG_KASIR,
  ADD_CART,
  SELECT_BARANG,
  DEL_CART,
  CART_TOTAL,
  // CART_SUB_TOTAL,
  CLEAR_ORDER,
} from "../actions";

const intialState = {
  barang_kasir: [],
  order_list: [],
  harga_sub_total_cart: 0,
  harga_total_cart: 0,
  harga_diskon: 0,
  total_item: 0,
};

const Kasir = (state = intialState, action) => {
  switch (action.type) {
    case GET_BARANG_KASIR: {
      return {
        ...state,
        barang_kasir: action.data,
      };
    }

    case SELECT_BARANG: {
      const data = state.order_list.find((x) => x.id === action.data.id);
      if (!data) {
        let hargaTotal = action.data.harga_jual * 1;
        let diskon = (hargaTotal * action.data.diskon) / 100;
        return {
          ...state,
          order_list: [
            ...state.order_list,
            {
              id: action.data.id,
              id_barang: action.data.id_barang,
              id_satuan: action.data.id_satuan,
              nama_brg: action.data.nama_brg,
              kode_brg: action.data.kode_brg,
              harga_jual: action.data.harga_jual,
              diskon: action.data.diskon,
              quantity: 1,
              jenis: action.data.jenis,
              sub_total: hargaTotal,
              hargaTotal: hargaTotal - diskon,
              totalDiskon: diskon,
              hargaDiskon: action.data.harga_jual - diskon,
            },
          ],
        };
      } else {
        return { ...state };
      }
    }

    case ADD_CART: {
      state.order_list.find((x) => {
        let qty = action.qty == "" ? 1 : parseInt(action.qty);
        if (x.id === action.data.id) {
          let subTotal = x.harga_jual * qty;
          x.quantity = qty;
          x.sub_total = subTotal;
          x.hargaTotal = subTotal - (x.sub_total * x.diskon) / 100;
          x.totalDiskon = (x.sub_total * x.diskon) / 100;
        }
      });
      return {
        ...state,
      };
    }

    case DEL_CART: {
      const arr = state.order_list.filter((x) => x.id !== action.data.id);
      return {
        ...state,
        order_list: arr,
      };
    }

    case CLEAR_ORDER: {
      return {
        ...state,
        order_list: [],
      };
    }

    // case CART_SUB_TOTAL: {
    //   const TOTAL = state.order_list.reduce(
    //     (a, b) => (a = parseInt(a) + parseInt(b.sub_total)),
    //     0
    //   );

    //   return {
    //     ...state,
    //     harga_sub_total_cart: TOTAL,
    //   };
    // }

    case CART_TOTAL: {
      const TOTAL = state.order_list.reduce(
        (a, b) => (a = parseInt(a) + parseInt(b.hargaTotal)),
        0
      );
      const SUB_TOTAL = state.order_list.reduce(
        (a, b) => (a = parseInt(a) + parseInt(b.sub_total)),
        0
      );
      const TOTAL_DISKON = state.order_list.reduce(
        (a, b) => (a = parseInt(a) + parseInt(b.totalDiskon)),
        0
      );
      const TOTAL_ITEM = state.order_list.reduce(
        (a, b) => (a = parseInt(a) + parseInt(b.quantity)),
        0
      );

      return {
        ...state,
        harga_total_cart: TOTAL,
        harga_sub_total_cart: SUB_TOTAL,
        harga_diskon: TOTAL_DISKON,
        total_item: TOTAL_ITEM,
      };
    }

    default:
      return state;
  }
};

export default Kasir;
