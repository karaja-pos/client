import { GET_KARYAWAN, GET_KARYAWAN_BY_TOKO } from "../actions";

const intialState = {
  karyawan: [],
  karyawan_toko: [],
};

const Karyawan = (state = intialState, action) => {
  switch (action.type) {
    case GET_KARYAWAN: {
      return {
        ...state,
        karyawan: action.data,
      };
    }

    case GET_KARYAWAN_BY_TOKO: {
      return {
        ...state,
        karyawan_toko: action.data,
      };
    }

    default:
      return state;
  }
};

export default Karyawan;
