import { GET_TOKO } from "../actions";

const intialState = {
  toko: [],
};

const Toko = (state = intialState, action) => {
  switch (action.type) {
    case GET_TOKO: {
      return {
        ...state,
        toko: action.data,
      };
    }

    default:
      return state;
  }
};

export default Toko;
