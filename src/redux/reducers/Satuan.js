import { GET_SATUAN } from "../actions";

const intialState = {
  satuan: [],
};

const Satuan = (state = intialState, action) => {
  switch (action.type) {
    case GET_SATUAN: {
      return {
        ...state,
        satuan: action.data,
      };
    }

    default:
      return state;
  }
};

export default Satuan;
