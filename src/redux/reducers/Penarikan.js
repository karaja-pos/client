import { GET_PENARIKAN } from "../actions";

const intialState = {
  data: [],
};

const Penarikan = (state = intialState, action) => {
  switch (action.type) {
    case GET_PENARIKAN: {
      return {
        ...state,
        data: action.data,
      };
    }

    default:
      return state;
  }
};

export default Penarikan;
