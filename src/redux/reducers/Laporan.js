import { GET_LAPORAN_TUNAI, GET_LAPORAN_KREDIT } from "../actions";

const intialState = {
  data_tunai: [],
  data_kredit: [],
  total_tunai: 0,
  total_kredit: 0,
};

const Laporan = (state = intialState, action) => {
  switch (action.type) {
    case GET_LAPORAN_TUNAI: {
      return {
        ...state,
        data_tunai: action.data.data,
        total_tunai: action.data.total,
      };
    }

    case GET_LAPORAN_KREDIT: {
      return {
        ...state,
        data_kredit: action.data.data,
        total_kredit: action.data.total,
      };
    }

    default:
      return state;
  }
};

export default Laporan;
