import {
  USER_LOGIN,
  USER_LOGOUT,
  GET_USERS,
  GET_ALL_TOKEN,
} from "../actions/index";

const intialState = {
  currentUser: null,
  users: [],
  tokens: [],
};

const Auth = (state = intialState, action) => {
  switch (action.type) {
    case USER_LOGIN: {
      return {
        ...state,
        currentUser: action.data,
      };
    }

    case USER_LOGIN: {
      return {
        ...state,
        currentUser: null,
      };
    }

    case GET_USERS: {
      return {
        ...state,
        users: action.data,
      };
    }

    case GET_ALL_TOKEN: {
      return {
        ...state,
        tokens: action.data,
      };
    }

    default:
      return state;
  }
};

export default Auth;
