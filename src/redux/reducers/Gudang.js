import {
  GET_FAKTUR_PEMBELIAN,
  GET_FAKTUR_BYNOMOR,
  GET_PENGIRIMAN_BYNOMOR,
  GET_PENGIRIMAN,
  GET_ITEMS_PENGIRIMAN,
} from "../actions";

const intialState = {
  faktur_pembelian: [],
  cetak: [],
  cetak_pengiriman: [],
  data_pengiriman: [],
  items_pengiriman: [],
};

const Gudang = (state = intialState, action) => {
  switch (action.type) {
    case GET_FAKTUR_PEMBELIAN: {
      return {
        ...state,
        faktur_pembelian: action.data,
      };
    }

    case GET_FAKTUR_BYNOMOR: {
      return {
        ...state,
        cetak: action.data,
      };
    }

    case GET_PENGIRIMAN_BYNOMOR: {
      return {
        ...state,
        cetak_pengiriman: action.data,
      };
    }

    case GET_PENGIRIMAN: {
      return {
        ...state,
        data_pengiriman: action.data,
      };
    }

    case GET_ITEMS_PENGIRIMAN: {
      return {
        ...state,
        items_pengiriman: action.data,
      };
    }

    default:
      return state;
  }
};

export default Gudang;
