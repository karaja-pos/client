import { SET_SIDEBAR } from "../actions";

const intialState = {
  sidebar: true,
};

const Barang = (state = intialState, action) => {
  switch (action.type) {
    case SET_SIDEBAR: {
      return {
        ...state,
        sidebar: action.data,
      };
    }

    default:
      return state;
  }
};

export default Barang;
