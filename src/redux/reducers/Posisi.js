import { GET_POSISI } from "../actions";

const intialState = {
  posisi: [],
};

const Posisi = (state = intialState, action) => {
  switch (action.type) {
    case GET_POSISI: {
      return {
        ...state,
        posisi: action.data,
      };
    }

    default:
      return state;
  }
};

export default Posisi;
