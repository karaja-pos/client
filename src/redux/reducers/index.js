import { combineReducers } from "redux";
import Toko from "./Toko";
import Posisi from "./Posisi";
import Satuan from "./Satuan";
import Supplier from "./Supplier";
import Kategori from "./Kategori";
import Merek from "./Merek";
import Barang from "./Barang";
import Kreditur from "./Kreditur";
import Karyawan from "./Karyawan";
import Gudang from "./Gudang";
import Kasir from "./Kasir";
import App from "./App";
import Auth from "./Auth";
import Penarikan from "./Penarikan";
import History from "./History";
import Laporan from "./Laporan";

export default combineReducers({
  toko: Toko,
  posisi: Posisi,
  satuan: Satuan,
  supplier: Supplier,
  kategori: Kategori,
  merek: Merek,
  barang: Barang,
  kreditur: Kreditur,
  karyawan: Karyawan,
  gudang: Gudang,
  kasir: Kasir,
  app: App,
  auth: Auth,
  penarikan: Penarikan,
  history: History,
  laporan: Laporan,
});
