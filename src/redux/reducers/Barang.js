import {
  GET_BARANG,
  GET_STOK_TOKO,
  GET_BARANG_TOKO,
  SET_SELECTED_BARANG,
  SET_ID_BARANG,
} from "../actions";

const intialState = {
  barang: [],
  barang_toko: [],
  stok_toko: [],
  selected: [],
  id_barang: null,
};

const Barang = (state = intialState, action) => {
  switch (action.type) {
    case GET_BARANG: {
      return {
        ...state,
        barang: action.data,
      };
    }

    case GET_BARANG_TOKO: {
      return {
        ...state,
        barang_toko: action.data,
      };
    }

    case GET_STOK_TOKO: {
      return {
        ...state,
        stok_toko: action.data,
      };
    }

    case SET_SELECTED_BARANG: {
      return {
        ...state,
        selected: action.data,
      };
    }

    case SET_ID_BARANG: {
      return {
        ...state,
        id_barang: action.data,
      };
    }

    default:
      return state;
  }
};

export default Barang;
