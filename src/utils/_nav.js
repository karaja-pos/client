import React, { useEffect, useState } from "react";
import {
  IoCashOutline,
  IoCardOutline,
  IoWalletOutline,
  IoAirplaneOutline,
  IoBagCheckSharp,
  IoBriefcaseSharp,
  IoAnalytics,
  IoArrowUndoSharp,
  IoArrowRedo,
  IoReceipt,
  IoPeople,
  IoStorefrontSharp,
  IoCube,
  IoCellularSharp,
  IoPricetags,
  IoLayersSharp,
  IoBagAdd,
  IoCard,
  IoBookmarks,
  IoBagHandle,
  IoPrintOutline,
  IoGitCompareOutline,
  IoCubeSharp,
  IoLayersOutline,
} from "react-icons/io5";
import io from "socket.io-client";
import { FaHistory, FaTruck, FaUserLock } from "react-icons/fa";
import { Badge } from "reactstrap";
import useSound from "use-sound";
import notif from "../assets/sound/notify.ogg";
import { SOCKET_URL } from "utils";
import { useDispatch, useSelector } from "react-redux";
import { getPengiriman } from "redux/actions/Gudang";
import { getPengirimanByToko } from "redux/actions/Gudang";
const socket = io(`${SOCKET_URL}`);

const Nav = () => {
  const [play] = useSound(notif);
  const dispatch = useDispatch();
  const currentUser = useSelector((data) => data.auth.currentUser);
  const dataFaktur = useSelector((data) => data.gudang.data_pengiriman);

  useEffect(() => {
    if (currentUser.role == "admin") {
      dispatch(getPengiriman());
    } else {
      dispatch(getPengirimanByToko(currentUser.id_toko));
    }
  }, []);

  useEffect(() => {
    socket.on("pengiriman", () => {
      play();
      if (currentUser.role == "admin") {
        dispatch(getPengiriman());
      } else {
        dispatch(getPengirimanByToko(currentUser.id_toko));
      }
    });
  });
  return dataFaktur.length;
};

export const _navAdmin = [
  {
    id: 1,
    name: "Dashboard",
    type: 1,
    to: "/admin/index",
    icon: "ni ni-tv-2 text-info",
  },

  {
    id: 2,
    name: "Toko",
    type: 2,
    route: "/toko",
    _children: [
      {
        id: 1,
        name: "Penjualan",
        to: "/admin/toko/penjualan",
        icon: <IoCashOutline className="text-info" />,
      },
      {
        id: 2,
        name: "History Penjualan",
        to: "/admin/toko/histori-penjualan",
        icon: <FaHistory className="text-info" />,
      },
      {
        id: 3,
        name: (
          <span>
            Penerimaan{" "}
            <Badge color="warning">
              <Nav />
            </Badge>
          </span>
        ),
        to: "/admin/toko/penerimaan",
        icon: <IoBagCheckSharp className="text-info" />,
      },
      {
        id: 5,
        name: (
          <span>
            Penarikan{" "}
            {/* <Badge color="warning">
              <Nav />
            </Badge> */}
          </span>
        ),
        to: "/admin/toko/penarikan",
        icon: <IoGitCompareOutline className="text-info" />,
      },
    ],
  },

  {
    id: 3,
    name: "Gudang",
    route: "/operasional",
    type: 2,
    _children: [
      {
        id: 1,
        name: "Faktur Pembelian",
        to: "/admin/gudang/pembelian",
        icon: <IoReceipt className="text-info" />,
      },
      {
        id: 2,
        name: "Pengiriman",
        to: "/admin/gudang/pengiriman",
        icon: <FaTruck className="text-info" />,
      },
      {
        id: 3,
        name: "Opname",
        to: "",
        icon: <IoBriefcaseSharp className="text-info" />,
      },
    ],
  },

  {
    id: 4,
    name: "Akuntansi",
    route: "/operasional",
    type: 2,
    _children: [
      {
        id: 1,
        name: "Master Akun",
        to: "/admin/akuntansi/masterakun",
        icon: <IoCubeSharp className="text-warning" />,
      },
      {
        id: 2,
        name: "Jenis Akun",
        to: "",
        icon: <IoLayersOutline className="text-warning" />,
      },
      {
        id: 3,
        name: "Kas Masuk",
        to: "",
        icon: <IoArrowRedo className="text-warning" />,
      },
      {
        id: 4,
        name: "Kas Keluar",
        to: "",
        icon: <IoArrowUndoSharp className="text-warning" />,
      },
    ],
  },

  {
    id: 5,
    name: "Laporan",
    route: "/operasional",
    type: 2,
    _children: [
      {
        id: 1,
        name: "Laporan Penjualan",
        to: "/admin/laporan/penjualan",
        icon: <IoReceipt className="text-info" />,
      },
      {
        id: 2,
        name: "Laporan Stok",
        to: "",
        icon: <IoReceipt className="text-info" />,
      },
      {
        id: 3,
        name: "Laporan Harian Kasir",
        to: "",
        icon: <IoReceipt className="text-info" />,
      },
      // {
      //   id: 4,
      //   name: "Data Karyawan",
      //   to: "",
      //   icon: <IoPeople className="text-info" />,
      // },
    ],
  },

  {
    id: 6,
    name: "Setting",
    route: "/setting",
    type: 2,
    _children: [
      {
        id: 1,
        name: "Penjualan",
        to: "/admin/setting/penjualan",
        icon: <IoBagHandle className="text-info" />,
      },
      {
        id: 1,
        name: "Toko",
        to: "/admin/setting/toko",
        icon: <IoStorefrontSharp className="text-info" />,
      },
      {
        id: 2,
        name: "Karyawan",
        to: "/admin/setting/karyawan",
        icon: <IoPeople className="text-info" />,
      },
      {
        id: 3,
        name: "Barang",
        to: "/admin/setting/barang",
        icon: <IoCube className="text-info" />,
      },
      {
        id: 4,
        name: "Posisi / Level",
        to: "/admin/setting/posisi",
        icon: <IoCellularSharp className="text-info" />,
      },
      {
        id: 5,
        name: "Kategori Barang",
        to: "/admin/setting/kategori",
        icon: <IoPricetags className="text-info" />,
      },
      {
        id: 5,
        name: "Merek",
        to: "/admin/setting/merek",
        icon: <IoBookmarks className="text-info" />,
      },
      {
        id: 6,
        name: "Satuan Barang",
        to: "/admin/setting/satuan",
        icon: <IoLayersSharp className="text-info" />,
      },
      {
        id: 7,
        name: "Supplier",
        to: "/admin/setting/supplier",
        icon: <IoBagAdd className="text-info" />,
      },
      {
        id: 8,
        name: "Kreditur",
        to: "/admin/setting/kreditur",
        icon: <IoCard className="text-info" />,
      },
      {
        id: 8,
        name: "Cetak",
        to: "/admin/setting/cetak",
        icon: <IoPrintOutline className="text-info" />,
      },
      {
        id: 8,
        name: "Akun",
        to: "/admin/setting/akun",
        icon: <FaUserLock className="text-info" />,
      },
    ],
  },
];

export const _navToko = [
  {
    id: 1,
    name: "Dashboard",
    type: 1,
    to: "/toko/index",
    icon: "ni ni-tv-2 text-info",
  },

  {
    id: 2,
    name: "Toko",
    type: 2,
    route: "/toko",
    _children: [
      {
        id: 1,
        name: "Penjualan",
        to: "/toko/toko/penjualan",
        icon: <IoCashOutline className="text-info" />,
      },

      {
        id: 4,
        name: (
          <span>
            Penerimaan{" "}
            <Badge color="warning">
              <Nav />
            </Badge>
          </span>
        ),
        to: "/toko/toko/penerimaan",
        icon: <IoBagCheckSharp className="text-info" />,
      },
    ],
  },
];

// export default _nav;
