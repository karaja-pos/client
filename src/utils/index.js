export const SOCKET_URL = "http://localhost:5000";
export const BASE_URL = "http://localhost:5000/api/v1";



export const setKodeToko = (num) => {
  return ("000" + num).slice(-4);
};

export const setKodeSupplier = (num) => {
  return ("000" + num).slice(-4);
};

export const setKodeBarang = (num) => {
  const date = new Date().getTime();
  return date;
};

export const setKodeKaryawan = (num) => {
  return ("0000" + num).slice(-5);
};

export const setNoFaktur = (num) => {
  return ("000" + num).slice(-4);
};

// 4349
