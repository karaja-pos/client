import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
const TheLayout = React.lazy(() => import("../layouts/Auth"));

const AuthRoute = ({ component: Component, ...rest }) => {
  const currentUser = useSelector((data) => data.auth.currentUser);
  return (
    <Route
      {...rest}
      render={(props) =>
        currentUser ? <Redirect to="/" /> : <TheLayout {...props} />
      }
    />
  );
};

export default AuthRoute;
