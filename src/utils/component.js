import React, { useState } from "react";
import CurrencyInput from "react-currency-input-field";
import { useDispatch } from "react-redux";
import { Card, Media } from "reactstrap";
import {
  setTotalHarga,
  setStokAkhir,
  setDiskonBarang,
} from "redux/actions/Barang";
import styled from "styled-components";

import NoPhoto from "../assets/img/no_photo.jpg";

const Container = styled.div`
  width: 100%;
  text-align: center;
  padding: 10px;

  p {
    opacity: 0.7;
    line-height: 0.5;
  }

  svg {
    width: 100px;
    height: 100px;
    opacity: 0.5;
  }
`;

export const NoDataBarang = () => (
  <Container>
    <p>Opps.!</p>
    <p>Tidak ada Data</p>
    <svg
      width={479}
      height={479}
      viewBox="0 0 479 479"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0)">
        <path
          d="M281.768 359.256H96.6318L103.177 416.898C104.073 424.991 110.919 431.115 119.066 431.107H136.709C136.709 409.063 154.582 391.19 176.626 391.19C198.67 391.19 216.544 409.063 216.544 431.107H298.374C283.456 410.337 277.476 384.461 281.768 359.256Z"
          fill="#9BC9FF"
        />
        <path
          d="M71.325 135.719L96.6319 359.256H281.768C283.623 348.013 287.49 337.192 293.186 327.322C310.31 297.661 341.963 279.398 376.213 279.421C388.788 279.386 401.247 281.854 412.856 286.687L456.047 135.719H71.325Z"
          fill="#9BC9FF"
        />
        <path
          d="M472.014 119.752C472.014 110.934 464.865 103.785 456.047 103.785H67.7308L71.325 135.719H456.047C464.865 135.719 472.014 128.57 472.014 119.752Z"
          fill="#9BC9FF"
        />
        <path
          d="M422.414 282.286L462.331 142.759C473.901 139.629 481.358 128.41 479.772 116.528C478.181 104.646 468.034 95.782 456.047 95.8015H74.845L66.4055 21.3308C65.0918 9.17239 54.8123 -0.0311979 42.5837 -1.25388e-05H8.97369C4.56486 -1.25388e-05 0.990234 3.57461 0.990234 7.98345C0.990234 12.3923 4.56486 15.9669 8.97369 15.9669H42.5837C46.6807 15.9435 50.1306 19.0231 50.5672 23.0967L63.3805 136.611L95.2442 417.759C96.5579 429.918 106.837 439.121 119.066 439.09H129.443C133.318 462.129 153.265 479 176.626 479C199.988 479 219.935 462.129 223.81 439.09H294.597C320.863 472.65 364.982 486.73 405.831 474.583C446.68 462.436 475.94 426.538 479.604 384.083C483.264 341.624 460.581 301.247 422.414 282.286V282.286ZM76.6655 111.768H456.047C460.456 111.768 464.031 115.343 464.031 119.752C464.031 124.161 460.456 127.735 456.047 127.735H78.4314L76.6655 111.768ZM80.2674 143.702H445.46L407.539 276.228C407.316 276.154 407.094 276.115 406.868 276.045C406.182 275.827 405.488 275.667 404.794 275.468C402.51 274.813 400.21 274.233 397.879 273.738C396.904 273.531 395.934 273.344 394.951 273.168C392.772 272.771 390.566 272.451 388.356 272.198C387.428 272.093 386.512 271.964 385.576 271.886C382.481 271.613 379.405 271.438 376.213 271.438C340.817 271.516 307.882 289.552 288.753 319.338H168.643C164.234 319.338 160.659 322.913 160.659 327.322C160.659 331.731 164.234 335.305 168.643 335.305H280.411C279.947 336.424 279.511 337.551 279.086 338.681C278.72 339.671 278.376 340.661 278.033 341.659C277.441 343.402 276.907 345.164 276.435 346.945C276.155 347.935 275.87 348.926 275.636 349.931C275.527 350.38 275.383 350.816 275.277 351.272H103.777L80.2674 143.702ZM119.066 423.123C114.969 423.147 111.519 420.067 111.082 415.994L105.574 367.239H272.763C272.556 369.906 272.428 372.572 272.428 375.223C272.401 391.903 276.42 408.338 284.138 423.123H223.81C219.935 400.085 199.988 383.214 176.626 383.214C153.265 383.214 133.318 400.085 129.443 423.123H119.066ZM176.626 463.041C158.991 463.041 144.693 448.742 144.693 431.107C144.693 413.471 158.991 399.173 176.626 399.173C194.262 399.173 208.56 413.471 208.56 431.107C208.56 448.742 194.262 463.041 176.626 463.041ZM376.213 463.041C347.935 462.998 321.393 449.385 304.857 426.445C294.098 411.538 288.332 393.606 288.395 375.223C288.403 370.315 288.82 365.415 289.642 360.573C291.338 350.27 294.877 340.361 300.089 331.314C315.85 304.178 344.836 287.459 376.213 287.405C379.335 287.412 382.458 287.584 385.561 287.915C393.899 288.769 402.069 290.839 409.807 294.063C448.547 310.072 470.572 351.233 462.398 392.347C454.223 433.461 418.13 463.064 376.213 463.041Z"
          fill="#1E81CE"
        />
        <path
          d="M427.022 324.418C423.903 321.299 418.847 321.299 415.732 324.418L376.213 363.933L336.693 324.418C333.563 321.389 328.581 321.436 325.505 324.511C322.426 327.591 322.379 332.573 325.408 335.703L364.924 375.223L325.408 414.742C323.33 416.746 322.5 419.712 323.229 422.504C323.958 425.295 326.141 427.478 328.932 428.207C331.723 428.936 334.689 428.105 336.693 426.027L376.213 386.512L415.732 426.027C418.863 429.056 423.845 429.013 426.924 425.934C430.004 422.854 430.047 417.873 427.022 414.742L387.502 375.223L427.022 335.703C430.136 332.588 430.136 327.532 427.022 324.418V324.418Z"
          fill="#1E81CE"
        />
        <path
          d="M136.709 319.338H120.742C116.333 319.338 112.759 322.913 112.759 327.322C112.759 331.731 116.333 335.305 120.742 335.305H136.709C141.118 335.305 144.693 331.731 144.693 327.322C144.693 322.913 141.118 319.338 136.709 319.338Z"
          fill="#1E81CE"
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width={479} height={479} fill="white" />
        </clipPath>
      </defs>
    </svg>
  </Container>
);

export const BarangComponent = (props) => {
  return (
    <div style={{ padding: 20 }}>
      <Card className="p-5">
        <Media>
          <Media>
            <Media
              style={{ width: 100 }}
              object
              src={NoPhoto}
              alt={props.data.nama_brg}
            />
          </Media>
          <Media body className="ml-4">
            <Media heading>{props.data.nama_brg}</Media>
            {props.data.deskripsi}
          </Media>
        </Media>
      </Card>
    </div>
  );
};

export const KaryawanComponent = (props) => {
  return (
    <div style={{ padding: 20 }}>
      <Card className="p-5">
        <Media>
          <Media>
            <Media
              style={{ width: 100 }}
              object
              src={
                props.data.file !== null
                  ? `data:image/jpeg;base64,${new Buffer(
                      props.data.file
                    ).toString("base64")}`
                  : NoPhoto
              }
              alt={props.data.nama_karyawan}
            />
          </Media>
          <Media body className="ml-4">
            <Media heading>Nama {props.data.nama_karyawan}</Media>
            <Media>Posisi {props.data.posisi}</Media>
            <Media>Nama Toko {props.data.nama_toko}</Media>
            <Media>
              Nomor HP{" "}
              {props.data.nomor_hp === null ? "TIDAK ADA" : props.data.nomor_hp}
            </Media>
          </Media>
        </Media>
      </Card>
    </div>
  );
};

export const InputHarga = ({ item }) => {
  const dispatch = useDispatch();
  const [harga, setHarga] = useState(0);
  return (
    <CurrencyInput
      id="input-example"
      name="input-name"
      placeholder="0"
      decimalsLimit={2}
      prefix="Rp "
      defaultValue={item.harga_jual}
      onBlur={(e) => {
        dispatch(setTotalHarga(item.id, item.id_toko, harga));
      }}
      onValueChange={(value, name) => setHarga(value)}
      style={{ textAlign: "left", width: 100 }}
    />
  );
};

export const InputStok = ({ item }) => {
  const dispatch = useDispatch();
  const [stok, setStok] = useState(0);
  return (
    <input
      type="number"
      min="0"
      onBlur={() => {
        dispatch(setStokAkhir(item.id, item.id_toko, stok));
      }}
      defaultValue={item.stok_akhir}
      style={{ width: 50, textAlign: "center" }}
      onChange={(e) => setStok(e.target.value)}
      placeholder="0"
    />
  );
};

export const InputDiskon = ({ item }) => {
  const dispatch = useDispatch();
  const [diskon, setDiskon] = useState(0);
  return (
    <input
      type="number"
      min="0"
      step="0.01"
      max="100"
      onBlur={() => {
        dispatch(setDiskonBarang(item.id, item.id_toko, diskon));
      }}
      defaultValue={item.diskon}
      style={{ width: 50, textAlign: "center" }}
      onChange={(e) => setDiskon(e.target.value)}
      placeholder="0"
    />
  );
};
