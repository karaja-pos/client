import React from "react";
import { useDispatch, useSelector } from "react-redux";
const PenjualanTunai = React.lazy(() =>
  import("./views/toko/penjualantunai/index")
);
const HistoriPenjualan = React.lazy(() =>
  import("./views/toko/historipenjualan/index")
);
const PenerimaanToko = React.lazy(() =>
  import("./views/users/toko/penerimaan/index")
);

const LaporanPenjualan = React.lazy(() =>
  import("./views/laporan/penjualan/index")
);
const Penarikan = React.lazy(() => import("./views/toko/penarikan/index"));
const Penerimaan = React.lazy(() => import("./views/toko/penerimaan/index"));
const Pengiriman = React.lazy(() => import("./views/gudang/pengiriman/index"));
const Pembelian = React.lazy(() => import("./views/gudang/pembelian/index"));
const Penjualan = React.lazy(() => import("./views/setting/penjualan/index"));
const CetakLabel = React.lazy(() => import("./views/setting/cetaklabel/index"));
const Akun = React.lazy(() => import("./views/setting/akun/index"));
const Karyawan = React.lazy(() => import("./views/setting/karyawan/index"));
const Kreditur = React.lazy(() => import("./views/setting/kreditur/index"));
const Barang = React.lazy(() => import("./views/setting/barang/index"));
const Merek = React.lazy(() => import("./views/setting/merek/index"));
const Kategori = React.lazy(() => import("./views/setting/kategori/index"));
const Supplier = React.lazy(() => import("./views/setting/supplier/index"));
const Toko = React.lazy(() => import("./views/setting/toko/index"));
const Satuan = React.lazy(() => import("./views/setting/satuan/index"));
const Posisi = React.lazy(() => import("./views/setting/posisi/index"));
const Index = React.lazy(() => import("./views/Index"));
const IndexToko = React.lazy(() => import("./views/users/toko/Index"));
// akuntansi
const MasterAkun = React.lazy(() =>
  import("./views/akuntansi/masterakun/index")
);

export const routeAdmin = [
  {
    path: "/index",
    layout: "/admin",
    exact: true,
    name: "Dashboard",
    component: Index,
  },

  // TOKO
  {
    path: "/toko/penjualan",
    layout: "/admin",
    name: "Toko/Penjualan",
    component: PenjualanTunai,
  },
  {
    path: "/toko/penerimaan",
    layout: "/admin",
    name: "Toko/Penerimaan",
    component: Penerimaan,
  },

  {
    path: "/toko/histori-penjualan",
    layout: "/admin",
    name: "Toko/Histori-Penjualan",
    component: HistoriPenjualan,
  },

  {
    path: "/toko/penarikan",
    layout: "/admin",
    name: "Toko/Penarikan-Barang",
    component: Penarikan,
  },

  // GUDANG
  {
    path: "/gudang/pembelian",
    layout: "/admin",
    name: "Gudang/Pembelian",
    component: Pembelian,
  },

  {
    path: "/gudang/pengiriman",
    layout: "/admin",
    name: "Gudang/Pengiriman",
    component: Pengiriman,
  },

  // Akuntansi
  {
    path: "/akuntansi/masterakun",
    layout: "/admin",
    name: "Akuntansi/Master Akun",
    component: MasterAkun,
  },

  // laporan
  {
    path: "/laporan/penjualan",
    layout: "/admin",
    name: "Laporan/Penjualan",
    component: LaporanPenjualan,
  },

  // SETTING
  {
    path: "/setting/toko",
    layout: "/admin",
    name: "Setting/Toko",
    component: Toko,
  },
  {
    path: "/setting/posisi",
    layout: "/admin",
    name: "Setting/Posisi",
    component: Posisi,
  },
  {
    path: "/setting/satuan",
    layout: "/admin",
    name: "Setting/Satuan",
    component: Satuan,
  },

  {
    path: "/setting/supplier",
    layout: "/admin",
    name: "Setting/Supplier",
    component: Supplier,
  },

  {
    path: "/setting/kategori",
    layout: "/admin",
    name: "Setting/Kategori",
    component: Kategori,
  },

  {
    path: "/setting/merek",
    layout: "/admin",
    name: "Setting/Merek",
    component: Merek,
  },

  {
    path: "/setting/barang",
    layout: "/admin",
    name: "Setting/Barang",
    component: Barang,
  },

  {
    path: "/setting/kreditur",
    layout: "/admin",
    name: "Setting/Kreditur",
    component: Kreditur,
  },

  {
    path: "/setting/karyawan",
    layout: "/admin",
    name: "Setting/Karyawan",
    component: Karyawan,
  },

  {
    path: "/setting/penjualan",
    layout: "/admin",
    name: "Setting/Penjualan",
    component: Penjualan,
  },

  {
    path: "/setting/cetak",
    layout: "/admin",
    name: "Setting/Cetak",
    component: CetakLabel,
  },

  {
    path: "/setting/akun",
    layout: "/admin",
    name: "Setting/Akun",
    component: Akun,
  },
];

export const routeToko = [
  {
    path: "/index",
    layout: "/toko",
    exact: true,
    name: "Dashboard",
    component: IndexToko,
  },
  // TOKO
  {
    path: "/toko/penjualan",
    layout: "/toko",
    name: "Toko/Penjualan",
    component: PenjualanTunai,
  },
  {
    path: "/toko/penerimaan",
    layout: "/toko",
    name: "Toko/Penerimaan",
    component: PenerimaanToko,
  },
];
