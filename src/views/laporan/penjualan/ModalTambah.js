import React, { createRef, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Row,
  Col,
  Media,
} from "reactstrap";
import { tambahBarang } from "redux/actions/Barang";
import { getKategori } from "redux/actions/Kategori";
import { getMerek } from "redux/actions/Merek";
import { getSatuan } from "redux/actions/Satuan";
import { setKodeBarang } from "utils";
import NoPhoto from "../../../assets/img/no_photo.jpg";

const ModalTambah = ({ modal, toggle, barang }) => {
  const fileRef = useRef(null);
  const dataMerek = useSelector((data) => data.merek.merek);
  const dataKategori = useSelector((data) => data.kategori.kategori);
  const dataSatuan = useSelector((data) => data.satuan.satuan);
  const dispatch = useDispatch();
  const [kodeErr, setKodeErr] = useState(false);
  const [nama, setNama] = useState("");
  const [namaErr, setNamaErr] = useState(false);
  const [file, setFile] = useState(null);
  const [img, setImg] = useState("");
  const [stok, setStok] = useState(0);
  const [kategori, setKategori] = useState(0);
  const [kategoriErr, setKategoriErr] = useState(false);
  const [merek, setMerek] = useState(0);
  const [merekErr, setMerekErr] = useState(false);
  const [satuan, setSatuan] = useState(0);
  const [satuanErr, setSatuanErr] = useState(false);
  const [deskripsi, setDeskripsi] = useState("");
  const [deskripsiErr, setDeskripsiErr] = useState(false);
  const [kode, setKode] = useState("");

  const handleSimpan = () => {
    kode === "" ? setKodeErr(true) : setKodeErr(false);
    nama === "" ? setNamaErr(true) : setNamaErr(false);
    kategori === 0 ? setKategoriErr(true) : setKategoriErr(false);
    merek === 0 ? setMerekErr(true) : setMerekErr(false);
    satuan === 0 ? setSatuanErr(true) : setSatuanErr(false);
    deskripsi === "" ? setDeskripsiErr(true) : setDeskripsiErr(false);

    if (
      kode !== "" &&
      nama !== "" &&
      kategori !== 0 &&
      merek !== 0 &&
      satuan !== 0 &&
      deskripsi !== ""
    ) {
      let data = {
        id_kategori: kategori,
        kode_brg: kode,
        id_merek: merek,
        nama_brg: nama,
        deskripsi: deskripsi,
        stok_akhir: stok,
        id_satuan: satuan,
        gambar_brg: 0,
        user: "",
      };

      // const formData = new FormData();
      // formData.append("id_kategori", kategori);
      // formData.append("kode_brg", kode);
      // formData.append("id_merek", merek);
      // formData.append("nama_brg", nama);
      // formData.append("deskripsi", deskripsi);
      // formData.append("id_satuan", satuan);
      // formData.append("file", file == null ? "" : file);
      // formData.append("stok_akhir", stok);
      // formData.append("user", "");
      dispatch(tambahBarang(data));
      toggle();
      reset();
    }
  };

  const reset = () => {
    setKode("");
    setNama("");
    setKategori(0);
    setMerek(0);
    setSatuan(0);
    setStok(0);
    setDeskripsi("");
    setFile(null);
    setImg("");
  };

  const handleOnChange = (e) => {
    // console.log("changed");
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      setFile(file);
      setImg(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleOpenFile = () => {
    if (fileRef !== null) {
      fileRef.current.click();
    }
  };

  useEffect(() => {
    setKode(`Q${setKodeBarang(barang + 1)}C`);
    dispatch(getMerek());
    dispatch(getKategori());
    dispatch(getSatuan());
  }, [barang]);

  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      // style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Tambah Barang
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row className="mb-3">
          <Col>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Media
                center
                onClick={handleOpenFile}
                style={{ cursor: "pointer" }}
              >
                <Media
                  style={{
                    width: 100,
                    height: 100,
                    borderRadius: "50%",
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                  object
                  src={file === null ? NoPhoto : img}
                  alt="Generic placeholder image"
                />
                <input
                  style={{ display: "none" }}
                  type="file"
                  name="file"
                  onChange={(e) => handleOnChange(e)}
                  ref={fileRef}
                  id="photo"
                />
              </Media>
            </div>
          </Col>
        </Row>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="kode">Kode Barang</Label>
              <Input
                type="text"
                name="kode"
                id="kode"
                placeholder="Kode Barang"
                onChange={(e) => setKode(e.target.value)}
                value={kode}
              />
              {kodeErr && <FormFeedback>Pilih Satuan</FormFeedback>}
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="kategori">Kategori</Label>
              <Input
                type="select"
                name="kategori"
                id="kategori"
                value={kategori}
                invalid={kategoriErr}
                onChange={(e) => setKategori(e.target.value)}
              >
                <option value="0">-- Pilik Kategori --</option>
                {dataKategori.map((data) => (
                  <option key={data.id} value={data.id}>
                    {data.kategori}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="nama">Nama Barang</Label>
          <Input
            invalid={namaErr}
            type="text"
            name="nama"
            id="nama"
            value={nama}
            onChange={(e) => setNama(e.target.value)}
            placeholder="Masukan nama barang"
          />
          {namaErr && <FormFeedback>Masih kosong</FormFeedback>}
        </FormGroup>

        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="merek">Merek</Label>
              <Input
                invalid={merekErr}
                type="select"
                name="merek"
                id="merek"
                value={merek}
                onChange={(e) => setMerek(e.target.value)}
              >
                <option value="0">-- Pilik Merek --</option>
                {dataMerek.map((data) => (
                  <option key={data.id} value={data.id}>
                    {data.merek}
                  </option>
                ))}
              </Input>
              {merekErr && <FormFeedback>Pilih Merek</FormFeedback>}
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="satuan">Satuan</Label>
              <Input
                invalid={satuanErr}
                type="select"
                name="satuan"
                id="satuan"
                value={satuan}
                onChange={(e) => setSatuan(e.target.value)}
              >
                <option value="0">-- Pilik Satuan --</option>
                {dataSatuan.map((data) => (
                  <option key={data.id} value={data.id}>
                    {data.nama_satuan}
                  </option>
                ))}
              </Input>
              {satuanErr && <FormFeedback>Pilih Satuan</FormFeedback>}
            </FormGroup>
          </Col>
        </Row>

        <FormGroup>
          <Label for="stok">Stok Akhir</Label>
          <Input
            type="number"
            name="stok"
            id="stok"
            value={stok}
            onChange={(e) => setStok(e.target.value)}
            placeholder="Masukan stok barang"
          />
        </FormGroup>

        <FormGroup>
          <Label for="deskripsi">Deskripsi</Label>
          <Input
            invalid={deskripsiErr}
            type="textarea"
            name="deskripsi"
            id="deskripsi"
            value={deskripsi}
            onChange={(e) => setDeskripsi(e.target.value)}
            placeholder="Masukan deskripsi barang"
          />
          {namaErr && <FormFeedback>Masih kosong</FormFeedback>}
        </FormGroup>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleSimpan()}>
          Tambahkan
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalTambah;
