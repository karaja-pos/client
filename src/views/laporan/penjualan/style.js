import styled from "styled-components";

export const TitleHead = styled.div`
  width: 100%;
  font-family: "Open Sans" sans-serif;
  font-size: 10px;
  background-color: #26dff6;
  color: #fff;
  text-align: center;
  font-weight: bold;
  border-radius: 5px;
  margin-bottom: 5px;
  padding: 12px 24px;
`;

export const Container = styled.div`
  width: 100%;
  font-family: "Open Sans" sans-serif;
  font-size: 10px;
`;

export const TableContainer = styled.div`
  width: 100%;
  font-family: "Open Sans" sans-serif;
  font-size: 10px;
  overflow: auto;
  /* #f6f9fc */

  /* width */
  &&::-webkit-scrollbar {
    width: 10px;
    transition: 0.3s;
  }

  /* Track */
  &&::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #e9ecef;
    border-radius: 10px;
    transition: 0.3s;
  }

  /* Handle */
  &&::-webkit-scrollbar-thumb {
    background: rgba(52, 73, 94, 0.1);
    border-radius: 10px;
    transition: 0.3s;
  }

  /* Handle on hover */
  &&::-webkit-scrollbar-thumb:hover {
    background: rgba(52, 152, 219, 0.1);
    transition: 0.3s;
  }

  &&:hover {
    &&::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px #e9ecef;
      border-radius: 10px;
      transition: 0.3s;
    }

    /* Handle */
    &&::-webkit-scrollbar-thumb {
      background: rgba(52, 73, 94, 0.2);
      border-radius: 10px;
      transition: 0.3s;
    }

    /* Handle on hover */
    &&::-webkit-scrollbar-thumb:hover {
      background: rgba(52, 152, 219, 0.2);
      transition: 0.3s;
    }
  }
`;

export const TableHeader = styled.div`
  /* background-color: #f6f9fc; */
  color: #8898aa;
  display: flex;
  border-top: 1px solid #e9ecef;
  border-bottom: 1px solid #e9ecef;
  align-items: center;
`;

export const THeadItem = styled.div`
  font-weight: bold;
  width: 100%;
  padding: 12px 24px;
`;

export const TableBody = styled.div`
  .active {
    background-color: rgba(52, 152, 219, 0.2);
  }
`;

export const TBodyBaris = styled.div`
  display: flex;
  border-top: 1px solid #e9ecef;
  cursor: pointer;

  &&:hover {
    transition: 0.3s;
    background-color: rgba(52, 152, 219, 0.2);
  }
`;

export const TBodyColom = styled.div`
  width: 100%;
  padding: 12px 24px;
`;
