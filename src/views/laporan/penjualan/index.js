import React, { useEffect, useRef, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Button,
  Col,
  Input,
  Label,
  Badge,
} from "reactstrap";
import { useReactToPrint } from "react-to-print";
import Select from "react-select";
import convertRupiah from "rupiah-format";
import DataTable from "react-data-table-component";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import BounceLoader from "react-spinners/BounceLoader";
import { css } from "@emotion/react";
import { getKategori } from "redux/actions/Kategori";
import { getToko } from "redux/actions/Toko";
import { NoDataBarang } from "utils/component";
import { getLaporan } from "redux/actions/Laporan";
import Swal from "sweetalert2";
import { IoPrint } from "react-icons/io5";
import { GET_LAPORAN_TUNAI } from "redux/actions";
import { GET_LAPORAN_KREDIT } from "redux/actions";
import CetakLaporanTunai from "./CetakLaporanTunai";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const override = css`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, 0);
  z-index: 99999999;
`;

const LaporanPenjualan = () => {
  const dispatch = useDispatch();
  // const dataUsers = useSelector((x) => x.auth.users);
  // const dataToken = useSelector((x) => x.auth.tokens);
  const cetakRef = useRef();
  const dataKategory = useSelector((data) => data.kategori.kategori);
  const dataToko = useSelector((data) => data.toko.toko);
  const dataLaporanTunai = useSelector((data) => data.laporan.data_tunai);
  const dataLaporanKredit = useSelector((data) => data.laporan.data_kredit);
  const totalTunai = useSelector((data) => data.laporan.total_tunai);
  const totalKredit = useSelector((data) => data.laporan.total_kredit);
  const [idKategori, setIdKategori] = useState(0);
  const [namaKategori, setNamaKategori] = useState("");
  const [idToko, setIdToko] = useState(0);
  const [namaToko, setNamaToko] = useState("");
  const [jenis, setJenis] = useState("");
  const [jenisID, setJenisID] = useState("");
  const [start, setStart] = useState(moment(Date.now()).format("yyyy-MM-DD"));
  const [end, setEnd] = useState(moment(Date.now()).format("yyyy-MM-DD"));
  const [loading, setLoading] = useState(false);

  const katArr = dataKategory.map((x) => {
    return {
      value: x.id,
      label: x.kategori,
    };
  });

  const tokoArr = dataToko.map((x) => {
    return {
      value: x.id,
      label: x.nama_toko,
    };
  });

  const handleCari = async () => {
    if (jenisID === "") {
      Swal.fire({
        icon: "error",
        title: "Perhatian",
        text: `Jenis Laporan Belum dipilih`,
      });
    } else {
      if (idToko === 0) {
        return Swal.fire({
          icon: "error",
          title: "Perhatian",
          text: `Toko Belum dipilih`,
        });
      }

      if (idKategori === 0) {
        return Swal.fire({
          icon: "error",
          title: "Perhatian",
          text: `Kategori Belum dipilih`,
        });
      }

      setLoading(true);
      dispatch(
        getLaporan(start, end, idToko, idKategori, jenisID, () => {
          setLoading(false);
        })
      );
    }
  };

  const handleItemToko = (e) => {
    if (e) {
      setIdToko(e.value);
      setNamaToko(e.label);
    } else {
      setIdToko(0);
      setNamaToko("");
    }
  };

  const handleItemKat = (e) => {
    if (e) {
      setIdKategori(e.value);
      setNamaKategori(e.label);
    } else {
      setIdKategori(0);
      setNamaKategori("");
    }
  };

  // console.log(dataUsers);

  const columns = [
    {
      name: "NO",
      selector: "nama_brg",
      width: "50px",
      cell: (item, i) => i + 1,
    },
    {
      name: "NAMA BARANG",
      selector: "nama_brg",
      sortable: true,
    },

    {
      name: "ITEM TERJUAL",
      selector: "jumlah_item",
      sortable: true,
      center: true,
      width: "100px",
    },

    {
      name: "SATUAN",
      selector: "nama_satuan",
      center: true,
      width: "100px",
    },

    {
      name: "HARGA SATUAN",
      selector: "harga_satuan",
      center: true,
      cell: (item) => convertRupiah.convert(item.harga_satuan),
    },

    {
      name: "DISKON",
      selector: "diskon",
      center: true,
      width: "100px",
      cell: (item) => `${item.diskon}%`,
    },
    {
      name: "HARGA SETELAH DISKON",
      selector: "harga_diskon",
      center: true,
      cell: (item) => convertRupiah.convert(item.harga_diskon),
    },
    {
      name: "TOTAL",
      selector: "harga_diskon",
      center: true,
      cell: (item) =>
        convertRupiah.convert(item.harga_diskon * item.jumlah_item),
    },
  ];

  const columnsKredit = [
    {
      name: "NO",
      selector: "nama_brg",
      width: "50px",
      cell: (item, i) => i + 1,
    },
    {
      name: "NO. KREDIT",
      selector: "no_penjualan",
      sortable: true,
      width: "200px",
    },

    {
      name: "TANGGAL",
      selector: "tgl_penjualan",
      sortable: true,
      center: true,
      width: "100px",
      cell: (item) => moment(item.tgl_penjualan).format("DD-MM-yyyy"),
    },

    {
      name: "KETERANGAN",
      selector: "nama_satuan",
      cell: (item) => (
        <>
          Nama Barang <Badge color="success">{item.nama_brg}</Badge>, Jumlah
          Barang{" "}
          <Badge color="primary">
            {item.jumlah_item} {item.nama_satuan}
          </Badge>
          , Harga{" "}
          <Badge color="primary">
            {convertRupiah.convert(item.harga_satuan)}
          </Badge>
          , Nama Pembeli <Badge color="warning">{item.nama_pembeli}</Badge>,
          Nama Kreditur <Badge color="success">{item.nama_kreditur}</Badge>, DP{" "}
          <Badge color="default">{convertRupiah.convert(item.dp)}</Badge>, Kasir
          yang melakukan Transaksi{" "}
          <Badge color="warning">{item.nama_karyawan}</Badge>.
        </>
      ),
    },
  ];

  const handleCetakLaporanTunai = useReactToPrint({
    content: () => cetakRef.current,
  });

  const penjualanTunai = () => {
    setJenisID("TUNAI");
    setJenis("Laporan Penjualan Tunai");
    dispatch({ type: GET_LAPORAN_TUNAI, data: { data: [], total: 0 } });
  };

  const penjualanNonTunai = () => {
    setJenisID("NONTUNAI");
    setJenis("Laporan Penjualan Non Tunai");
    dispatch({ type: GET_LAPORAN_TUNAI, data: { data: [], total: 0 } });
  };

  const penjualanKredit = () => {
    setJenisID("KREDIT");
    setJenis("Laporan Penjualan Kredit");
    dispatch({ type: GET_LAPORAN_KREDIT, data: { data: [], total: 0 } });
  };

  const rekapitulasi = () => {
    setJenisID("REKAPITULASI");
    setJenis("Rekapitulasi Penjualan");
    dispatch({ type: GET_LAPORAN_TUNAI, data: { data: [], total: 0 } });
  };
  useEffect(() => {
    // dispatch(getAllUsers());
    // console.log("OK");
    // dispatch(getAllUsers());
    dispatch({ type: GET_LAPORAN_TUNAI, data: { data: [], total: 0 } });
    dispatch({ type: GET_LAPORAN_KREDIT, data: { data: [], total: 0 } });
    dispatch(getKategori());
    dispatch(getToko());

    setJenis("Laporan dan Informasi Penjualan");
  }, []);

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <Col>
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">{jenis}</h3>
                  </div>
                  <div className="col text-right"></div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row style={{ marginBottom: 10 }}>
                  <Col>
                    <Button
                      outline
                      color="info"
                      onClick={() => penjualanTunai()}
                    >
                      # Laporan Penjualan Tunai
                    </Button>

                    <Button
                      outline
                      color="success"
                      onClick={() => penjualanNonTunai()}
                    >
                      # Laporan Penjualan NonTunai
                    </Button>

                    <Button
                      outline
                      color="primary"
                      onClick={() => penjualanKredit()}
                    >
                      # Laporan Penjualan Kredit
                    </Button>

                    <Button
                      outline
                      color="warning"
                      onClick={() => rekapitulasi()}
                    >
                      # Rekapitulasi Penjualan
                    </Button>
                  </Col>
                </Row>
                <Row className="mb-3">
                  <Col>
                    <Card>
                      <CardBody>
                        <div>
                          <Row>
                            <Col>
                              <div style={{ display: "flex" }}>
                                <div
                                  style={{
                                    display: "flex",
                                    marginRight: 5,
                                    alignItems: "center",
                                    justifyItems: "center",
                                  }}
                                >
                                  <Label style={{ marginRight: 10 }}>
                                    Priode
                                  </Label>
                                  <Input
                                    style={{
                                      padding: 18,
                                    }}
                                    type="date"
                                    size="sm"
                                    value={start}
                                    onChange={(e) => setStart(e.target.value)}
                                  />
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyItems: "center",
                                  }}
                                >
                                  <Label style={{ marginRight: 10 }}>S/D</Label>
                                  <Input
                                    style={{
                                      padding: 18,
                                    }}
                                    type="date"
                                    size="sm"
                                    value={end}
                                    onChange={(e) => setEnd(e.target.value)}
                                  />
                                </div>
                              </div>
                            </Col>
                            <Col>
                              <div style={{ display: "flex", fontSize: 12 }}>
                                <div style={{ width: "100%", marginRight: 20 }}>
                                  <Select
                                    options={[
                                      { id: 0, label: "All" },
                                      ...tokoArr,
                                    ]}
                                    placeholder="Toko..."
                                    isClearable
                                    onChange={(e) => handleItemToko(e)}
                                  />
                                </div>

                                <div style={{ width: "100%" }}>
                                  <Select
                                    options={[
                                      { id: 0, label: "All" },
                                      ...katArr,
                                    ]}
                                    placeholder="Jenis Barang..."
                                    isClearable
                                    onChange={(e) => handleItemKat(e)}
                                  />
                                </div>
                              </div>
                            </Col>
                            <Col xs={1}>
                              <Button
                                size="sm"
                                color="default"
                                style={{ padding: 8, width: 100 }}
                                onClick={() => handleCari()}
                              >
                                Cari
                              </Button>
                            </Col>
                          </Row>
                        </div>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Card style={{ marginBottom: 20 }}>
                      <CardBody>
                        {/* <DataTable
                          columns={columsToken}
                          data={dataToken}
                          pagination
                          noHeader
                        /> */}

                        <Row className="mb-3">
                          <Col>
                            <h4>{jenis}</h4>
                            <table style={{ fontSize: 10 }}>
                              <tr>
                                <td style={{ width: 100 }}>Priode</td>
                                <td style={{ width: 20 }}>:</td>
                                <td>
                                  {start} s/d {end}
                                </td>
                              </tr>
                              <tr>
                                <td style={{ width: 100 }}>Toko</td>
                                <td style={{ width: 20 }}>: </td>
                                <td>{namaToko}</td>
                              </tr>
                              <tr>
                                <td style={{ width: 100 }}>Jesni Barang</td>
                                <td style={{ width: 20 }}>: </td>
                                <td>{namaKategori}</td>
                              </tr>
                            </table>
                          </Col>
                          <Col xs={3}>
                            <h4>Total Penjualan</h4>
                            {jenisID === "KREDIT" ? (
                              <h1>{convertRupiah.convert(totalKredit)}</h1>
                            ) : (
                              <h1>{convertRupiah.convert(totalTunai)}</h1>
                            )}

                            {dataLaporanTunai.length > 0 && (
                              <Button
                                size="sm"
                                color="primary"
                                onClick={() => handleCetakLaporanTunai()}
                              >
                                <IoPrint style={{ marginRight: 10 }} />
                                Cetak Laporan
                              </Button>
                            )}

                            {/* {dataLaporanKredit.length > 0 && (
                              <Button size="sm" color="primary">
                                <IoPrint style={{ marginRight: 10 }} />
                                Cetak Laporan
                              </Button>
                            )} */}
                          </Col>
                        </Row>

                        <Row>
                          <BounceLoader
                            css={override}
                            color="#26dff6"
                            size={100}
                            loading={loading}
                          />
                          {jenisID === "KREDIT" ? (
                            <DataTable
                              data={dataLaporanKredit}
                              columns={columnsKredit}
                              pagination
                              responsive
                              striped
                              noHeader
                              noDataComponent={<NoDataBarang />}
                            />
                          ) : (
                            <DataTable
                              data={dataLaporanTunai}
                              columns={columns}
                              pagination
                              responsive
                              striped
                              noHeader
                              noDataComponent={<NoDataBarang />}
                            />
                          )}
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
      {dataLaporanTunai.length > 0 && (
        <div style={{ display: "none" }}>
          <CetakLaporanTunai
            ref={cetakRef}
            nama_laporan={jenis.toUpperCase()}
            start={start}
            end={end}
            nama_toko={namaToko}
            jenis={namaKategori}
            total={totalTunai}
            data={dataLaporanTunai}
          />
        </div>
      )}
    </>
  );
};

export default LaporanPenjualan;
