import React from "react";
import moment from "moment";
import convertRupiah from "rupiah-format";

class CetakLaporanTunai extends React.PureComponent {
  render() {
    return (
      <>
        <div
          style={{
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 30,
          }}
        >
          <p align="center" style={{ fontWeight: "bold" }}>
            <strong>{this.props.nama_laporan}</strong>
          </p>
          <table width={400} border={0} style={{ marginBottom: 20 }}>
            <tbody>
              <tr>
                <td>PRIODE </td>
                <td>
                  : {moment(this.props.start).format("DD/MM/yyyy")} S/D{" "}
                  {moment(this.props.end).format("DD/MM/yyyy")}{" "}
                </td>
              </tr>
              <tr>
                <td>NAMA TOKO </td>
                <td>: {this.props.nama_toko}</td>
              </tr>
              <tr>
                <td>JENIS BARANG</td>
                <td>: {this.props.jenis}</td>
              </tr>
            </tbody>
          </table>
          <table width="100%">
            <thead>
              <tr
                style={{
                  borderBottom: "1px solid #7f8c8d",
                  borderTop: "1px solid #7f8c8d",
                  backgroundColor: "rgba(38,223,246, 1)",
                  color: "#576574",
                }}
              >
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>NO</strong>
                  </div>
                </th>
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>NAMA BARANG </strong>
                  </div>
                </th>
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>TERJUAL</strong>
                  </div>
                </th>
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>SATUAN </strong>
                  </div>
                </th>
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>HARGA SATUAN</strong>
                  </div>
                </th>
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>DISKON</strong>
                  </div>
                </th>
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>HARGA SETELAH DISKON</strong>
                  </div>
                </th>
                <th style={{ padding: "10px 0px 10px 0px" }}>
                  <div align="center">
                    <strong>HARGA TOTAL</strong>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item, i) => (
                <tr
                  style={{
                    borderBottom: "1px solid #c8d6e5",
                    color: "#576574",
                    padding: "10px 0px 10px 0px",
                  }}
                  // style={{ padding: "10px 0px 10px 0px" }}
                >
                  <td
                    style={{
                      textAlign: "center",
                      padding: "10px 0px 10px 0px",
                    }}
                  >
                    {i + 1}
                  </td>
                  <td
                    style={{
                      paddingLeft: 10,
                      padding: "10px 0px 10px 0px",
                    }}
                  >
                    {item.nama_brg}
                  </td>
                  <td
                    style={{
                      textAlign: "center",
                      padding: "10px 0px 10px 0px",
                    }}
                  >
                    {item.jumlah_item}
                  </td>
                  <td
                    style={{
                      textAlign: "center",
                      padding: "10px 0px 10px 0px",
                    }}
                  >
                    {item.nama_satuan}
                  </td>
                  <td
                    style={{
                      textAlign: "center",
                      padding: "10px 0px 10px 0px",
                    }}
                  >
                    {item.nama_satuan}
                  </td>
                  <td
                    style={{
                      paddingRight: 10,
                      padding: "10px 0px 10px 0px",
                    }}
                  >
                    {convertRupiah.convert(item.harga_satuan)}
                  </td>
                  <td
                    style={{
                      paddingRight: 10,
                      padding: "10px 0px 10px 0px",
                    }}
                  >
                    {convertRupiah.convert(item.harga_diskon)}
                  </td>
                  <td style={{ padding: "10px 0px 10px 0px" }}>
                    {convertRupiah.convert(
                      item.harga_diskon * item.jumlah_item
                    )}
                  </td>
                </tr>
              ))}
              <tr>
                <td
                  style={{
                    padding: "10px 0px 10px 0px",
                    fontWeight: "bold",
                    textAlign: "left",
                  }}
                  colSpan={7}
                >
                  TOTAL PENJUALAN
                </td>
                <td
                  style={{ padding: "10px 0px 10px 0px", fontWeight: "bold" }}
                >
                  {convertRupiah.convert(this.props.total)}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default CetakLaporanTunai;
