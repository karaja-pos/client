import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Button,
  Col,
  Badge,
} from "reactstrap";
import Select from "react-select";

import DataTable from "react-data-table-component";
import { useDispatch, useSelector } from "react-redux";
import { getAllUsers } from "redux/actions/Auth";
import { getAllToken } from "redux/actions/Auth";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Akun = () => {
  const dispatch = useDispatch();
  const dataUsers = useSelector((x) => x.auth.users);
  const dataToken = useSelector((x) => x.auth.tokens);
  // console.log(dataUsers);

  const colums = [
    {
      name: "NAMA AKUN",
      selector: "email",
      sortable: true,
    },
    {
      name: "AKSES",
      selector: "role",
      sortable: true,
      center: true,
      width: "100px",
      cell: (data) => (
        <>
          {data.role === "admin" && <Badge color="danger">Admin</Badge>}
          {data.role === "staf" && <Badge color="info">Staff</Badge>}
          {data.role === "toko" && <Badge color="success">Toko</Badge>}
          {data.role === "user" && <Badge color="primary">User</Badge>}
        </>
      ),
    },
    {
      name: "USERNAME",
      selector: "username",
      sortable: true,
      center: true,
      width: "100px",
    },
    {
      name: "AKSI",
      selector: "",
      sortable: true,
      center: true,
    },
  ];

  const columsToken = [
    {
      name: "TOKEN",
      selector: "token",
      sortable: true,
    },
    {
      name: "NAMA TOKO",
      selector: "nama_toko",
      sortable: true,
      center: true,
    },
  ];

  useEffect(() => {
    // dispatch(getAllUsers());
    // console.log("OK");
    dispatch(getAllUsers());
    dispatch(getAllToken());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <Col md={7}>
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Seting Akun</h3>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card style={{ marginBottom: 20 }}>
                      <CardBody>
                        <DataTable
                          columns={colums}
                          data={dataUsers}
                          pagination
                          noHeader
                        />
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>

          <Col md={5}>
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Token App</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" size="sm">
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card style={{ marginBottom: 20 }}>
                      <CardBody>
                        <DataTable
                          columns={columsToken}
                          data={dataToken}
                          pagination
                          noHeader
                        />
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Akun;
