import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

import { IoEye, IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import ModalTambah from "./ModalTambah";
import ModalUpdate from "./ModalUpdate";
import { getMerek, setActiveMerek } from "redux/actions/Merek";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Merek = () => {
  const dispatch = useDispatch();

  const dataMerek = useSelector((data) => data.merek.merek);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemMerek, setItemMerek] = useState(null);

  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const filterMerek = dataMerek.filter((data) => {
    return data.merek.toLowerCase().indexOf(cari) !== -1;
  });

  const setItemUpdate = (data) => {
    setItemMerek(data);
    toggleUpdate();
  };

  useEffect(() => {
    dispatch(getMerek());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Merek</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Merek</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      {filterMerek.map((data, id) => (
                        <tr key={id}>
                          <td>{id + 1}</td>
                          <td>{data.merek}</td>
                          <td>
                            <CustomInput
                              type="switch"
                              id={`${data.merek.trim().toLowerCase()}_${
                                data.id
                              }`}
                              name={`${data.merek.trim().toLowerCase()}_${
                                data.id
                              }`}
                              checked={data.status === "Y" ? true : false}
                              onClick={() =>
                                dispatch(
                                  setActiveMerek(
                                    data.id,
                                    data.status === "Y" ? "N" : "Y"
                                  )
                                )
                              }
                              label="Active/Tidak"
                            />
                          </td>
                          <td>
                            <Button
                              color="info"
                              size="sm"
                              onClick={() => setItemUpdate(data)}
                            >
                              <IoEye />
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalTambah modal={modal} toggle={toggle} />
      {itemMerek && (
        <ModalUpdate
          modal={modalUpdate}
          toggle={toggleUpdate}
          item={itemMerek}
        />
      )}
    </>
  );
};

export default Merek;
