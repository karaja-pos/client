import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
} from "reactstrap";
import { updateMerek } from "redux/actions/Merek";

const ModalUpdate = ({ modal, toggle, item }) => {
  const dispatch = useDispatch();
  const [id, setId] = useState("");
  const [nama, setNama] = useState("");
  const [namaErr, setNamaErr] = useState(false);

  const handleUpdate = () => {
    nama === "" ? setNamaErr(true) : setNamaErr(false);

    if (nama !== "") {
      dispatch(updateMerek(id, nama));
      toggle();
    }
  };

  useEffect(() => {
    setId(item.id);
    setNama(item.merek);
  }, [item.id]);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Update Merek
        </h5>
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label for="posisi">Nama Merek</Label>
          <Input
            invalid={namaErr}
            type="text"
            name="nama"
            id="nama"
            value={nama}
            onChange={(e) => setNama(e.target.value)}
            placeholder="Masukan nama merek"
          />
          {namaErr && <FormFeedback>Masih kosong</FormFeedback>}
        </FormGroup>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleUpdate()}>
          Update
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalUpdate;
