import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

import { IoEye, IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { getSatuan } from "redux/actions/Satuan";
import ModalTambah from "./ModalTambah";
import { setActiveSatuan } from "redux/actions/Satuan";
import ModalUpdate from "./ModalUpdate";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Satuan = () => {
  const dispatch = useDispatch();

  const dataSatuan = useSelector((data) => data.satuan.satuan);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemSatuan, setItemSatuan] = useState(null);

  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const filterSatuan = dataSatuan.filter((data) => {
    return data.nama_satuan.toLowerCase().indexOf(cari) !== -1;
  });

  const setItemUpdate = (data) => {
    setItemSatuan(data);
    toggleUpdate();
  };

  useEffect(() => {
    dispatch(getSatuan());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Satuan Barang</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Satuan</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      {filterSatuan.map((data, id) => (
                        <tr key={id}>
                          <td>{id + 1}</td>
                          <td>{data.nama_satuan}</td>
                          <td>
                            <CustomInput
                              type="switch"
                              id={`${data.nama_satuan.trim().toLowerCase()}_${
                                data.id
                              }`}
                              name={`${data.nama_satuan.trim().toLowerCase()}_${
                                data.id
                              }`}
                              checked={data.status === "Y" ? true : false}
                              onClick={() =>
                                dispatch(
                                  setActiveSatuan(
                                    data.id,
                                    data.status === "Y" ? "N" : "Y"
                                  )
                                )
                              }
                              label="Active/Tidak"
                            />
                          </td>
                          <td>
                            <Button
                              color="info"
                              size="sm"
                              onClick={() => setItemUpdate(data)}
                            >
                              <IoEye />
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalTambah modal={modal} toggle={toggle} />
      {itemSatuan && (
        <ModalUpdate
          modal={modalUpdate}
          toggle={toggleUpdate}
          item={itemSatuan}
        />
      )}
    </>
  );
};

export default Satuan;
