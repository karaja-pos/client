import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Row,
  Col,
} from "reactstrap";
import { tambahSupplier } from "redux/actions/Supplier";

const ModalTambah = ({ modal, toggle, kode }) => {
  const dispatch = useDispatch();
  const [nama, setNama] = useState("");
  const [namaErr, setNamaErr] = useState(false);
  const [alamat, setAlamat] = useState("");
  const [alamatErr, setAlamatErr] = useState(false);
  const [telp, setTelp] = useState("");
  const [telpErr, setTelpErr] = useState(false);

  const handleSimpan = () => {
    nama === "" ? setNamaErr(true) : setNamaErr(false);
    alamat === "" ? setAlamatErr(true) : setAlamatErr(false);
    telp === "" ? setTelpErr(true) : setTelpErr(false);

    if (nama !== "" && alamat !== "" && telp !== "") {
      let data = {
        kode_supplier: kode,
        nama_supplier: nama,
        alamat_supplier: alamat,
        telp_supplier: telp,
        user: "",
      };
      dispatch(tambahSupplier(data));
      kosong();
      toggle();
    }
  };

  const kosong = () => {
    setNama("");
    setAlamat("");
    setTelp("");
  };

  useEffect(() => {});
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Tambahkan Supplier
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="kode">Kode Supplier</Label>
              <Input
                type="text"
                name="kode"
                id="kode"
                disabled
                placeholder="Kode Supplier"
                value={kode}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="nama">Nama Supplier</Label>
              <Input
                invalid={namaErr}
                type="text"
                name="nama"
                id="nama"
                placeholder="Masukan Nama Supplier"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
              {namaErr && (
                <FormFeedback>Nama supplier masih kosong</FormFeedback>
              )}
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="alamat">Alamat</Label>
          <Input
            invalid={alamatErr}
            type="text"
            name="alamat"
            id="alamat"
            value={alamat}
            onChange={(e) => setAlamat(e.target.value)}
            placeholder="Masukan alamat Supplier"
          />
          {alamatErr && (
            <FormFeedback>Alamat supploer masih kosong</FormFeedback>
          )}
        </FormGroup>
        <FormGroup>
          <Label for="telp">Nomor Telephone</Label>
          <Input
            invalid={telpErr}
            type="telp"
            name="telp"
            id="telp"
            placeholder="+6208xxx"
            value={telp}
            onChange={(e) => setTelp(e.target.value)}
          />
          {telpErr && (
            <FormFeedback>Nomor Telp supplier masih kosong</FormFeedback>
          )}
        </FormGroup>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleSimpan()}>
          Tambahkan
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalTambah;
