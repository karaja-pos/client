import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  Col,
} from "reactstrap";
import DataTable from "react-data-table-component";

import { IoEye, IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import ModalTambah from "./ModalTambah";
import ModalUpdate from "./ModalUpdate";
import { getSupplier, blokirSupplier } from "redux/actions/Supplier";
import { setKodeSupplier } from "../../../utils";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Supplier = () => {
  const dispatch = useDispatch();

  const dataSupplier = useSelector((data) => data.supplier.supplier);
  const [cari, setCari] = useState("");
  const [kode, setKode] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemSupplier, setItemSupplier] = useState(null);

  const toggle = () => {
    setKode(`SPL${setKodeSupplier(dataSupplier.length + 1)}`);
    setModal(!modal);
  };
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const setItemUpdate = (data) => {
    setItemSupplier(data);
    toggleUpdate();
  };

  const filterSupplier = dataSupplier.filter((data) => {
    return data.nama_supplier.toLowerCase().indexOf(cari) !== -1;
  });

  const columns = [
    {
      name: "KODE",
      selector: "kode_supplier",
      sortable: true,
    },
    {
      name: "NAMA SUPPLIER",
      selector: "nama_supplier",
      sortable: true,
    },
    {
      name: "ALAMAT",
      selector: "alamat_supplier",
      sortable: true,
    },
    {
      name: "TELP",
      selector: "telp_supplier",
      sortable: true,
    },
    {
      name: "BLOKIR",
      selector: "blokir",
      center: true,
      cell: (item) => (
        <CustomInput
          type="switch"
          id={item.kode_supplier}
          name={item.kode_supplier}
          checked={item.blokir === "Y" ? true : false}
          onClick={() =>
            dispatch(blokirSupplier(item.id, item.blokir === "Y" ? "N" : "Y"))
          }
          label="Tidak/Ya"
        />
      ),
    },

    {
      name: "AKSI",
      selector: "aksi",
      center: true,
      cell: (item) => (
        <Button color="info" size="sm" onClick={() => setItemUpdate(item)}>
          <IoEye />
        </Button>
      ),
    },
  ];
  //   const data = [{ id: 1, title: "Conan the Barbarian", year: "1982" }];

  useEffect(() => {
    dispatch(getSupplier());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Data Supplier</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <DataTable
                    className="align-items-center table-flush"
                    columns={columns}
                    data={filterSupplier}
                    responsive={true}
                    striped={true}
                    direction="ltr"
                    noHeader={true}
                    fixedHeader={true}
                    pagination={true}
                    style={{
                      headCells: {
                        style: {
                          backgroundColor: "#f6f9fc",
                          color: "#8898aa",
                        },
                      },
                    }}
                  />
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalTambah modal={modal} toggle={toggle} kode={kode} />
      {itemSupplier && (
        <ModalUpdate
          modal={modalUpdate}
          toggle={toggleUpdate}
          item={itemSupplier}
        />
      )}
    </>
  );
};

export default Supplier;
