import React from "react";
import "./idcard.css";
import QRCode from "react-qr-code";

class IdCard extends React.PureComponent {
  render() {
    const data = this.props.data;
    console.log(data);
    return (
      <div
        style={{
          position: "relative",
          width: "3.37in",
        }}
      >
        <div
          style={{
            marginTop: 20,
            marginLeft: 20,
            width: "100%",
          }}
        >
          <p style={{ fontWeight: "bolder", color: "#FFF" }}>
            {data.nama_toko}
          </p>
        </div>
        <div
          style={{
            marginTop: 200,
            width: "100%",
          }}
        >
          <p style={{ textAlign: "center", fontWeight: "bolder" }}>
            {data.nama_karyawan}
          </p>
          <p style={{ textAlign: "center", lineHeight: 0 }}>{data.posisi}</p>
        </div>
        <div
          style={{
            position: "absolute",
            top: 335,
            left: 120,
            background: "#FFF",
          }}
        >
          <QRCode value={data.kode_karyawan} size={80} bgColor="#FFF" />
        </div>
      </div>
    );
  }
}

export default IdCard;
