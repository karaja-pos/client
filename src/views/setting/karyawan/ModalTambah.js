import React, { createRef, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Row,
  Col,
  Media,
} from "reactstrap";
import { tambahKaryawan } from "redux/actions/Karyawan";
import { getPosisi } from "redux/actions/Posisi";
import { getToko } from "redux/actions/Toko";
import { setKodeKaryawan } from "utils";
import NoPhoto from "../../../assets/img/no_photo.jpg";

const ModalTambah = ({ modal, toggle, karyawan }) => {
  const fileRef = useRef(null);
  const datatoko = useSelector((data) => data.toko.toko);
  const dataPosisi = useSelector((data) => data.posisi.posisi);
  const dispatch = useDispatch();
  const [kodeErr, setKodeErr] = useState(false);
  const [nama, setNama] = useState("");
  const [namaErr, setNamaErr] = useState(false);
  const [file, setFile] = useState(null);
  const [img, setImg] = useState("");
  const [posisi, setposisi] = useState(0);
  const [posisiErr, setposisiErr] = useState(false);
  const [toko, settoko] = useState(0);
  const [tokoErr, settokoErr] = useState(false);
  const [kode, setKode] = useState("");
  const [nohp, setNohp] = useState("");
  const [nohpErr, setNohpErr] = useState("");

  const handleSimpan = () => {
    kode === "" ? setKodeErr(true) : setKodeErr(false);
    nama === "" ? setNamaErr(true) : setNamaErr(false);
    posisi === 0 ? setposisiErr(true) : setposisiErr(false);
    toko === 0 ? settokoErr(true) : settokoErr(false);
    nohp === "" ? setNohpErr(true) : setNohpErr(false);

    if (
      kode !== "" &&
      nama !== "" &&
      posisi !== 0 &&
      toko !== 0 &&
      nohp !== 0
    ) {
      // let data =
      //   toko: nama,
      //   user: "",
      // };
      const formData = new FormData();
      formData.append("kode_karyawan", kode);
      formData.append("nama_karyawan", nama);
      formData.append("nomor_hp", nohp);
      formData.append("id_posisi", posisi);
      formData.append("id_toko", toko);
      formData.append("file", file == null ? "" : file);
      formData.append("user", "");
      dispatch(tambahKaryawan(formData));
      toggle();
      reset();
    }
  };

  const reset = () => {
    setKode("");
    setNama("");
    setposisi(0);
    settoko(0);
    setFile(null);
    setImg("");
    setNohp("");
  };

  const handleOnChange = (e) => {
    // console.log("changed");
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      setFile(file);
      setImg(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleOpenFile = () => {
    if (fileRef !== null) {
      fileRef.current.click();
    }
  };

  useEffect(() => {
    setKode(`KRY${setKodeKaryawan(karyawan + 1)}`);
    dispatch(getPosisi());
    dispatch(getToko());
  }, [karyawan + 1]);

  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      // style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Tambah Karyawan
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row className="mb-3">
          <Col>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Media
                center
                onClick={handleOpenFile}
                style={{ cursor: "pointer" }}
              >
                <Media
                  style={{
                    width: 100,
                    height: 100,
                    borderRadius: "50%",
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                  object
                  src={file === null ? NoPhoto : img}
                  alt="Generic placeholder image"
                />
                <input
                  style={{ display: "none" }}
                  type="file"
                  name="file"
                  onChange={(e) => handleOnChange(e)}
                  ref={fileRef}
                  id="photo"
                />
              </Media>
            </div>
          </Col>
        </Row>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="kode">Kode</Label>
              <Input
                type="text"
                name="kode"
                id="kode"
                disabled
                placeholder="Kode Karyawan"
                onChange={(e) => setKode(e.target.value)}
                value={kode}
              />
              {kodeErr && <FormFeedback>Masih kosong</FormFeedback>}
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="posisi">Posisi / Level</Label>
              <Input
                type="select"
                name="posisi"
                id="posisi"
                value={posisi}
                invalid={posisiErr}
                onChange={(e) => setposisi(e.target.value)}
              >
                <option value="0">-- Pilik posisi --</option>
                {dataPosisi.map((data) => (
                  <option key={data.id} value={data.id}>
                    {data.posisi}
                  </option>
                ))}
              </Input>
              {posisiErr && <FormFeedback>Belum dipilih</FormFeedback>}
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="nama">Nama Karyawan</Label>
          <Input
            invalid={namaErr}
            type="text"
            name="nama"
            id="nama"
            value={nama}
            onChange={(e) => setNama(e.target.value)}
            placeholder="Masukan nama Karyawan"
          />
          {namaErr && <FormFeedback>Masih kosong</FormFeedback>}
        </FormGroup>

        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="toko">Toko</Label>
              <Input
                invalid={tokoErr}
                type="select"
                name="toko"
                id="toko"
                value={toko}
                onChange={(e) => settoko(e.target.value)}
              >
                <option value="0">-- Pilik toko --</option>
                {datatoko.map((data) => (
                  <option key={data.id} value={data.id}>
                    {data.nama_toko}
                  </option>
                ))}
              </Input>
              {tokoErr && <FormFeedback>Belum dipilih</FormFeedback>}
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="nomor_hp">Nomor HP</Label>
              <Input
                invalid={nohpErr}
                type="text"
                name="nomor_hp"
                id="nomor_hp"
                value={nohp}
                onChange={(e) => setNohp(e.target.value)}
                placeholder="Masukan Nomor HP"
              />
              {nohpErr && <FormFeedback>Masih kosong</FormFeedback>}
            </FormGroup>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleSimpan()}>
          Tambahkan
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalTambah;
