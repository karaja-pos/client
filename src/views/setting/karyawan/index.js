import React, { useEffect, useRef, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  CardImg,
  CardTitle,
  CardSubtitle,
  CardText,
  Media,
} from "reactstrap";
import DataTable from "react-data-table-component";
import { IoCardOutline, IoEye, IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import ModalTambah from "./ModalTambah";
import ModalUpdate from "./ModalUpdate";
import { NoDataBarang, KaryawanComponent } from "utils/component";
import { getKaryawan } from "redux/actions/Karyawan";
import { blokirKaryawan } from "redux/actions/Karyawan";
import IdCard from "./IdCard";
import { useReactToPrint } from "react-to-print";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Karyawan = () => {
  const dispatch = useDispatch();

  const dataKaryawan = useSelector((data) => data.karyawan.karyawan);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemBarang, setItemBarang] = useState(null);
  const cardRef = useRef();
  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);
  const [selectedKaryawan, setSelectedKaryawan] = useState(null);

  const filterKaryawan = dataKaryawan.filter((data) => {
    return (
      data.nama_karyawan.toLowerCase().indexOf(cari) !== -1 ||
      data.kode_karyawan.toLowerCase().indexOf(cari) !== -1
    );
  });

  const selectedData = async (item) => {
    await setSelectedKaryawan(item);
    await handleCetakIdCard();
  };

  const handleCetakIdCard = useReactToPrint({
    content: () => cardRef.current,
  });

  const columns = [
    {
      name: "KODE",
      selector: "kode_karyawan",
      sortable: true,
      width: "150px",
    },
    {
      name: "NAMA KARYAWAN",
      selector: "nama_karyawan",
      sortable: true,
    },
    {
      name: "POSISI",
      selector: "posisi",
      sortable: true,
      center: true,
      width: "120px",
    },
    {
      name: "TOKO",
      selector: "nama_toko",
      sortable: true,
      width: "200px",
    },

    {
      name: "BLOKIR",
      selector: "nama_toko",
      sortable: true,
      width: "200px",
      cell: (item) => (
        <CustomInput
          type="switch"
          id={item.kode_karyawan}
          name={item.kode_karyawan}
          checked={item.blokir === "Y" ? true : false}
          onClick={() =>
            dispatch(blokirKaryawan(item.id, item.blokir === "Y" ? "N" : "Y"))
          }
          label="Tidak/Ya"
        />
      ),
    },
    {
      name: "AKSI",
      selector: "aksi",
      width: "120px",
      center: true,
      cell: (item) => (
        <>
          <Button color="default" size="sm" onClick={() => selectedData(item)}>
            <IoCardOutline />
          </Button>
          <Button color="info" size="sm" onClick={() => setItemUpdate(item)}>
            <IoEye />
          </Button>
        </>
      ),
    },
  ];

  const setItemUpdate = (data) => {
    setItemBarang(data);
    toggleUpdate();
  };

  useEffect(() => {
    dispatch(getKaryawan());
  }, []);

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Karyawan</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <DataTable
                    className="align-items-center table-flush"
                    columns={columns}
                    data={filterKaryawan}
                    responsive={true}
                    striped={true}
                    direction="ltr"
                    noHeader={true}
                    expandableRows
                    expandOnRowClicked={true}
                    fixedHeader={true}
                    pagination={true}
                    highlightOnHover
                    noDataComponent={<NoDataBarang />}
                    expandableRowsComponent={<KaryawanComponent />}
                    style={{
                      headCells: {
                        style: {
                          backgroundColor: "#f6f9fc",
                          color: "#8898aa",
                        },
                      },
                    }}
                  />
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalTambah
        modal={modal}
        toggle={toggle}
        karyawan={dataKaryawan.length}
      />
      {itemBarang && (
        <ModalUpdate
          modal={modalUpdate}
          toggle={toggleUpdate}
          item={itemBarang}
        />
      )}

      <div style={{ display: "none" }}>
        {selectedKaryawan && <IdCard ref={cardRef} data={selectedKaryawan} />}
      </div>
    </>
  );
};

export default Karyawan;
