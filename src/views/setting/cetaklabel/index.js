import React, { useEffect, useRef, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  CardImg,
  CardTitle,
  CardSubtitle,
  CardText,
  Media,
} from "reactstrap";
import Select from "react-select";
import convertRupiah from "rupiah-format";
import DataTable from "react-data-table-component";
import { IoPrintOutline, IoSearchOutline } from "react-icons/io5";
import { useReactToPrint } from "react-to-print";
import { useDispatch, useSelector } from "react-redux";
import ModalUpdate from "./ModalUpdate";
import { NoDataBarang, BarangComponent } from "utils/component";
import { getKategori } from "redux/actions/Kategori";
import { getToko } from "redux/actions/Toko";
import { getStokToko } from "redux/actions/Barang";
import LabelBarang from "./LabelBarang";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const CetakLabel = () => {
  const dispatch = useDispatch();

  const dataToko = useSelector((data) => data.toko.toko);
  const dataBarang = useSelector((data) => data.barang.barang);
  const dataKategory = useSelector((data) => data.kategori.kategori);
  const dataStokToko = useSelector((data) => data.barang.stok_toko);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [idKategori, setIdKategori] = useState(0);
  const [idToko, setIdToko] = useState(0);
  const [selectBarang, setSelectBarang] = useState(null);

  const labelRef = useRef();
  const cardRef = useRef();

  const katArr = dataKategory.map((x) => {
    return {
      value: x.id,
      label: x.kategori,
    };
  });

  const tokoArr = dataToko.map((x) => {
    return {
      value: x.id,
      label: x.nama_toko,
    };
  });

  const filterBarang = dataStokToko.filter((data) => {
    return (
      data.nama_barang.toLowerCase().indexOf(cari) !== -1 ||
      data.kode_brg.toLowerCase().indexOf(cari) !== -1
    );
  });

  const filterItemKat = filterBarang.filter((data) => {
    if (idKategori === 0) {
      return true;
    } else {
      return data.id_kategori === idKategori;
    }
  });

  const handleItemKat = (e) => {
    if (e) {
      setIdKategori(e.value);
    } else {
      setIdKategori(0);
    }
  };

  const handleItemToko = (e) => {
    if (e) {
      setIdToko(e.value);
      dispatch(getStokToko(e.value));
    } else {
      dispatch(getStokToko(0));
      setIdToko(0);
    }
  };

  const columns = [
    {
      name: "KODE",
      selector: "kode_brg",
      sortable: true,
      width: "150px",
    },
    {
      name: "NAMA BARANG",
      selector: "nama_barang",
      sortable: true,
    },

    {
      name: "HARGA",
      selector: "harga_jual",
      sortable: true,
      cell: (data) => convertRupiah.convert(data.harga_jual),
    },

    {
      name: "KATEGORI",
      selector: "kategori",
      width: "200px",
      center: true,
    },
  ];

  const handleCetakLabelBarang = useReactToPrint({
    content: () => labelRef.current,
  });

  useEffect(() => {
    // dispatch(getBarang());
    dispatch(getKategori());
    dispatch(getToko());
    dispatch(getStokToko(0));
  }, []);

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <Col>
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Cetak Label Barang</h3>
                  </div>
                  <div className="col text-right">
                    <Button
                      color="default"
                      disabled={
                        selectBarang && selectBarang.length > 0 ? false : true
                      }
                      onClick={() => handleCetakLabelBarang()}
                    >
                      <IoPrintOutline size={20} style={{ marginRight: 10 }} />
                      cetak
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card style={{ marginBottom: 20 }}>
                      <CardBody>
                        <Row>
                          <Col>
                            <div
                              style={{
                                fontSize: 12,
                                fontWeight: "bold",
                                padding: 0,
                              }}
                            >
                              <Select
                                options={tokoArr}
                                placeholder="Pilih Toko..."
                                isClearable
                                onChange={(e) => handleItemToko(e)}
                              />
                            </div>
                          </Col>
                          <Col>
                            <InputGroup size="sm">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <IoSearchOutline />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Nama / Kode Barang..."
                                style={{ paddingTop: 18, paddingBottom: 18 }}
                                onChange={(e) => setCari(e.target.value)}
                              />
                            </InputGroup>
                          </Col>
                          <Col>
                            <div
                              style={{
                                fontSize: 12,
                                fontWeight: "bold",
                                padding: 0,
                              }}
                            >
                              <Select
                                options={katArr}
                                placeholder="Pilih Kategori..."
                                isClearable
                                onChange={(e) => handleItemKat(e)}
                              />
                            </div>
                          </Col>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>

                <Row className="info-toko">
                  <DataTable
                    className="align-items-center table-flush"
                    columns={columns}
                    data={filterItemKat}
                    responsive={true}
                    striped={true}
                    direction="ltr"
                    noHeader={true}
                    selectableRows={true}
                    fixedHeader={true}
                    pagination={true}
                    highlightOnHover
                    clearSelectedRows={selectBarang === null ? true : false}
                    onSelectedRowsChange={(a) =>
                      setSelectBarang(a.selectedRows)
                    }
                    noDataComponent={<NoDataBarang />}
                    style={{
                      headCells: {
                        style: {
                          backgroundColor: "#f6f9fc",
                          color: "#8898aa",
                        },
                      },
                    }}
                  />
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
      <div style={{ display: "none" }}>
        {selectBarang && selectBarang.length > 0 && (
          <LabelBarang ref={labelRef} data={selectBarang} />
        )}
      </div>
    </>
  );
};

export default CetakLabel;
