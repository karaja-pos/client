import React from "react";
import convertRupiah from "rupiah-format";
import QRCode from "react-qr-code";
import "./print.css";

class LabelBarang extends React.PureComponent {
  render() {
    const data = this.props.data;
    return (
      <div>
        {data.map((x) => (
          <>
            <div
              style={{
                width: 285,
                height: 112,
                display: "flex",
                justifyContent: "space-between",
                // border: "1px solid grey",
                padding: 20,
                boxSizing: "border-box",
                marginBottom: 10,
              }}
            >
              <div style={{ paddingTop: 5, paddingBottom: 5, width: "100%" }}>
                <p
                  style={{
                    lineHeight: 0,
                    fontSize: 12,
                    fontWeight: "bold",
                    color: "#000",
                  }}
                >
                  {x.nama_toko}
                </p>
                <p
                  style={{
                    lineHeight: 0,
                    fontSize: 10,
                    color: "#000",
                    fontWeight: "bold",
                  }}
                >
                  {x.nama_barang}
                </p>
                <p
                  style={{
                    lineHeight: 0,
                    fontSize: 10,
                    color: "#000",
                    fontWeight: "bold",
                  }}
                >
                  {x.merek}
                </p>

                <p
                  style={{
                    lineHeight: 0,
                    fontWeight: "bold",
                    //   textAlign: "center",
                    fontSize: 20,
                    color: "#000",
                  }}
                >
                  {convertRupiah.convert(x.harga_jual)}
                </p>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <QRCode value={x.kode_brg} size={70} />
              </div>
            </div>
          </>
        ))}
      </div>
    );
  }
}

export default LabelBarang;
