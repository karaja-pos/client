import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

import { IoEye, IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import ModalTambah from "./ModalTambah";
import ModalUpdate from "./ModalUpdate";
import { getKategori, setActiveKategori } from "redux/actions/Kategori";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Kategori = () => {
  const dispatch = useDispatch();

  const datakategoru = useSelector((data) => data.kategori.kategori);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemKategori, setItemKategori] = useState(null);

  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const filterKategori = datakategoru.filter((data) => {
    return data.kategori.toLowerCase().indexOf(cari) !== -1;
  });

  const setItemUpdate = (data) => {
    setItemKategori(data);
    toggleUpdate();
  };

  useEffect(() => {
    dispatch(getKategori());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Kategori Barang</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Kategori</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      {filterKategori.map((data, id) => (
                        <tr key={id}>
                          <td>{id + 1}</td>
                          <td>{data.kategori}</td>
                          <td>
                            <CustomInput
                              type="switch"
                              id={`${data.kategori.trim().toLowerCase()}_${
                                data.id
                              }`}
                              name={`${data.kategori.trim().toLowerCase()}_${
                                data.id
                              }`}
                              checked={data.status === "Y" ? true : false}
                              onClick={() =>
                                dispatch(
                                  setActiveKategori(
                                    data.id,
                                    data.status === "Y" ? "N" : "Y"
                                  )
                                )
                              }
                              label="Active/Tidak"
                            />
                          </td>
                          <td>
                            <Button
                              color="info"
                              size="sm"
                              onClick={() => setItemUpdate(data)}
                            >
                              <IoEye />
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalTambah modal={modal} toggle={toggle} />
      {itemKategori && (
        <ModalUpdate
          modal={modalUpdate}
          toggle={toggleUpdate}
          item={itemKategori}
        />
      )}
    </>
  );
};

export default Kategori;
