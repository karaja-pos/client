import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  Col,
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Button,
} from "reactstrap";
import { updateTokoById } from "redux/actions/Toko";
// import { isTemplateExpression } from "typescript";

const ModalDetailToko = ({ modal, toggle, data }) => {
  const dispatch = useDispatch();
  const [id, setId] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telp, setTelp] = useState("");

  const [namaErr, setNamaErr] = useState(false);
  const [alamatErr, setAlamatErr] = useState(false);
  const [telpErr, setTelpErr] = useState(false);
  const handleUpdate = () => {
    nama === "" ? setNamaErr(true) : setNamaErr(false);
    alamat === "" ? setAlamatErr(true) : setAlamatErr(false);
    telp === "" ? setTelpErr(true) : setTelpErr(false);

    if (nama !== "" && alamat !== "" && telp !== "") {
      const data = {
        nama_toko: nama,
        alamat: alamat,
        telp: telp,
      };
      dispatch(updateTokoById(data, id));
      toggle();
    }
  };

  useEffect(() => {
    setId(data.id);
    setNama(data.nama_toko);
    setAlamat(data.alamat);
    setTelp(data.telp);
  }, [data.kode_toko]);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Detail Toko
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="kode">Kode Toko</Label>
              <Input
                type="text"
                name="kode"
                id="kode"
                disabled
                placeholder="Kode Toko"
                value={data.kode_toko}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="nama">Nama Toko</Label>
              <Input
                invalid={namaErr}
                type="text"
                name="nama"
                id="nama"
                placeholder="Masukan Nama Toko"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
              {namaErr && <FormFeedback>Nama toko masih kosong</FormFeedback>}
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="alamat">Alamat</Label>
          <Input
            invalid={alamatErr}
            type="text"
            name="alamat"
            id="alamat"
            value={alamat}
            onChange={(e) => setAlamat(e.target.value)}
            placeholder="Masukan alamat toko"
          />
          {alamatErr && <FormFeedback>Alamat toko masih kosong</FormFeedback>}
        </FormGroup>
        <FormGroup>
          <Label for="telp">Nomor Telephone</Label>
          <Input
            invalid={telpErr}
            type="telp"
            name="telp"
            id="telp"
            placeholder="+6208xxx"
            value={telp}
            onChange={(e) => setTelp(e.target.value)}
          />
          {telpErr && <FormFeedback>Nomor Telp toko masih kosong</FormFeedback>}
        </FormGroup>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleUpdate()}>
          Update
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalDetailToko;
