import React, { useState, useEffect } from "react";
// react component that copies the given text inside your clipboard
// import { CopyToClipboard } from "react-copy-to-clipboard";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

import { IoCall, IoEye, IoSearchSharp } from "react-icons/io5";
// import { transform } from "typescript";
import { useDispatch, useSelector } from "react-redux";
import DataTable from "react-data-table-component";
// core components
import { setKodeToko } from "../../../utils";
import { getToko } from "redux/actions/Toko";
import ModalDetailToko from "./ModalDetailToko";
import { updateStatusToko } from "redux/actions/Toko";
const ModalAddToko = React.lazy(() => import("./ModalAddToko"));
const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Toko = () => {
  const dispatch = useDispatch();
  const dataToko = useSelector((data) => data.toko.toko);
  // const [copiedText, setCopiedText] = useState();
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [detail, setDetail] = useState(null);
  const [kode, setKode] = useState(`TK${setKodeToko(dataToko.length + 1)}`);

  const toggle = () => {
    setKode(`TK${setKodeToko(dataToko.length + 1)}`);
    setModal(!modal);
  };

  const toggleDetail = () => {
    setModalDetail(!modalDetail);
    // modalDetail === false && setDetail(null);
  };

  const filterToko = dataToko.filter((data) => {
    return data.nama_toko.toLowerCase().indexOf(cari) !== -1;
  });

  const setDetailToko = async (data) => {
    await setDetail(data);
    await toggleDetail();
  };

  const updateStatus = (id, status) => {
    dispatch(updateStatusToko(id, status));
  };

  useEffect(() => {
    dispatch(getToko());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Informasi Toko</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah Toko
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">Kode Toko</th>
                        <th scope="col">Nama Toko</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Telephone</th>
                        <th scope="col">status</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      {filterToko.map((data) => (
                        <tr>
                          <th scope="row">{data.kode_toko}</th>
                          <td>{data.nama_toko}</td>
                          <td>{data.alamat}</td>
                          <td>
                            <i className="mr-3">
                              <IoCall className="text-success" />
                            </i>
                            {data.telp}
                          </td>
                          <td>
                            <CustomInput
                              type="switch"
                              id={data.kode_toko}
                              name={data.kode_toko}
                              checked={data.status === "Y" ? true : false}
                              onClick={() =>
                                updateStatus(
                                  data.id,
                                  data.status === "Y" ? "N" : "Y"
                                )
                              }
                              label="Tutup/Buka"
                            />
                          </td>
                          <td>
                            <Button
                              color="info"
                              size="sm"
                              onClick={() => setDetailToko(data)}
                            >
                              <IoEye />
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalAddToko modal={modal} toggle={toggle} kode={kode} />
      {detail && (
        <ModalDetailToko
          modal={modalDetail}
          toggle={toggleDetail}
          data={detail}
        />
      )}
    </>
  );
};

export default Toko;
