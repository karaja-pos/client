import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

import { IoEye, IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import ModalTambah from "./ModalTambah";
import { getPosisi, blokirPosisi } from "../../../redux/actions/Posisi";
import ModalUpdate from "./ModalUpdate";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Posisi = () => {
  const dispatch = useDispatch();

  const dataPosisi = useSelector((data) => data.posisi.posisi);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemPosisi, setItemPosisi] = useState(null);

  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const setItemUpdate = (data) => {
    setItemPosisi(data);
    toggleUpdate();
  };

  const filterPosisi = dataPosisi.filter((data) => {
    return data.posisi.toLowerCase().indexOf(cari) !== -1;
  });

  useEffect(() => {
    dispatch(getPosisi());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Informasi Posisi</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah Posisi
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <Table className="align-items-center table-flush" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Posisi / Level</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      {filterPosisi.map((data, id) => (
                        <tr key={id}>
                          <td>{id + 1}</td>
                          <td>{data.posisi}</td>
                          <td>
                            <CustomInput
                              type="switch"
                              id={`${data.posisi.trim().toLowerCase()}_${
                                data.id
                              }`}
                              name={`${data.posisi.trim().toLowerCase()}_${
                                data.id
                              }`}
                              checked={data.blokir === "Y" ? true : false}
                              onClick={
                                () =>
                                  dispatch(
                                    blokirPosisi(
                                      data.id,
                                      data.blokir === "Y" ? "N" : "Y"
                                    )
                                  )
                                // console.log("Update")
                              }
                              label="Tidak/Blokir"
                            />
                          </td>
                          <td>
                            <Button
                              color="info"
                              size="sm"
                              onClick={() => setItemUpdate(data)}
                            >
                              <IoEye />
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalTambah modal={modal} toggle={toggle} />
      {itemPosisi && (
        <ModalUpdate
          modal={modalUpdate}
          toggle={toggleUpdate}
          item={itemPosisi}
        />
      )}
    </>
  );
};

export default Posisi;
