import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
} from "reactstrap";
import { tambahPoisi } from "redux/actions/Posisi";

const ModalTambah = ({ modal, toggle }) => {
  const dispatch = useDispatch();
  const [posisi, setPosisi] = useState("");
  const [posisiErr, setPosisiErr] = useState(false);

  const handleSimpan = () => {
    posisi === "" ? setPosisiErr(true) : setPosisiErr(false);

    if (posisi !== "") {
      let data = {
        posisi: posisi,
        user: "",
      };
      dispatch(tambahPoisi(data));
      toggle();
    }
  };
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Tambah Posisi/Level
        </h5>
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label for="posisi">Nama Posisi/Level</Label>
          <Input
            invalid={posisiErr}
            type="text"
            name="posisi"
            id="posisi"
            value={posisi}
            onChange={(e) => setPosisi(e.target.value)}
            placeholder="Masukan Posisi/Level"
          />
          {posisiErr && <FormFeedback>Masih kosong</FormFeedback>}
        </FormGroup>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleSimpan()}>
          Tambahkan
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalTambah;
