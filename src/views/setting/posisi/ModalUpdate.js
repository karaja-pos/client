import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
} from "reactstrap";
import { updatePosisi } from "redux/actions/Posisi";
import { tambahPoisi } from "redux/actions/Posisi";

const ModalUpdate = ({ modal, toggle, item }) => {
  const dispatch = useDispatch();
  const [id, setId] = useState("");
  const [posisi, setPosisi] = useState("");
  const [posisiErr, setPosisiErr] = useState(false);

  const handleUpdate = () => {
    posisi === "" ? setPosisiErr(true) : setPosisiErr(false);

    if (posisi !== "") {
      let data = {
        posisi: posisi,
      };
      dispatch(updatePosisi(id, data));
      toggle();
    }
  };

  useEffect(() => {
    setId(item.id);
    setPosisi(item.posisi);
  }, [item.id]);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Update Posisi/Level
        </h5>
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label for="posisi">Nama Posisi/Level</Label>
          <Input
            invalid={posisiErr}
            type="text"
            name="posisi"
            id="posisi"
            value={posisi}
            onChange={(e) => setPosisi(e.target.value)}
            placeholder="Masukan Posisi/Level"
          />
          {posisiErr && <FormFeedback>Masih kosong</FormFeedback>}
        </FormGroup>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleUpdate()}>
          Update
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalUpdate;
