import React, { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import {
  TableBody,
  TableContainer,
  TableHeader,
  TBodyBaris,
  TBodyColom,
  THeadItem,
  TitleHead,
} from "./style";

const TableKaryawan = ({ item }) => {
  const [active, setActive] = useState("");
  const handleChange = (id) => {
    setActive(id);
    // dispatch(getKaryawanByToko(id));
  };

  const columns = [
    {
      name: "Select All",
      selector: "nama_karyawan",
      // sortable: true,
      // width: "150px",
    },
  ];

  useEffect(() => {
    setActive("");
  }, [item]);
  return (
    <>
      <TitleHead>KARYAWAN</TitleHead>
      <DataTable
        columns={columns}
        data={item}
        noHeader
        fixedHeader
        selectableRows={true}
        dense={true}
        onRowClicked={(a) => console.log(a)}
        onSelectedRowsChange={(a) => console.log(a.selectedRows)}
        noDataComponent={<p style={{ fontSize: 10 }}>Tidak ada data</p>}
        customStyles={{
          rows: {
            style: {
              fontSize: 10,
            },
          },

          headCells: {
            style: {
              fontSize: 10,
            },
          },
        }}
      />
      {/* <TableContainer>
        <TableHeader>
          <THeadItem>NAMA KARYAWAN</THeadItem>
        </TableHeader>
        <TableBody>
          {item.map((data) => (
            <TBodyBaris
            // className={active == data.id ? "active" : ""}
            // onClick={() => handleChange(data.id)}
            >
              <TBodyColom>{data.nama_karyawan}</TBodyColom>
            </TBodyBaris>
          ))}
        </TableBody>
      </TableContainer> */}
    </>
  );
};

export default TableKaryawan;
