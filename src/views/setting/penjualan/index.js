import React, { useEffect, useState } from "react";
import { Card, CardBody, Container, Row, CustomInput, Col } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { getBarang } from "redux/actions/Barang";
import { getToko } from "redux/actions/Toko";
import { updateStatusToko } from "redux/actions/Toko";
import { getKaryawanByToko } from "redux/actions/Karyawan";
import TableBarang from "./TableBarang";
import { getKategori } from "redux/actions/Kategori";

const TableToko = React.lazy(() => import("./TableToko"));

const Penjualan = () => {
  const dispatch = useDispatch();

  const dataBarang = useSelector((data) => data.barang.barang);
  const dataToko = useSelector((data) => data.toko.toko);
  const dataBarangToko = useSelector((data) => data.barang.barang_toko);
  const dataStokToko = useSelector((data) => data.barang.stok_toko);
  // const dataKaryawan = useSelector((data) => data.karyawan.karyawan_toko);
  // const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemBarang, setItemBarang] = useState(null);

  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const [activeTab, setActiveTab] = useState("1");

  const toggles = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  // const filterBarang = dataBarang.filter((data) => {
  //   return (
  //     data.nama_brg.toLowerCase().indexOf(cari) !== -1 ||
  //     data.kode_brg.toLowerCase().indexOf(cari) !== -1
  //   );
  // });

  const updateStatus = (id, status) => {
    dispatch(updateStatusToko(id, status));
  };

  const handleChange = (state) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    console.log("Selected Rows: ", state.selectedRows);
  };

  const columns = [
    {
      name: "TOKO",
      selector: "nama_toko",
      sortable: true,
      width: "150px",
    },
    {
      name: "AKTIF",
      selector: "status",
      sortable: true,
      center: true,
      cell: (data) => (
        <CustomInput
          type="switch"
          id={data.kode_toko}
          name={data.kode_toko}
          checked={data.status === "Y" ? true : false}
          onClick={() => updateStatus(data.id, data.status === "Y" ? "N" : "Y")}
          // label="Tutup/Buka"
        />
      ),
    },
  ];

  // const columns_karyawan = [
  //   {
  //     name: "NAMA KARYAWAN",
  //     selector: "nama_karyawan",
  //     sortable: true,
  //     // width: "150px",
  //   },
  // ];

  // const conditionalRowStyles = [
  //   {
  //     when: (row) => row.toggleSelected,
  //     style: {
  //       backgroundColor: "green",
  //       userSelect: "none",
  //     },
  //   },
  // ];

  // const getKaryawanToko = (item) => {
  //   console.log(item);
  //   dispatch(getKaryawanByToko(item.id));
  //   return {
  //     ...item,
  //     toggleSelected: !item.toggleSelected,
  //   };
  // };

  const setItemUpdate = (data) => {
    setItemBarang(data);
    toggleUpdate();
  };

  useEffect(() => {
    dispatch(getBarang());
    dispatch(getToko());
    dispatch(getKategori());
  }, []);

  return (
    <>
      <div className="header bg-gradient-info pb-6 pt-5 pt-md-8">
        <Container fluid>
          {/* <div className="header-body">{props.children}</div> */}
        </Container>
      </div>

      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}

        <Row style={{ height: 700, maxHeight: 700, marginBottom: 50 }}>
          <Col xs={12} md={9} className="mb-3">
            <Card className="shadow" style={{ height: "100%" }}>
              <CardBody>
                <Row className="info-toko">
                  {/* <Col md={3}>
                    <TableKaryawan item={dataKaryawan} />
                  </Col> */}

                  <Col md={12}>
                    <TableBarang item={dataBarangToko} stok={dataStokToko} />
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>

          <Col xs={12} md={3} className="mb-3">
            <Card
              className="shadow"
              style={{ height: "100%", minHeight: "100%" }}
            >
              <CardBody>
                <Row className="info-toko">
                  <TableToko item={dataToko} />
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Penjualan;
