import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Input, Table } from "reactstrap";

import { getKaryawanByToko } from "redux/actions/Karyawan";
import {
  TableContainer,
  TableHeader,
  THeadItem,
  TableBody,
  TBodyBaris,
  TBodyColom,
  TitleHead,
} from "./style";
import {
  getStokToko,
  getBarangToko,
  setBarangId,
} from "../../../redux/actions/Barang";

const TableToko = ({ item }) => {
  const dispatch = useDispatch();
  const [active, setActive] = useState("");

  const handleChange = (id) => {
    dispatch(setBarangId(id));
    setActive(id);
    dispatch(getKaryawanByToko(id));
    dispatch(getStokToko(id));
    dispatch(getBarangToko(id));
  };

  return (
    <>
      <TitleHead>DAFTAR TOKO</TitleHead>
      <TableContainer>
        <TableHeader>
          <THeadItem>NAMA TOKO</THeadItem>
          <THeadItem>STATUS</THeadItem>
        </TableHeader>
        <TableBody>
          {item.map((data) => (
            <TBodyBaris
              className={active == data.id ? "active" : ""}
              onClick={() => handleChange(data.id)}
            >
              <TBodyColom>{data.nama_toko}</TBodyColom>
              <TBodyColom>{data.status}</TBodyColom>
            </TBodyBaris>
          ))}
        </TableBody>
      </TableContainer>
    </>
  );
};

export default TableToko;
