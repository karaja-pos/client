import { Button } from "bootstrap";
import React from "react";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Downloads extends React.Component {
  render() {
    return (
      <ExcelFile
        filename={this.props.nama_toko}
        element={
          <button className="btn btn-outline-primary btn-sm">disini!</button>
        }
      >
        <ExcelSheet data={this.props.data} name="Sheet1">
          <ExcelColumn label="ID" value="id" />
          <ExcelColumn label="ID_BARANG" value="id_barang" />
          <ExcelColumn label="NAMA_BARANG" value="nama_barang" />
          <ExcelColumn label="STOK" value="stok_akhir" />
          <ExcelColumn label="HARGA" value="harga_jual" />
          <ExcelColumn label="ID_TOKO" value="id_toko" />
        </ExcelSheet>
      </ExcelFile>
    );
  }
}

export default Downloads;
