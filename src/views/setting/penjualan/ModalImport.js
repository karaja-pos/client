import axios from "axios";
import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Button,
  Col,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import { getStokToko } from "redux/actions/Barang";

import Swal from "sweetalert2";
import { BASE_URL } from "utils";
import Downloads from "./Downloads";

const ModalImport = ({ modal, toggle, data, nama_toko, id_toko }) => {
  const [file, setFile] = useState(null);
  const dispatch = useDispatch();

  const handleFileChange = (e) => {
    setFile(e[0]);
  };

  const handleImport = () => {
    if (file !== null) {
      const data = new FormData();
      data.append("stok", file);
      axios
        .post(`${BASE_URL}/file/stok`, data)
        .then((e) => {
          Swal.fire({
            icon: "success",
            title: "Success",
            text: "Import data stok berhasil!",
          });
          toggle();
          dispatch(getStokToko(id_toko));
        })
        .catch((err) => {
          console.log(err);
          Swal.fire({
            icon: "error",
            title: "Ooops!",
            text: "Import data stok GAGAL!",
          });
        });
    }
  };

  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"

      // style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Import Data Stok
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col>
            <p>
              Silahkan download teplate stok barang{" "}
              <Downloads data={data} nama_toko={nama_toko} />
            </p>
          </Col>
        </Row>
        <Row>
          <Col>
            <Label for="kategori">Pilih File Stok</Label>
            <Input
              type="file"
              accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
              onChange={(e) => handleFileChange(e.target.files)}
            />
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="danger" onClick={toggle} size="sm">
          Batal
        </Button>
        <Button color="success" onClick={() => handleImport()} size="sm">
          Import
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalImport;
