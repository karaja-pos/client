import React, { useState } from "react";
import DataTable from "react-data-table-component";
import CurrencyInput from "react-currency-input-field";
import { useDispatch, useSelector } from "react-redux";

import {
  Badge,
  Button,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import Select from "react-select";
import {
  TableBody,
  TableContainer,
  TableHeader,
  TBodyBaris,
  TBodyColom,
  THeadItem,
  TitleHead,
  Container,
} from "./style";
import { insertBarangToToko } from "redux/actions/Barang";
import { addSelectedBarang } from "redux/actions/Barang";
import { IoReturnUpForwardSharp, IoSearchOutline } from "react-icons/io5";
import { setTotalHarga } from "redux/actions/Barang";
import { InputHarga } from "utils/component";
import { InputStok } from "utils/component";
import { InputDiskon } from "utils/component";
import { FaRegFileExcel } from "react-icons/fa";
import ModalImport from "./ModalImport";

const TableBarang = ({ item, stok }) => {
  const dispatch = useDispatch();
  const dataSelected = useSelector((data) => data.barang.selected);
  const idBarang = useSelector((data) => data.barang.id_barang);
  const dataKategory = useSelector((data) => data.kategori.kategori);

  const [cariItem, setCariItem] = useState("");
  const [cariStok, setCariStok] = useState("");
  const [idKategori, setIdKategori] = useState(0);
  const [idKategoriStok, setIdKategoriStok] = useState(0);
  const [select, setSelect] = useState(null);

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const katArr = dataKategory.map((x) => {
    return {
      value: x.id,
      label: x.kategori,
    };
  });

  const filterItem = item.filter((data) => {
    return (
      data.nama_brg.toLowerCase().indexOf(cariItem) !== -1 ||
      data.kode_brg.toLowerCase().indexOf(cariItem) !== -1
    );
  });

  const filterItemKat = filterItem.filter((data) => {
    if (idKategori === 0) {
      return true;
    } else {
      return data.id_kategori === idKategori;
    }
  });

  const filterStok = stok.filter((data) => {
    return (
      data.nama_barang.toLowerCase().indexOf(cariStok) !== -1 ||
      data.kode_brg.toLowerCase().indexOf(cariStok) !== -1
    );
  });

  const filterStokKat = filterStok.filter((data) => {
    if (idKategoriStok === 0) {
      return true;
    } else {
      return data.id_kategori === idKategoriStok;
    }
  });

  const handleItemKat = (e) => {
    if (e) {
      setIdKategori(e.value);
    } else {
      setIdKategori(0);
    }
  };

  const handleStokKat = (e) => {
    if (e) {
      setIdKategoriStok(e.value);
    } else {
      setIdKategoriStok(0);
    }
  };

  // const [harga, setHarga] = useState(0);
  const [idFocus, setIdFocus] = useState(null);
  const columns = [
    {
      name: "Select All",
      selector: "nama_brg",
      sortable: true,
      // width: "150px",
    },
  ];

  const columns_barang_toko = [
    {
      name: "NAMA BARANG",
      selector: "nama_barang",
      sortable: true,
      cell: (data) => (
        <span>
          {data.nama_barang}{" "}
          <Badge color="success" style={{ marginLeft: 10 }}>
            {data.merek}
          </Badge>
        </span>
      ),
    },

    {
      name: "STOK",
      selector: "",
      width: "120px",
      center: true,
      cell: (data) => <InputStok item={data} />,
    },

    {
      name: "HARGA",
      selector: "",
      center: true,
      width: "100px",
      cell: (data) => <InputHarga item={data} />,
    },

    {
      name: "DISKON",
      selector: "",
      center: true,
      width: "100px",
      cell: (data) => <InputDiskon item={data} />,
    },
  ];

  const mappingBarang = (data) => {
    if (idBarang !== null && select !== null) {
      dispatch(insertBarangToToko(idBarang, select));
      setSelect(null);
    }
  };

  return (
    <>
      <TitleHead>BARANG</TitleHead>
      <Row>
        <Col md={5}>
          <Container>
            <TableHeader>
              <THeadItem>MASTER BARANG</THeadItem>
              <THeadItem style={{ textAlign: "right", width: 70 }}>
                <Button
                  className="mr-4"
                  color="info"
                  onClick={(e) => mappingBarang(dataSelected)}
                  size="sm"
                >
                  <IoReturnUpForwardSharp />
                </Button>
              </THeadItem>
            </TableHeader>
            <TableHeader>
              <THeadItem>
                <InputGroup size="sm">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <IoSearchOutline />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    value={cariItem}
                    onChange={(e) => setCariItem(e.target.value)}
                    placeholder="Barang..."
                    style={{ paddingTop: 17, paddingBottom: 17 }}
                  />
                </InputGroup>
              </THeadItem>
              <THeadItem>
                <Select
                  options={katArr}
                  placeholder="Kategori..."
                  isClearable
                  onChange={(e) => handleItemKat(e)}
                />
              </THeadItem>
            </TableHeader>
          </Container>
          <TableContainer>
            <TableBody>
              {/* <div style={{ height: 600 }}> */}
              <DataTable
                columns={columns}
                data={filterItemKat}
                // style={{ height: "100%" }}
                selectableRows={true}
                fixedHeaderScrollHeight
                responsive={true}
                striped={true}
                direction="ltr"
                noHeader={true}
                overflowY
                // progressPending={item.length > 0 ? false : true}
                clearSelectedRows={select === null ? true : false}
                onSelectedRowsChange={(a) => setSelect(a.selectedRows)}
                highlightOnHover
                fixedHeaderScrollHeight="10vh"
                noDataComponent={<p style={{ fontSize: 10 }}>Tidak ada data</p>}
                // noTableHead
                dense={true}
                customStyles={{
                  rows: {
                    style: {
                      fontSize: 10,
                    },
                  },

                  headCells: {
                    style: {
                      fontSize: 10,
                    },
                  },
                }}
              />
            </TableBody>
            {/* </div> */}
          </TableContainer>
        </Col>
        <Col md={7}>
          <Container>
            <TableHeader style={{ padding: 7 }}>
              <THeadItem style={{ textAlign: "center" }}>
                BARANG TOKO = {stok.length > 0 ? stok[0].nama_toko : ""}
              </THeadItem>
              {stok.length > 0 && (
                <div>
                  <Button size="sm" color="success" onClick={() => toggle()}>
                    <FaRegFileExcel size={18} />
                  </Button>
                </div>
              )}
            </TableHeader>
            <TableHeader>
              <THeadItem>
                <InputGroup size="sm">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <IoSearchOutline />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="Barang..."
                    style={{ paddingTop: 17, paddingBottom: 17 }}
                    onChange={(e) => setCariStok(e.target.value)}
                  />
                </InputGroup>
              </THeadItem>
              <THeadItem>
                <Select
                  options={katArr}
                  placeholder="Kategori..."
                  isClearable
                  onChange={(e) => handleStokKat(e)}
                />
              </THeadItem>
            </TableHeader>
          </Container>
          <TableContainer>
            <TableBody>
              {/* <div style={{ height: 600 }}> */}
              <DataTable
                columns={columns_barang_toko}
                data={filterStokKat}
                fixedHeaderScrollHeight
                responsive={true}
                striped={true}
                direction="ltr"
                noHeader={true}
                pagination
                paginationPerPage={20}
                overflowY
                highlightOnHover
                // progressPending={stok.length > 0 ? false : true}
                fixedHeaderScrollHeight="10vh"
                noDataComponent={
                  <p style={{ fontSize: 10, padding: 20 }}>Tidak ada data</p>
                }
                dense={true}
                customStyles={{
                  rows: {
                    style: {
                      fontSize: 10,
                    },
                  },

                  headCells: {
                    style: {
                      fontSize: 10,
                    },
                  },
                }}
              />
            </TableBody>
            {/* </div> */}
          </TableContainer>
        </Col>
      </Row>
      {/* <TableContainer>
        <TableHeader>
          <THeadItem>DATA BARANG</THeadItem>
        </TableHeader>
        <TableBody> */}
      {/* {item.map((data) => (
          <TBodyBaris
            className={active == data.id ? "active" : ""}
            onClick={() => handleChange(data.id)}
          >
            <TBodyColom>{data.nama_karyawan}</TBodyColom>
          </TBodyBaris>
        ))} */}
      {/* </TableBody>
      </TableContainer> */}
      {stok.length > 0 && (
        <ModalImport
          modal={modal}
          toggle={toggle}
          data={stok}
          nama_toko={stok[0].nama_toko}
          id_toko={stok[0].id_toko}
        />
      )}
    </>
  );
};

export default TableBarang;
