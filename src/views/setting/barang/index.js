import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  CardImg,
  CardTitle,
  CardSubtitle,
  CardText,
  Media,
} from "reactstrap";
import Select from "react-select";

import DataTable from "react-data-table-component";
import { IoEye, IoPrintOutline, IoSearchOutline } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import ModalTambah from "./ModalTambah";
import ModalUpdate from "./ModalUpdate";
import { NoDataBarang, BarangComponent } from "utils/component";
import { getBarang } from "redux/actions/Barang";
import { getKategori } from "redux/actions/Kategori";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Barang = () => {
  const dispatch = useDispatch();

  const dataBarang = useSelector((data) => data.barang.barang);
  const dataKategory = useSelector((data) => data.kategori.kategori);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemBarang, setItemBarang] = useState(null);
  const [idKategori, setIdKategori] = useState(0);

  const katArr = dataKategory.map((x) => {
    return {
      value: x.id,
      label: x.kategori,
    };
  });

  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const filterBarang = dataBarang.filter((data) => {
    return (
      data.nama_brg.toLowerCase().indexOf(cari) !== -1 ||
      data.kode_brg.toLowerCase().indexOf(cari) !== -1
    );
  });

  const filterItemKat = filterBarang.filter((data) => {
    if (idKategori === 0) {
      return true;
    } else {
      return data.id_kategori === idKategori;
    }
  });

  const handleItemKat = (e) => {
    if (e) {
      setIdKategori(e.value);
    } else {
      setIdKategori(0);
    }
  };

  const columns = [
    {
      name: "KODE",
      selector: "kode_brg",
      sortable: true,
      width: "150px",
    },
    {
      name: "NAMA BARANG",
      selector: "nama_brg",
      sortable: true,
    },
    {
      name: "KATEGORI",
      selector: "kategori",
      sortable: true,
      center: true,
      width: "120px",
    },
    {
      name: "MEREK",
      selector: "merek",
      sortable: true,
      width: "200px",
    },
    {
      name: "STOK",
      selector: "stok_akhir",
      center: true,
      width: "120px",
    },
    {
      name: "SATUAN",
      selector: "nama_satuan",
      center: true,
      width: "120px",
    },

    {
      name: "AKSI",
      selector: "aksi",
      width: "120px",
      center: true,
      cell: (item) => (
        <Button color="info" size="sm" onClick={() => setItemUpdate(item)}>
          <IoEye />
        </Button>
      ),
    },
  ];

  const setItemUpdate = (data) => {
    setItemBarang(data);
    toggleUpdate();
  };

  useEffect(() => {
    dispatch(getBarang());
    dispatch(getKategori());
  }, []);

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Data Barang</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card style={{ marginBottom: 20 }}>
                      <CardBody>
                        <Row>
                          <Col xs={12} md={8}></Col>
                          <Col xs={12} md={2}>
                            <InputGroup size="sm">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <IoSearchOutline />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Nama / Kode Barang..."
                                style={{ paddingTop: 18, paddingBottom: 18 }}
                                onChange={(e) => setCari(e.target.value)}
                              />
                            </InputGroup>
                          </Col>
                          <Col xs={12} md={2}>
                            <div
                              style={{
                                fontSize: 12,
                                fontWeight: "bold",
                                padding: 0,
                              }}
                            >
                              <Select
                                options={katArr}
                                placeholder="Kategori..."
                                isClearable
                                onChange={(e) => handleItemKat(e)}
                              />
                            </div>
                          </Col>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>

                <Row className="info-toko">
                  <DataTable
                    className="align-items-center table-flush"
                    columns={columns}
                    data={filterItemKat}
                    responsive={true}
                    striped={true}
                    direction="ltr"
                    noHeader={true}
                    expandableRows
                    expandOnRowClicked={true}
                    fixedHeader={true}
                    pagination={true}
                    // selectableRows={true}
                    // onSelectedRowsChange={(a) => console.log(a.selectedRows)}
                    highlightOnHover
                    noDataComponent={<NoDataBarang />}
                    expandableRowsComponent={<BarangComponent />}
                    style={{
                      headCells: {
                        style: {
                          backgroundColor: "#f6f9fc",
                          color: "#8898aa",
                        },
                      },
                    }}
                  />
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalTambah modal={modal} toggle={toggle} barang={dataBarang.length} />
      {itemBarang && (
        <ModalUpdate
          modal={modalUpdate}
          toggle={toggleUpdate}
          item={itemBarang}
        />
      )}
    </>
  );
};

export default Barang;
