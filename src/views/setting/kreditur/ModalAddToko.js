import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Button,
  Col,
  Row,
  FormGroup,
  Label,
  FormFeedback,
} from "reactstrap";
import { simpanKreditur } from "redux/actions/Kreditur";

const ModalAddToko = ({ modal, toggle, kode }) => {
  const dispatch = useDispatch();
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telp, setTelp] = useState("");

  const [namaErr, setNamaErr] = useState(false);
  const [alamatErr, setAlamatErr] = useState(false);
  const [telpErr, setTelpErr] = useState(false);
  const handleSimpan = () => {
    nama === "" ? setNamaErr(true) : setNamaErr(false);
    alamat === "" ? setAlamatErr(true) : setAlamatErr(false);
    telp === "" ? setTelpErr(true) : setTelpErr(false);

    if (nama !== "" && alamat !== "" && telp !== "") {
      const data = {
        kode: kode,
        nama_kreditur: nama,
        alamat: alamat,
        telp: telp,
        user: "",
      };
      dispatch(simpanKreditur(data));
      toggle();
    }
  };
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      style={{ transform: "translate(0%, 10%)", top: 0, left: 0 }}
    >
      <ModalHeader slot="header">
        <h5 class="modal-title" id="exampleModalLabel">
          Daftarkan Toko
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="kode">Kode</Label>
              <Input
                type="text"
                name="kode"
                id="kode"
                disabled
                placeholder="Kode Toko"
                value={kode}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="nama">Nama Kreditur</Label>
              <Input
                invalid={namaErr}
                type="text"
                name="nama"
                id="nama"
                placeholder="Masukan Nama Toko"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
              {namaErr && (
                <FormFeedback>Nama kreditur masih kosong</FormFeedback>
              )}
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="alamat">Alamat</Label>
          <Input
            invalid={alamatErr}
            type="text"
            name="alamat"
            id="alamat"
            value={alamat}
            onChange={(e) => setAlamat(e.target.value)}
            placeholder="Masukan alamat toko"
          />
          {alamatErr && (
            <FormFeedback>Alamat kreditur masih kosong</FormFeedback>
          )}
        </FormGroup>
        <FormGroup>
          <Label for="telp">Nomor Telephone</Label>
          <Input
            invalid={telpErr}
            type="telp"
            name="telp"
            id="telp"
            placeholder="+6208xxx"
            value={telp}
            onChange={(e) => setTelp(e.target.value)}
          />
          {telpErr && (
            <FormFeedback>Nomor Telp kreditur masih kosong</FormFeedback>
          )}
        </FormGroup>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={toggle}>
          Batal
        </Button>
        <Button color="primary" onClick={() => handleSimpan()}>
          Daftarkan
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalAddToko;
