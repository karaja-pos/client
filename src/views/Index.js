import React, { useState } from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  Badge,
  Input,
} from "reactstrap";
import { DateRangePicker, DateRange } from "react-date-range";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2,
} from "variables/charts.js";
import { IoEyeOutline } from "react-icons/io5";
import moment from "moment";

const Header = React.lazy(() => import("../components/Headers/Header"));
const CardStatus = React.lazy(() => import("../components/Card/CardStatus"));

const Index = (props) => {
  const [activeNav, setActiveNav] = useState(1);
  const [chartExample1Data, setChartExample1Data] = useState("data1");
  const [date, setDate] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: "selection",
    },
  ]);

  if (window.Chart) {
    parseOptions(Chart, chartOptions());
  }

  const toggleNavs = (e, index) => {
    e.preventDefault();
    setActiveNav(index);
    setChartExample1Data("data" + index);
  };
  return (
    <>
      <Header>
        <CardStatus />
      </Header>
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          {/* <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="bg-gradient-default shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      Overview
                    </h6>
                    <h2 className="text-white mb-0">Sales value</h2>
                  </div>
                  <div className="col">
                    <Nav className="justify-content-end" pills>
                      <NavItem>
                        <NavLink
                          className={classnames("py-2 px-3", {
                            active: activeNav === 1,
                          })}
                          href="#pablo"
                          onClick={(e) => toggleNavs(e, 1)}
                        >
                          <span className="d-none d-md-block">Month</span>
                          <span className="d-md-none">M</span>
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames("py-2 px-3", {
                            active: activeNav === 2,
                          })}
                          data-toggle="tab"
                          href="#pablo"
                          onClick={(e) => toggleNavs(e, 2)}
                        >
                          <span className="d-none d-md-block">Week</span>
                          <span className="d-md-none">W</span>
                        </NavLink>
                      </NavItem>
                    </Nav>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <div className="chart">
                  <Line
                    data={chartExample1[chartExample1Data]}
                    options={chartExample1.options}
                    getDatasetAtEvent={(e) => console.log(e)}
                  />
                </div>
              </CardBody>
            </Card>
          </Col> */}
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Penjualan Kasir</h3>
                  </div>
                  <div className="col text-right">
                    <Button
                      color="primary"
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                      size="sm"
                    >
                      See all
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Nama Kasir</th>
                    <th scope="col">Login</th>
                    <th scope="col">Nama Toko</th>
                    <th scope="col">Progress</th>
                    <th scope="col">Detail</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Kasir 1</th>
                    <td>
                      <Badge color="info">07/04/2021 16:20</Badge>
                    </td>
                    <td>Toko 1</td>
                    <td>
                      <i className="fas fa-arrow-up text-success mr-3" /> 0%
                    </td>
                    <td>
                      <Button size="sm" color="success">
                        <IoEyeOutline /> Lihat
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Kasir 2</th>
                    <td>
                      <Badge color="info">07/04/2021 16:20</Badge>
                    </td>
                    <td>Toko 2</td>
                    <td>
                      <i className="fas fa-arrow-down text-warning mr-3" /> 0%
                    </td>
                    <td>
                      <Button size="sm" color="success">
                        <IoEyeOutline /> Lihat
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Kasir 3</th>
                    <td>
                      <Badge color="info">07/04/2021 16:20</Badge>
                    </td>
                    <td>Toko 3</td>
                    <td>
                      <i className="fas fa-arrow-down text-warning mr-3" /> 0%
                    </td>
                    <td>
                      <Button size="sm" color="success">
                        <IoEyeOutline /> Lihat
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Kasir 4</th>
                    <td>
                      <Badge color="info">07/04/2021 16:20</Badge>
                    </td>
                    <td>Toko 4</td>
                    <td>
                      <i className="fas fa-arrow-up text-success mr-3" /> 0%
                    </td>
                    <td>
                      <Button size="sm" color="success">
                        <IoEyeOutline /> Lihat
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Kasir 5</th>
                    <td>
                      <Badge color="info">07/04/2021 16:20</Badge>
                    </td>
                    <td>Toko 5</td>
                    <td>
                      <i className="fas fa-arrow-down text-danger mr-3" /> 0%
                    </td>
                    <td>
                      <Button size="sm" color="success">
                        <IoEyeOutline /> Lihat
                      </Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Card>
          </Col>
          <Col xl="4">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-muted ls-1 mb-1">
                      Performance
                    </h6>
                    <h2 className="mb-0">Penjualan</h2>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                {/* Chart */}
                <div className="chart">
                  <Bar
                    data={chartExample2.data}
                    options={chartExample2.options}
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col xl="12">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Trafik Penjualan Barang</h3>
                  </div>

                  <div className="col text-right">
                    <Row>
                      <Col>
                        <Input type="select" size="sm">
                          <option>Semua Toko</option>
                          <option>Toko 1</option>
                          <option>Toko 2</option>
                          <option>Toko 3</option>
                          <option>Toko 4</option>
                        </Input>
                      </Col>
                      <Col>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <Input
                            type="date"
                            size="sm"
                            defaultValue={moment().format("yyyy-MM-DD")}
                          />{" "}
                          <span style={{ paddingLeft: 5, paddingRight: 5 }}>
                            S/D
                          </span>
                          <Input
                            type="date"
                            size="sm"
                            defaultValue={moment().format("yyyy-MM-DD")}
                          />
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Row>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Barang</th>
                    <th scope="col">Terjual</th>
                    <th scope="col">Sisa Stok</th>
                    <th scope="col">Total Penjualan</th>
                    <th scope="col">Trafik</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Barang 1</th>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>
                      <div className="d-flex align-items-center">
                        <span className="mr-2">0%</span>
                        <div>
                          <Progress
                            max="100"
                            value="0"
                            barClassName="bg-gradient-danger"
                          />
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Barang 2</th>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>
                      <div className="d-flex align-items-center">
                        <span className="mr-2">0%</span>
                        <div>
                          <Progress
                            max="100"
                            value="0"
                            barClassName="bg-gradient-success"
                          />
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Barang 3</th>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>
                      <div className="d-flex align-items-center">
                        <span className="mr-2">0%</span>
                        <div>
                          <Progress max="100" value="0" />
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Barang 4</th>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>
                      <div className="d-flex align-items-center">
                        <span className="mr-2">0%</span>
                        <div>
                          <Progress
                            max="100"
                            value="0"
                            barClassName="bg-gradient-info"
                          />
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Barang 5</th>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>
                      <div className="d-flex align-items-center">
                        <span className="mr-2">0%</span>
                        <div>
                          <Progress
                            max="100"
                            value="0"
                            barClassName="bg-gradient-warning"
                          />
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Index;
