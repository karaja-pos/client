import React, { useEffect, useRef, useState } from "react";
import { IoSearchSharp, IoReceiptOutline, IoPrint } from "react-icons/io5";
import moment from "moment";
import DataTable from "react-data-table-component";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Button,
  Badge,
} from "reactstrap";
import { FaRegCheckCircle, FaTruck } from "react-icons/fa";
import { useReactToPrint } from "react-to-print";
import { NoDataBarang } from "utils/component";
import { useDispatch, useSelector } from "react-redux";
// import { getBarang } from "redux/actions/Barang";
import CetakFaktur from "./CetakFaktur";
import Swal from "sweetalert2";
// import ModalPengiriman from "./ModalPengiriman";
import { getPengiriman } from "redux/actions/Gudang";
import ModalPenerimaan from "./ModalPenerimaan";
import { getListItemPengiriman } from "redux/actions/Gudang";
import { cetakPengiriman } from "redux/actions/Gudang";
import { getPengirimanByToko } from "redux/actions/Gudang";

const Header = React.lazy(() =>
  import("../../../../components/Headers/Header")
);

const Penerimaan = () => {
  const dispatch = useDispatch();
  const cetakRef = useRef();
  const dataFaktur = useSelector((data) => data.gudang.data_pengiriman);
  const currentUser = useSelector((data) => data.auth.currentUser);
  // const cetakFaktur = useSelector((data) => data.gudang.cetak);
  const itemPengiriman = useSelector((data) => data.gudang.items_pengiriman);
  const [modal, setModal] = useState(false);
  const [itemCetak, setItemCetak] = useState([]);
  const [toko, setToko] = useState("");

  const toggle = () => setModal(!modal);
  const handleToogle = async (id, toko) => {
    setToko(toko);
    await dispatch(
      getListItemPengiriman(id, (err, data) => {
        toggle();
      })
    );
  };

  const handlePrint = useReactToPrint({
    content: () => cetakRef.current,
  });

  const hendleCetak = async (no_pengiriman) => {
    dispatch(
      cetakPengiriman(no_pengiriman, (err, data) => {
        setItemCetak(data);
        handlePrint();
      })
    );
  };

  const columns = [
    {
      name: "NO PENGIRIMAN",
      selector: "no_pengiriman",
      sortable: true,
      width: "200px",
    },
    {
      name: "TANGGL",
      selector: "tgl_pengiriman",
      sortable: true,
      width: "120px",
      cell: (data) => moment(data.tgl_pengiriman).format("DD/MM/yyyy"),
    },

    {
      name: "NAMA TOKO",
      selector: "nama_toko",
      sortable: true,
      center: true,
    },

    {
      name: "JUMLAH DIKIRIM",
      selector: "jumlah_item",
      center: true,
    },

    {
      name: "JUMLAH DITERIMA",
      selector: "barang_diterima",
      center: true,
      cell: (data) => (data.status === "N" ? 0 : data.barang_diterima),
    },

    {
      name: "RETUR",
      selector: "barang_retur",
      center: true,
      width: "100px",
      cell: (data) => (data.status === "N" ? 0 : data.barang_retur),
    },

    {
      name: "STATUS",
      selector: "status",
      sortable: true,
      center: true,
      cell: (data) => (
        <Badge color={data.status === "N" ? "warning" : "success"}>
          {data.status === "N" ? <FaTruck /> : <FaRegCheckCircle />}
          {data.status === "N" ? " Pengiriman" : " Diterima"}
        </Badge>
      ),
    },

    {
      name: "NOTA",
      selector: "kategori",
      sortable: true,
      center: true,
      width: "120px",
      cell: (data) => (
        <>
          {data.status === "N" ? (
            <Button
              size="sm"
              color="info"
              onClick={() => handleToogle(data.no_pengiriman, data.id_toko)}
            >
              <IoReceiptOutline />
            </Button>
          ) : (
            <Button
              size="sm"
              color="info"
              onClick={() => hendleCetak(data.no_pengiriman)}
            >
              <IoPrint />
            </Button>
          )}
        </>
      ),
    },
  ];

  useEffect(() => {
    dispatch(getPengirimanByToko(currentUser.id_toko));
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Penerimaan Barang</h3>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        //   onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <DataTable
                    data={dataFaktur}
                    columns={columns}
                    pagination
                    noDataComponent={<NoDataBarang />}
                  />
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalPenerimaan
        modal={modal}
        toggle={toggle}
        data={itemPengiriman}
        toko={toko}
      />
      {/* <ModalPengiriman
        modal={modal}
        toggle={toggle}
        barang={dataBarang}
        count={dataFaktur.length + 1}
      /> */}

      <div style={{ display: "none" }}>
        <CetakFaktur ref={cetakRef} data={itemCetak} />
      </div>
    </>
  );
};

export default Penerimaan;
