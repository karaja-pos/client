import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Container,
  Row,
  Table,
  Button,
  CustomInput,
  Col,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  CardImg,
  CardTitle,
  CardSubtitle,
  CardText,
  Media,
  FormGroup,
  Label,
} from "reactstrap";
import DataTable from "react-data-table-component";
import convertRupiah from "rupiah-format";
import { IoEye, IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import moment from "moment";
import "react-tabs/style/react-tabs.css";
import { NoDataBarang, BarangComponent } from "utils/component";
import { getBarang } from "redux/actions/Barang";
import { getHistoryPenjualan } from "redux/actions/History";
import { getToko } from "redux/actions/Toko";
import ModalItem from "./ModalItem";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const HistoriPenjualan = () => {
  const dispatch = useDispatch();

  const dataBarang = useSelector((data) => data.barang.barang);
  const dataHistory = useSelector((data) => data.history.penjualan);
  const dataToko = useSelector((data) => data.toko.toko);
  const [cari, setCari] = useState("");
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemBarang, setItemBarang] = useState(null);
  const [toko, setToko] = useState(0);
  const [mulai, setMulai] = useState("");
  const [end, setEnd] = useState("");
  const [jenis, setJenis] = useState("");

  const toggle = () => setModal(!modal);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);

  const filterBarang = dataBarang.filter((data) => {
    return (
      data.nama_brg.toLowerCase().indexOf(cari) !== -1 ||
      data.kode_brg.toLowerCase().indexOf(cari) !== -1
    );
  });

  const columns = [
    {
      name: "NO",
      selector: "kode_brg",
      sortable: true,
      width: "50px",
      cell: (item, i) => i + 1,
    },
    {
      name: "NOMOR TRANSAKSI",
      selector: "no_penjualan",
      sortable: true,
      // width: "300px",
    },
    {
      name: "TANGGAL TRANSAKSI",
      selector: "tgl_penjualan",
      sortable: true,
      center: true,
      cell: (item) => moment(item.tgl_penjualan).format("MM-DD-yyyy"),
    },
    {
      name: "JUMLAH ITEM",
      selector: "total_item",
      sortable: true,
      center: true,
      width: "200px",
    },
    {
      name: "JENIS BAYAR",
      selector: "merek",
      sortable: true,
      width: "200px",
      cell: (item) => (item.jenis_bayar === 1 ? "TUNAI" : "NON TUNAI"),
    },
    {
      name: "TOTAL",
      selector: "total_harga",
      right: true,
      cell: (item) => convertRupiah.convert(item.total_harga),
    },

    {
      name: "AKSI",
      selector: "aksi",
      width: "50px",
      center: true,
      cell: (item) => (
        <Button
          color="info"
          size="sm"
          onClick={() => {
            setItemUpdate(item.no_penjualan);
            toggle();
          }}
        >
          <IoEye />
        </Button>
      ),
    },
  ];

  const setItemUpdate = (data) => {
    setItemBarang(data);
    toggleUpdate();
  };

  const handleCari = () => {
    dispatch(getHistoryPenjualan(toko, mulai, end, jenis));
  };

  useEffect(() => {
    dispatch(getBarang());
    dispatch(getToko());
  }, []);

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                {/* <h3 className="mb-0">Informasi Toko</h3> */}
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Data Histori Penjualan</h3>
                  </div>
                  {/* <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div> */}
                </Row>
              </CardHeader>
              <CardBody>
                <Row style={{ marginBottom: 20 }}>
                  <Col>
                    <Input
                      type="select"
                      name="select"
                      id="exampleSelect"
                      value={toko}
                      onChange={(e) => setToko(e.target.value)}
                    >
                      <option value="0">-- Pilih Toko --</option>
                      {dataToko.map((x) => (
                        <option key={x.id} value={x.id}>
                          {x.nama_toko}
                        </option>
                      ))}
                    </Input>
                  </Col>
                  <Col>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <span style={{ marginRight: 10 }}>Proide</span>
                      <Input
                        type="date"
                        value={mulai}
                        onChange={(e) => setMulai(e.target.value)}
                      />
                    </div>
                  </Col>
                  <Col>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <span style={{ marginRight: 10 }}>S/D</span>
                      <Input
                        type="date"
                        value={end}
                        onChange={(e) => setEnd(e.target.value)}
                      />
                    </div>
                  </Col>
                  <Col>
                    <Input
                      type="select"
                      name="select"
                      id="exampleSelect"
                      value={jenis}
                      onChange={(e) => setJenis(e.target.value)}
                    >
                      <option value="0">-- Pilih Jenis --</option>
                      <option value="TUNAI">TUNAI</option>
                      <option value="NONTUNAI">NON TUNAI</option>
                    </Input>
                  </Col>

                  <Col xs={1} style={{ margin: 0, padding: 0 }}>
                    <div
                      style={{
                        padding: 5,
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Button
                        color="info"
                        size="sm"
                        style={{ padding: 10, marginTop: -3 }}
                        onClick={() => handleCari()}
                      >
                        Cari
                      </Button>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <DataTable
                      className="align-items-center table-flush"
                      columns={columns}
                      data={dataHistory}
                      responsive={true}
                      striped={true}
                      direction="ltr"
                      noHeader={true}
                      // expandableRows
                      // expandOnRowClicked={true}

                      fixedHeader={true}
                      pagination={true}
                      highlightOnHover
                      noDataComponent={<NoDataBarang />}
                      expandableRowsComponent={<BarangComponent />}
                      style={{
                        headCells: {
                          style: {
                            backgroundColor: "#f6f9fc",
                            color: "#8898aa",
                          },
                        },
                      }}
                    />
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalItem modal={modal} toggle={toggle} item={itemBarang} />
    </>
  );
};

export default HistoriPenjualan;
