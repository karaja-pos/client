import axios from "axios";
import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import {
  Button,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import convertRupiah from "rupiah-format";
import { BASE_URL } from "utils";
import { NoDataBarang } from "utils/component";

const ModalItem = ({ modal, toggle, item }) => {
  const [data, setData] = useState([]);
  const columns = [
    {
      name: "NO",
      selector: "kode_brg",
      sortable: true,
      width: "50px",
      cell: (item, i) => i + 1,
    },
    {
      name: "NAMA BARANG",
      selector: "nama_brg",
      sortable: true,
      width: "200px",
    },
    {
      name: "TOTAL ITEM",
      selector: "jumlah_item",
      sortable: true,
      width: "115px",
      center: true,
    },
    {
      name: "DISKON",
      selector: "diskon",
      sortable: true,
      width: "90px",
      center: true,
      cell: (item) => `${item.diskon}%`,
    },
    {
      name: "HARGA",
      selector: "harga_diskon",
      sortable: true,
      // width: "300px",
      right: true,
      cell: (item) => convertRupiah.convert(item.harga_diskon),
    },
    {
      name: "TOTAL HARGA",
      selector: "harga_diskon",
      sortable: true,
      // width: "300px",
      right: true,
      cell: (item) =>
        convertRupiah.convert(item.harga_diskon * item.jumlah_item),
    },
  ];
  useEffect(() => {
    axios
      .get(`${BASE_URL}/history/transaksi/${item}`)
      .then((res) => {
        if (res.data.status === "Success") {
          setData(res.data.data);
        }
      })
      .catch((err) => {
        setData([]);
      });
  }, [item]);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      size="lg"
      centered={true}
    >
      <ModalHeader toggle={() => toggle()}>
        <h5 className="modal-title">Item Transaksi</h5>
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col>
            <DataTable
              className="align-items-center table-flush"
              columns={columns}
              data={data}
              responsive={true}
              striped={true}
              direction="ltr"
              noHeader={true}
              fixedHeader={true}
              pagination={true}
              highlightOnHover
              noDataComponent={<NoDataBarang />}
            />
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={() => toggle()} size="sm">
          Kembali
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalItem;
