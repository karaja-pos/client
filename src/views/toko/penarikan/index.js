import Header from "components/Headers/Header";
import moment from "moment";
import { useEffect } from "react";
import DataTable from "react-data-table-component";
import { IoSearchSharp } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import {
  Card,
  CardHeader,
  Container,
  Row,
  CardBody,
  Col,
  Button,
  Badge,
} from "reactstrap";
import { handleSetuju } from "redux/actions/Penarikan";
import { tolakPenarikan } from "redux/actions/Penarikan";
import { getPenarikan } from "redux/actions/Penarikan";

const Penarikan = () => {
  const dispatch = useDispatch();
  const dataPenarikan = useSelector((data) => data.penarikan.data);

  const handleTerima = (id) => {
    dispatch(handleSetuju(id));
  };

  const handleTolak = (id) => {
    dispatch(tolakPenarikan(id));
  };

  const colums = [
    {
      name: "NO",
      width: "50px",
      cell: (data, i) => i + 1,
    },

    {
      name: "TANGGAL PENARIKAN",
      selector: "tgl_penarikan",
      sortable: true,
      width: "100px",
      cell: (data) => moment(data.tgl_penarikan).format("MM-DD-yyyy"),
    },

    {
      name: "NAMA BARANG",
      selector: "nama_brg",
      sortable: true,
    },

    {
      name: "TOKO ASAL",
      // selector: "toko",
      sortable: true,
      cell: (data) => data.toko_asal.nama_toko,
    },
    {
      name: "TOKO TUJUAN",
      // selector: "toko",
      sortable: true,
      cell: (data) => data.toko_tujuan.nama_toko,
    },

    {
      name: "AKSI",
      // selector: "toko",
      center: true,
      width: "200px",
      cell: (data) => (
        <>
          {data.status === 1 && (
            <>
              <Button
                size="sm"
                color="success"
                onClick={() => handleTerima(data.id)}
              >
                Setuju
              </Button>
              <Button
                size="sm"
                color="danger"
                onClick={() => handleTolak(data.id)}
              >
                Tolak
              </Button>
            </>
          )}

          {data.status === 2 && <Badge color="success">Disetujui</Badge>}

          {data.status === 3 && <Badge color="danger">Ditolak</Badge>}
        </>
      ),
    },
  ];

  useEffect(() => {
    dispatch(getPenarikan());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Penarikan Barang</h3>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <DataTable
                      columns={colums}
                      data={dataPenarikan}
                      noHeader
                      noDataComponent={<p>Belum ada data</p>}
                    />
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Penarikan;
