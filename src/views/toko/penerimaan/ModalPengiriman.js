import React, { useEffect, useRef, useState, use } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Badge,
  Button,
  Col,
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
} from "reactstrap";
import moment from "moment";
import { IoAddCircleOutline, IoTrashOutline } from "react-icons/io5";
import { Typeahead } from "react-bootstrap-typeahead";
import ReactToPrint, { useReactToPrint } from "react-to-print";
import TimeInput from "react-input-time";

import { simpanFakturPembelian } from "redux/actions/Gudang";
import { getToko } from "redux/actions/Toko";
import { useFocus } from "utils/hooks";
import { kirimBarang } from "redux/actions/Gudang";
import Swal from "sweetalert2";
import { getBarang } from "redux/actions/Barang";
import { setKodeKaryawan } from "utils";
import CetakFaktur from "./CetakFaktur";

const ModalPengiriman = ({ modal, toggle, barang, count }) => {
  const [filterBy, setFilterBy] = useState("callback");
  const filterByFields = ["nama_brg", "kode_brg"];
  const dispatch = useDispatch();
  const [jumlahRef, setJumlahFocus] = useFocus();
  const selectRef = useRef();
  const dataToko = useSelector((data) => data.toko.toko);
  const [jumlah, setJumlah] = useState("");
  const [tampung, setTampung] = useState([]);
  const [selected, setSelected] = useState(null);
  const cetakRef = useRef();

  // data pengiriman
  const [noPengiriman, setNoPengiriman] = useState("");
  const [tglPengiriman, setTglPengiriman] = useState("");
  const [toko, setToko] = useState(0);
  const [jam, setJam] = useState("");

  const [noPengirimanErr, setNoPengirimanErr] = useState(false);
  const [tglPengirimanErr, setTglPengirimanErr] = useState(false);
  const [tokoErr, setTokoErr] = useState(false);
  const [jamErr, setJamErr] = useState(false);
  const [dataCetak, setDataCetak] = useState([]);

  const handleChangeBarang = (data) => {
    if (data.length > 0) {
      if (data[0].stok_akhir === 0) {
        Swal.fire({
          icon: "error",
          title: "Stok Kosong",
          text: `Barang ${data[0].nama_brg} kosong`,
        });
      } else {
        setSelected(data[0]);
        setJumlahFocus();
      }
    }
  };

  const handleAddBarang = () => {
    if (jumlah == 0) {
      return Swal.fire({
        icon: "error",
        title: "Inputan Jumlah",
        text: `Jumlah masih 0`,
      });
    }
    let stok = selected.stok_akhir - jumlah;
    if (stok < 0) {
      Swal.fire({
        icon: "error",
        title: "Stok tidak cukup",
        text: `Stok ${selected.nama_brg} adalah ${selected.stok_akhir} `,
      });
    } else {
      const data = tampung.find((x) => x.id === selected.id);
      if (!data) {
        setTampung([
          ...tampung,
          {
            id: selected.id,
            nama: selected.nama_brg,
            jumlah: parseInt(jumlah),
            satuan: selected.nama_satuan,
          },
        ]);
      } else {
        tampung.find((x) => {
          if (x.id === data.id) {
            let a = parseInt(x.jumlah) + parseInt(jumlah);
            if (selected.stok_akhir < a) {
              Swal.fire({
                icon: "error",
                title: "Stok tidak cukup",
                text: `Stok ${selected.nama_brg} adalah ${
                  selected.stok_akhir - x.jumlah
                } `,
              });
            } else {
              x.jumlah = a;
            }
          }
        });
      }
      clearInput();
    }
  };

  const clearInput = () => {
    setSelected(null);
    setFilterBy(null);
    setJumlah("");
    selectRef.current.clear();
  };

  const handleClose = () => {
    toggle();
    setTampung([]);
    kosongkan();
  };

  const handleDelItem = (id) => {
    const data = [...tampung];
    data.splice(id, 1);
    setTampung(data);
  };

  const filterByCallback = (option, props) =>
    option.nama_brg.toLowerCase().indexOf(props.text.toLowerCase()) !== -1 ||
    option.kode_brg.toLowerCase().indexOf(props.text.toLowerCase()) !== -1;

  const handlePrint = useReactToPrint({
    content: () => cetakRef.current,
    onAfterPrint: () => {
      simpan();
    },
  });

  const simpan = () => {
    if (noPengiriman !== "" && tglPengiriman !== "" && toko !== 0) {
      const jumlahItem = tampung.reduce(
        (a, b) => (a = parseInt(a) + parseInt(b.jumlah)),
        0
      );
      const data = {
        no_pengiriman: noPengiriman,
        tgl_pengiriman: tglPengiriman,
        jam_pengiriman: jam,
        jumlah_item: jumlahItem,
        id_toko: toko,
        user: "",
      };
      // console.log(data, tampung);
      kosongkan();
      toggle();
      dispatch(kirimBarang(data, tampung));
      dispatch(getBarang());
      setTampung([]);
      setDataCetak(null);
    }
  };

  const handleSetToko = (e) => {
    // const tokoArr = dataToko.find((x) => x.id == e.target.value);
    // console.log(tokoArr);
    setToko(e.target.value);
  };

  const handleSimpan = () => {
    if (toko == 0) {
      return Swal.fire({
        icon: "error",
        title: "Toko",
        text: `Toko belum dipilih`,
      });
    }
    if (tampung.length === 0) {
      Swal.fire({
        icon: "error",
        title: "Tidak ada barang",
        text: `Silahkan pilih barang yang akan dikirim`,
      });
    } else {
      noPengiriman === ""
        ? setNoPengirimanErr(true)
        : setNoPengirimanErr(false);
      tglPengiriman === ""
        ? setTglPengirimanErr(true)
        : setTglPengirimanErr(false);
      toko === 0 ? setTokoErr(true) : setTokoErr(false);

      const dataTokoArr = dataToko.find((x) => x.id == toko);
      const jumlahItem = tampung.reduce(
        (a, b) => (a = parseInt(a) + parseInt(b.jumlah)),
        0
      );
      setDataCetak([
        {
          no_pengiriman: noPengiriman,
          tgl_pengiriman: tglPengiriman,
          jam_pengiriman: jam,
          jumlah_item: jumlahItem,
          toko: dataTokoArr ? dataTokoArr.nama_toko : "",
          barang: tampung,
        },
      ]);

      Swal.fire({
        title: `Apakah anda ingin mencetak stuk pengiriman barang ini?`,
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonText: "Cetak",
        confirmButtonColor: "#2dce89",
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */

        if (result.isConfirmed) {
          handlePrint();
        } else {
          simpan();
        }
      });
    }
  };

  const kosongkan = () => {
    setNoPengiriman("");
    setTglPengiriman("");
    setToko(0);
    setJam("");
  };

  useEffect(() => {
    setJam(moment().format("HH:mm"));
    setTglPengiriman(moment().format("yyyy-MM-DD"));
    dispatch(getToko());
    setNoPengiriman(
      `${setKodeKaryawan(count)}${moment().format("DDMMYYHHmmss")}`
    );
  }, [modal]);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      size="xl"
      centered={true}
    >
      <ModalHeader slot="header">
        <h5 className="modal-title" id="exampleModalLabel">
          Pengiriman Barang
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row className="mb-4">
          <Col md={4}>
            <FormGroup row>
              <Label for="faktur" sm={4} size="sm">
                No Pengiriman
              </Label>
              <Col sm={8}>
                <Input
                  invalid={noPengirimanErr}
                  value={noPengiriman}
                  onChange={(e) => setNoPengiriman(e.target.value)}
                  size="sm"
                  type="text"
                  name="pengiriman"
                  id="pengiriman"
                  placeholder="No pengiriman"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="tanggal" sm={4} size="sm">
                Tgl. Pengiriman
              </Label>
              <Col sm={8}>
                <Input
                  invalid={tglPengirimanErr}
                  value={tglPengiriman}
                  defaultValue={moment().format("mm/dd/yyyy")}
                  onChange={(e) => setTglPengiriman(e.target.value)}
                  size="sm"
                  type="date"
                  name="date"
                  id="exampleDate"
                  placeholder="date placehold.lengther"
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="exampleSelect" sm={4} size="sm">
                Toko Tujuan
              </Label>
              <Col sm={8}>
                <Input
                  invalid={tokoErr}
                  value={toko}
                  onChange={(e) => handleSetToko(e)}
                  type="select"
                  name="select"
                  id="exampleSelect"
                  size="sm"
                >
                  <option value={0}>-- pilih toko --</option>
                  {dataToko.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.nama_toko}
                    </option>
                  ))}
                </Input>
              </Col>
            </FormGroup>
          </Col>
          <Col md={4} />
          <Col md={4}>
            <FormGroup row>
              <Label for="exampleSelect" sm={4} size="sm">
                Jam Pengiriman
              </Label>
              <Col sm={8}>
                <TimeInput
                  className="input-time form-control form-control-sm"
                  value={jam}
                  defaultValue={moment().format("HH:mm")}
                  id="example-time-input"
                  initialTime={moment().format("HH:mm")}
                  onChange={(e) => setJam(e.target.value)}
                />
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={8}>
            <Label size="sm">Barang</Label>
            <Typeahead
              id="input-barang"
              size="sm"
              ref={selectRef}
              filterBy={
                filterBy === "callback" ? filterByCallback : filterByFields
              }
              labelKey="nama_brg"
              onChange={(e) => handleChangeBarang(e)}
              options={barang}
              placeholder="Nama Barang / Kode"
              renderMenuItemChildren={(option) => (
                <div>
                  <strong>
                    {option.nama_brg}{" "}
                    {option.stok_akhir === 0 ? (
                      <Badge color="danger">Stok {option.stok_akhir}</Badge>
                    ) : (
                      <Badge color="success">Stok {option.stok_akhir}</Badge>
                    )}
                  </strong>
                  <div>
                    <small>{option.kode_brg}</small>
                  </div>
                </div>
              )}
            />
          </Col>
          <Col md={1}>
            <Label size="sm">Jumlah</Label>
            <input
              className="form-control form-control-sm"
              ref={jumlahRef}
              type="number"
              min="0"
              value={jumlah}
              onChange={(e) => setJumlah(e.target.value)}
              placeholder="0"
            />
            {/* <Input
              type="number"
              min="0"
              // ref={jumlahRef}
              value={jumlah}
              onChange={(e) => setJumlah(e.target.value)}
              placeholder="0"
              size="sm"
            /> */}
          </Col>
          <Col md={1}>
            <Label size="sm">Satuan</Label>
            <Input
              type="text"
              value={selected && selected !== null ? selected.nama_satuan : ""}
              placeholder=""
              size="sm"
              readOnly
            />
          </Col>

          <Col md={2}>
            <Label size="sm" className="text-center">
              tekan [Enter]
            </Label>

            <br />
            <Button
              size="sm"
              color="success"
              block
              disabled={selected ? false : true}
              onClick={handleAddBarang}
              onKeyPress={(e) => console.log(e.key)}
            >
              <IoAddCircleOutline /> Add
            </Button>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <Table
              className="align-items-center table-flush"
              responsive
              hover
              size="sm"
            >
              <thead className="thead-dark text-center">
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Barang</th>
                  <th scope="col">Jumlah</th>
                  <th scope="col">Satuan</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tfoot className="thead-dark text-center">
                <tr style={{ fontWeight: "bold" }}>
                  <th scope="col" className="text-center" colSpan={2}>
                    TOTAL
                  </th>
                  <th scope="col">
                    {tampung.reduce(
                      (a, b) => (a = parseInt(a) + parseInt(b.jumlah)),
                      0
                    )}
                  </th>
                  <th scope="col"></th>
                  <th scope="col"></th>
                </tr>
              </tfoot>
              <tbody>
                {tampung.map((data, i) => (
                  <tr>
                    <td className="text-center">{i + 1}</td>
                    <td className="text-left">{data.nama}</td>
                    <td className="text-center">{data.jumlah}</td>
                    <td className="text-center">{data.satuan}</td>
                    <td className="text-center">
                      <Button
                        size="sm"
                        color="danger"
                        onClick={() => handleDelItem(i)}
                      >
                        <IoTrashOutline />
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
        <div style={{ display: "none" }}>
          {dataCetak && <CetakFaktur ref={cetakRef} data={dataCetak} />}
        </div>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={handleClose}>
          Batal
        </Button>
        <Button color="primary" onClick={handleSimpan}>
          Kirim
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalPengiriman;
