import React, { useEffect } from "react";
import DataTable from "react-data-table-component";
import { useDispatch } from "react-redux";
import {
  Button,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { getPengiriman } from "redux/actions/Gudang";
import { terimaPengirimanByToko } from "redux/actions/Gudang";
import Swal from "sweetalert2";
import { NoDataBarang } from "utils/component";

const ModalPenerimaan = ({ modal, toggle, data, toko }) => {
  const dispatch = useDispatch();
  const convertData = data.map((item) => {
    return {
      id: item.id,
      no_pengiriman: item.no_pengiriman,
      id_barang: item.id_barang,
      nama_brg: item.nama_brg,
      jumlah_item: item.jumlah_item,
      jumlah_diterima: item.jumlah_diterima,
      retur: item.retur,
      status: item.status,
      input_jumlah: item.jumlah_item,
      input_retur: item.retur,
    };
  });

  //   console.log(convertData);
  const jumlahChange = (id, value) => {
    const a = convertData.find((x) => x.id === id);
    let jml = parseInt(value.target.value);
    if (jml > a.jumlah_item) {
      a.input_jumlah = a.jumlah_item;
      value.target.value = a.jumlah_item;
      return Swal.fire({
        icon: "info",
        title: "Perhatian",
        text: `inputan jumlah diterima = ${jml} lebih besar dari barang yang dikirim = ${a.jumlah_item}, silahkan perbaiki inputan anda`,
      });
    }

    a.input_jumlah = jml;
  };

  const returChange = (id, value) => {
    const a = convertData.find((x) => x.id === id);
    let jml = parseInt(value.target.value);
    let check = a.input_jumlah + jml;
    if (check > a.jumlah_item) {
      a.input_retur = a.input_retur;
      value.target.value = a.input_retur;
      return Swal.fire({
        icon: "info",
        title: "Perhatian",
        text: `Pastikan jumlah retur + jumlah diterima = jumlah dikirim, silahkan perbaiki jumlah diterima dan retur!`,
      });
    }
    a.input_retur = jml;
  };

  const handleTerima = (items) => {
    dispatch(terimaPengirimanByToko(toko, items));
  };

  const columns = [
    {
      name: "NO",
      selector: "no_pengiriman",
      width: "160px",
    },

    {
      name: "NAMA BARANG",
      selector: "nama_brg",
      sortable: true,
    },

    {
      name: "JUMLAH DIKIRIM",
      selector: "jumlah_item",
      center: true,
      width: "150px",
    },

    {
      name: "JUMLAH DITERIMA",
      selector: "jumlah_diterima",
      center: true,
      width: "200px",
      cell: (data) => (
        <Input
          //   className="form-control form-control-sm"
          defaultValue={data.input_jumlah}
          onChange={(e) => jumlahChange(data.id, e)}
          type="number"
          size="sm"
          placeholder="0"
          min="0"
        />
      ),
    },

    {
      name: "RETUR",
      selector: "retur",
      sortable: true,
      center: true,
      width: "100px",
      cell: (data) => (
        <Input
          //   className="form-control form-control-sm"
          defaultValue={data.retur}
          onChange={(e) => returChange(data.id, e)}
          type="number"
          size="sm"
          placeholder="0"
          min="0"
        />
      ),
    },

    {
      name: "AKSI",
      selector: "status",
      sortable: true,
      center: true,
      width: "100px",
      cell: (data) => (
        <Button size="sm" color="success" onClick={() => handleTerima(data)}>
          Terima
        </Button>
      ),
    },
  ];

  useEffect(() => {}, [modal]);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      size="xl"
      centered={true}
    >
      <ModalHeader>
        <h5 className="modal-title" id="exampleModalLabel">
          Detail Barang
        </h5>
      </ModalHeader>
      <ModalBody>
        <DataTable
          columns={columns}
          data={convertData}
          noHeader
          noDataComponent={<NoDataBarang />}
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggle}>
          Keluar
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalPenerimaan;
