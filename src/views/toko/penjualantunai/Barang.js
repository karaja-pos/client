import { useEffect, useState } from "react";
import CustomScroll from "react-custom-scroll";
import { IoBagHandleOutline, IoCartOutline, IoQrCode } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import {
  Badge,
  Card,
  CardBody,
  CardHeader,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Button,
} from "reactstrap";
// import v from ""
import convertRupiah from "rupiah-format";
import { getBarangToko } from "redux/actions/Kasir";
import { getToko } from "redux/actions/Toko";
import FOTO_BRG from "../../../assets/img/no_photo.jpg";
import { ADD_CART } from "redux/actions";
import { SELECT_BARANG, CART_TOTAL } from "redux/actions";
import DataTable from "react-data-table-component";
import { NoDataBarang } from "utils/component";

const Barang = ({ orderList, setOrderList, setIdToko }) => {
  const dispatch = useDispatch();
  const dataToko = useSelector((data) => data.toko.toko);
  const dataBarang = useSelector((data) => data.kasir.barang_kasir);
  const [toko, setToko] = useState(0);
  const [cari, setCari] = useState("");

  const handleChangeToko = (e) => {
    setIdToko(e.target.value);
    setToko(e.target.value);
    dispatch(getBarangToko(e.target.value));
  };

  const filterBarang = dataBarang.filter((data) => {
    return (
      data.nama_brg.toLowerCase().indexOf(cari) !== -1 ||
      data.kode_brg.toLowerCase().indexOf(cari) !== -1
    );
  });

  const handleAddBarang = (data) => {
    const items = { ...data, jenis: "JUAL" };
    console.log(items);
    dispatch({ type: SELECT_BARANG, data: items });
    // dispatch({ type: CART_SUB_TOTAL });
    dispatch({ type: CART_TOTAL });
  };

  useEffect(() => {
    dispatch(getToko());
  }, []);

  const columns = [
    {
      name: "KODE BARANG",
      selector: "kode_brg",
      sortable: true,
      width: "150px",
      center: true,
    },
    {
      name: "NAMA BARANG",
      selector: "nama_brg",
      sortable: true,
    },

    {
      name: "STOK",
      selector: "stok_akhir",
      sortable: true,
      width: "100px",
      center: true,
    },

    {
      name: "HARGA SATUAN",
      selector: "harga_jual",
      sortable: true,
      width: "200px",
      center: true,
      cell: (data) => (
        <div>
          <p style={{ fontSize: 12 }}>
            {convertRupiah.convert(data.harga_jual)}
          </p>
          <p style={{ fontSize: 12, lineHeight: 0 }}>Diskon {data.diskon}%</p>
        </div>
      ),
    },
    {
      name: "AKSI",
      sortable: true,
      width: "200px",
      center: true,
      cell: (data) => (
        <div>
          <Button size="sm" color="info" onClick={() => handleAddBarang(data)}>
            <IoCartOutline size={20} style={{ marginRight: 10 }} />
            Add
          </Button>
        </div>
      ),
    },
  ];

  return (
    <Card className="shadow" style={{ height: "75vh", maxHeight: "75vh" }}>
      <CardHeader>
        <div className="d-flex justify-content-between">
          <Col xs={12} md={6} className="" style={{ padding: 0 }}>
            <InputGroup>
              <Input type="text" onChange={(e) => setCari(e.target.value)} />
              <InputGroupAddon addonType="append">
                <InputGroupText>
                  <IoQrCode className="text-info" />
                </InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </Col>
          <Col xs={12} md={5}>
            <Input
              type="select"
              name="select"
              id="exampleSelect"
              value={toko}
              onChange={(e) => handleChangeToko(e)}
            >
              <option value="0">-- Pilih Toko --</option>
              {dataToko.map((item, i) => (
                <option key={i} value={item.id}>
                  {item.nama_toko}
                </option>
              ))}
            </Input>
          </Col>

          <div className="float-right">
            <IoBagHandleOutline size={30} />
          </div>
        </div>
      </CardHeader>
      <CardBody>
        <CustomScroll heightRelativeToParent="calc(100% - 10px)">
          <Row className="barang">
            <DataTable
              data={filterBarang}
              columns={columns}
              noHeader
              paginationPerPage={20}
              noDataComponent={<NoDataBarang />}
            />
            {/* {dataBarang.map((item) => (
              <Col xs={12} md={12} xl={3} className="mb-4" key={item}>
                <Card className="shadow pt-3" style={{ borderRadius: 15 }}>
                  <img
                    style={{
                      width: "100px",
                      margin: "0px auto",
                      objectFit: "cover",
                    }}
                    src={FOTO_BRG}
                    className="rounded-circle"
                  />
                  <CardBody>
                    <CardTitle
                      style={{ fontWeight: "bold", textAlign: "center" }}
                    >
                      {item.nama_brg}
                    </CardTitle>
                    <div
                      style={{
                        display: "flex",
                        fontSize: 12,
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <div>
                        stok:{" "}
                        <span style={{ fontWeight: "bold" }}>
                          {item.stok_akhir}
                        </span>
                      </div>
                      <div
                        style={{ fontWeight: "bold", fontSize: 14 }}
                        className="text-info"
                      >
                        {convertRupiah.convert(item.harga_jual)}
                      </div>
                    </div>
                    <CardText></CardText>
                    <Button
                      color="success"
                      block
                      className="mt-3"
                      onClick={() => handleAddBarang(item)}
                    >
                      <IoCartOutline /> Tambahkan
                    </Button>
                  </CardBody>
                </Card>
              </Col>
            ))} */}
          </Row>
        </CustomScroll>
      </CardBody>
    </Card>
  );
};

export default Barang;
