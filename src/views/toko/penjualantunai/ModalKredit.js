import {
  Button,
  Col,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import Select from "react-select";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getKreditur } from "redux/actions/Kreditur";
import moment from "moment";
import { transaksiPenjualan } from "redux/actions/Kasir";

const ModalKredit = ({
  modal,
  toggle,
  togglePilih,
  idToko,
  setIdToko,
  tanggal,
}) => {
  const dispatch = useDispatch();
  const dataKreditur = useSelector((data) => data.kreditur.kreditur);
  const order_list = useSelector((data) => data.kasir.order_list);
  const TOTAL = useSelector((data) => data.kasir.harga_total_cart);
  const SUB_TOTAL = useSelector((data) => data.kasir.harga_sub_total_cart);
  const HARGA_DISKON = useSelector((data) => data.kasir.harga_diskon);
  const TOTAL_ITEM = useSelector((data) => data.kasir.total_item);

  const [id, setId] = useState("");
  const [nama, setNama] = useState("");
  const [nomor, setNomor] = useState("");
  const [dp, setDp] = useState(0);

  const kreditArr = dataKreditur.map((x) => {
    return { value: x.id, label: x.nama_kreditur };
  });
  const handleKembali = () => {
    toggle();
    togglePilih();
  };

  const handleChange = (e) => {
    setId(e.value);
  };

  const handleProsses = () => {
    const data = {
      id_kasir: 0,
      id_toko: idToko,
      pajak: 0,
      no_penjualan: `KR0${idToko}${moment(Date.now()).format(
        "MMDDyyyyHHmmss"
      )}`,
      sub_total: SUB_TOTAL,
      total_diskon: HARGA_DISKON,
      total_harga: TOTAL,
      tgl_penjualan: tanggal,
      jam_penjualan: moment(Date.now()).format("HH:mm:ss"),
      barang: order_list,
      id_kreditur: parseInt(id),
      nama_pembeli: nama,
      nik: nomor,
      dp: dp,
      penjualan: "KREDIT",
      total_item: TOTAL_ITEM,
    };
    dispatch(transaksiPenjualan(data));
    toggle();
    kosong();
  };

  const kosong = () => {
    setId("");
    setNama("");
    setNomor("");
    // setIdToko(0);
  };

  useEffect(() => {
    dispatch(getKreditur());
  }, []);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      size="md"
      centered={true}
    >
      <ModalHeader toggle={() => handleKembali()}>
        <h5 className="modal-title">Formulir Pembayaran Kreditur</h5>
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col>
            <FormGroup>
              <Label>Pilih Kreditur</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                // defaultValue={colourOptions[0]}
                // isSearchable={isSearchable}
                onChange={(e) => handleChange(e)}
                options={kreditArr}
                name="kreditur"
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            <FormGroup>
              <Label for="nama">Nama Pembeli</Label>
              <Input
                type="text"
                name="nama"
                id="nama"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                autoComplete="off"
                placeholder="Masukan nama pembeli"
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            <FormGroup>
              <Label for="nomor">Nik / Identitas lainya</Label>
              <Input
                type="number"
                name="nomor"
                id="nomor"
                value={nomor}
                onChange={(e) => setNomor(e.target.value)}
                placeholder="Masukan nomor identitas"
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <FormGroup>
              <Label for="nomor">Uang Muka</Label>
              <Input
                type="number"
                name="dp"
                id="dp"
                value={dp}
                onChange={(e) => setDp(e.target.value)}
                placeholder="Masukan Uang Muka"
              />
            </FormGroup>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={() => handleKembali()}>
          Batal
        </Button>
        <Button color="success" onClick={() => handleProsses()}>
          Proses
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalKredit;
