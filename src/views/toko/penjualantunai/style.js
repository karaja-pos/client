import styled from "styled-components";

export const TableContainer = styled.div`
  width: 100%;
  font-size: 14px;
`;

export const BarangItems = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px 10px 20px 10px;
  border-bottom: 1px solid #ecf0f1;
  align-items: center;
`;

export const BarangNama = styled.div`
  p {
    line-height: 2px;
  }
`;
export const ButtonCount = styled.div``;
