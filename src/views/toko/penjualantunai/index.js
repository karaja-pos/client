import React, { useState } from "react";
import {
  IoAdd,
  IoCartOutline,
  IoRemove,
  IoSearchSharp,
  IoTrashOutline,
} from "react-icons/io5";
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Table,
} from "reactstrap";

import "./scroll.css";
import OrderItems from "./OrderItems";
import Barang from "./Barang";
import ModalPilihBayar from "./ModalPilihBayar";
import ModalKredit from "./ModalKredit";
import ModalNonTunai from "./ModalNonTunai";
import { updateExpressionWithTypeArguments } from "typescript";
import moment from "moment";

const PenjualanTunai = () => {
  const [orderList, setOrderList] = useState([]);
  const [modalPilih, setModalPilih] = useState(false);
  const [modalKredit, setModalKredit] = useState(false);
  const [modalNonTunai, setModalNonTunai] = useState(false);
  const [idToko, setIdToko] = useState(0);
  const [tanggal, setTanggal] = useState(
    moment(Date.now()).format("yyyy-MM-DD")
  );

  const handleTogglePilih = () => setModalPilih(!modalPilih);
  const handleKredit = () => setModalKredit(!modalKredit);
  const handleNonTunai = () => setModalNonTunai(!modalNonTunai);

  return (
    <>
      <div className="header bg-gradient-info pb-8 pt-5 pt-md-8"></div>
      <Container className="mt--8" fluid>
        {/* Table */}
        <Row>
          <Col md={8}>
            <Barang
              orderList={orderList}
              setOrderList={setOrderList}
              setIdToko={setIdToko}
            />
          </Col>
          <Col md={4}>
            <OrderItems
              togglePilih={handleTogglePilih}
              tanggal={tanggal}
              setTanggal={setTanggal}
            />
          </Col>
        </Row>
        <ModalPilihBayar
          modal={modalPilih}
          toggle={handleTogglePilih}
          toggleKredit={handleKredit}
          toggleNonTunai={handleNonTunai}
          idToko={idToko}
          setIdToko={setIdToko}
          tanggal={tanggal}
        />
        <ModalKredit
          modal={modalKredit}
          toggle={handleKredit}
          togglePilih={handleTogglePilih}
          idToko={idToko}
          setIdToko={setIdToko}
          tanggal={tanggal}
        />

        <ModalNonTunai
          modal={modalNonTunai}
          toggle={handleNonTunai}
          togglePilih={handleTogglePilih}
          idToko={idToko}
          setIdToko={setIdToko}
          tanggal={tanggal}
        />
      </Container>
    </>
  );
};

export default PenjualanTunai;
