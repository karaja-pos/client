import moment from "moment";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { transaksiPenjualan } from "redux/actions/Kasir";
import Swal from "sweetalert2";

const {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
} = require("reactstrap");

const ModalNonTunai = ({
  modal,
  toggle,
  togglePilih,
  idToko,
  setIdToko,
  tanggal,
}) => {
  const dispatch = useDispatch();
  const order_list = useSelector((data) => data.kasir.order_list);
  const TOTAL = useSelector((data) => data.kasir.harga_total_cart);
  const SUB_TOTAL = useSelector((data) => data.kasir.harga_sub_total_cart);
  const HARGA_DISKON = useSelector((data) => data.kasir.harga_diskon);
  const TOTAL_ITEM = useSelector((data) => data.kasir.total_item);

  const [rekTujuan, setRekTujuan] = useState("");
  const [rekPengirim, setRekPengirim] = useState("");
  const [namaPengirim, setNamaPengirim] = useState("");
  const [tglTransfer, setTglTransfer] = useState(
    moment(Date.now()).format("yyyy-MM-DD")
  );
  const [jamTransfer, setJamTransfer] = useState(
    moment(Date.now()).format("HH:mm:ss")
  );

  const handleProsses = () => {
    if (rekTujuan === "" || rekPengirim === "" || namaPengirim === "") {
      Swal.fire({
        icon: "error",
        title: "Formulir belum lengkap!",
        text: "Silahkan lengkapi formulir",
      });
    } else {
      const data = {
        id_kasir: 0,
        id_toko: idToko,
        pajak: 0,
        no_penjualan: `NT0${idToko}${moment(Date.now()).format(
          "MMDDyyyyHHmmss"
        )}`,
        sub_total: SUB_TOTAL,
        total_diskon: HARGA_DISKON,
        total_harga: TOTAL,
        tgl_penjualan: tanggal,
        jam_penjualan: moment(Date.now()).format("HH:mm:ss"),
        barang: order_list,
        nama_pembeli: namaPengirim,
        rek_tujuan: rekTujuan,
        rek_pengirim: rekPengirim,
        tgl_transfer: tglTransfer,
        jam_transfer: jamTransfer,
        jenis_bayar: 2,
        penjualan: "NONTUNAI",
        total_item: TOTAL_ITEM,
      };
      dispatch(transaksiPenjualan(data));
      toggle();
      kosong();
    }
  };

  const handleKembali = () => {
    toggle();
    togglePilih();
  };

  const kosong = () => {
    setRekTujuan("");
    setRekTujuan("");
    // setIdToko(0);
  };

  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      size="md"
      centered={true}
    >
      <ModalHeader toggle={() => handleKembali()}>
        <h5 className="modal-title">Formulir Pembayaran NonTunai</h5>
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col>
            <FormGroup>
              <Label for="rek_tujuan">Rekening Tujuan</Label>
              <Input
                type="number"
                name="rek_tujuan"
                id="rek_tujuan"
                autoComplete="off"
                value={rekTujuan}
                onChange={(e) => setRekTujuan(e.target.value)}
                placeholder="Masukan rekening tujuan"
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="rek_pengirim">Rekening Pengirim</Label>
              <Input
                type="number"
                name="rek_pengirim"
                id="rek_pengirim"
                autoComplete="off"
                value={rekPengirim}
                onChange={(e) => setRekPengirim(e.target.value)}
                placeholder="Masukan rekening pengirim"
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            <FormGroup>
              <Label for="nama_pengirim">Nama Pengirim</Label>
              <Input
                type="text"
                name="nama_pengirim"
                id="nama_pengirim"
                autoComplete="off"
                value={namaPengirim}
                onChange={(e) => setNamaPengirim(e.target.value)}
                placeholder="Masukan nama pengirim"
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <FormGroup>
              <Label for="rek_pengirim">Tanggal Transfer</Label>
              <Input
                type="date"
                name="rek_pengirim"
                id="rek_pengirim"
                autoComplete="off"
                value={tglTransfer}
                onChange={(e) => setTglTransfer(e.target.value)}
                placeholder="Masukan rekening pengirim"
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="rek_pengirim">Jam Transfer</Label>
              <Input
                type="time"
                name="rek_pengirim"
                id="rek_pengirim"
                autoComplete="off"
                value={jamTransfer}
                onChange={(e) => setJamTransfer(e.target.value)}
                placeholder="Masukan rekening pengirim"
              />
            </FormGroup>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={() => handleKembali()}>
          Batal
        </Button>
        <Button color="success" onClick={() => handleProsses()}>
          Proses
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalNonTunai;
