import { Button, Col, Modal, ModalBody, ModalHeader, Row } from "reactstrap";
import moment from "moment";
import Credit from "../../../assets/img/credit.svg";
import Tunai from "../../../assets/img/tunai.svg";
import NonTunai from "../../../assets/img/nontunai.svg";
import { useDispatch, useSelector } from "react-redux";
import { transaksiPenjualan } from "redux/actions/Kasir";

const ModalPilihBayar = ({
  modal,
  toggle,
  toggleKredit,
  toggleNonTunai,
  idToko,
  setIdToko,
  tanggal,
}) => {
  const dispatch = useDispatch();
  const order_list = useSelector((data) => data.kasir.order_list);
  const TOTAL = useSelector((data) => data.kasir.harga_total_cart);
  const SUB_TOTAL = useSelector((data) => data.kasir.harga_sub_total_cart);
  const HARGA_DISKON = useSelector((data) => data.kasir.harga_diskon);
  const TOTAL_ITEM = useSelector((data) => data.kasir.total_item);

  const handleKredit = () => {
    toggleKredit();
    toggle();
  };

  const handleNonTunai = () => {
    toggleNonTunai();
    toggle();
  };

  const handleTunai = () => {
    const data = {
      id_kasir: 0,
      id_toko: idToko,
      pajak: 0,
      no_penjualan: `0${idToko}${moment(Date.now()).format("MMDDyyyyHHmmss")}`,
      sub_total: SUB_TOTAL,
      total_diskon: HARGA_DISKON,
      total_harga: TOTAL,
      tgl_penjualan: tanggal,
      jam_penjualan: moment(Date.now()).format("HH:mm:ss"),
      barang: order_list,
      id_kreditur: "",
      nama_pembeli: "",
      nik: "",
      penjualan: "TUNAI",
      total_item: TOTAL_ITEM,
    };
    dispatch(transaksiPenjualan(data));
    toggle();
    // setIdToko(0);
  };

  //   id_kasir,
  //   id_toko,
  //   jam_penjualan,
  //   no_penjualan,
  //   pajak,
  //   tgl_penjualan,
  //   total_diskon,
  //   sub_total,
  //   total_harga,
  //   total_item,
  //   barang,
  //   penjualan,
  //   id_kreditur,
  //   nama_pembeli,
  //   nik,

  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      size="lg"
      centered={true}
    >
      <ModalHeader toggle={toggle}>
        <h5 className="modal-title">Silahkan pilih pembayaran</h5>
      </ModalHeader>

      <ModalBody>
        <Row>
          <Col>
            <div style={{ justifyItems: "center", textAlign: "center" }}>
              <img src={Credit} />
              <Button
                color="warning"
                style={{ marginTop: 40, marginBottom: 40, width: 200 }}
                onClick={() => handleKredit()}
              >
                Kredit
              </Button>
            </div>
          </Col>
          <Col>
            <div style={{ justifyItems: "center", textAlign: "center" }}>
              <img src={NonTunai} />
              <Button
                color="info"
                style={{ marginTop: 40, marginBottom: 40, width: 200 }}
                onClick={() => handleNonTunai()}
              >
                Non Tunai
              </Button>
            </div>
          </Col>
          <Col>
            <div style={{ justifyItems: "center", textAlign: "center" }}>
              <img src={Tunai} />
              <Button
                color="success"
                style={{ marginTop: 40, marginBottom: 40, width: 200 }}
                onClick={() => handleTunai()}
              >
                Tunai
              </Button>
            </div>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};

export default ModalPilihBayar;
