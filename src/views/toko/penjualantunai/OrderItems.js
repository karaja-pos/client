import moment from "moment";
import { useEffect, useState } from "react";
import CustomScroll from "react-custom-scroll";
import {
  IoAdd,
  IoCartOutline,
  IoRemove,
  IoTrashOutline,
} from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import {
  Card,
  Col,
  Row,
  Button,
  CardHeader,
  Badge,
  CardBody,
  Input,
  InputGroup,
} from "reactstrap";
import { ADD_CART, DEL_CART, CART_TOTAL } from "redux/actions";
// import ListBarang from "./ListBarang";
import convertRupiah from "rupiah-format";
import { BarangItems, BarangNama, ButtonCount, TableContainer } from "./style";

const OrderItems = ({ togglePilih, tanggal, setTanggal }) => {
  const dispatch = useDispatch();
  const order_list = useSelector((data) => data.kasir.order_list);
  const TOTAL = useSelector((data) => data.kasir.harga_total_cart);
  const SUB_TOTAL = useSelector((data) => data.kasir.harga_sub_total_cart);
  const HARGA_DISKON = useSelector((data) => data.kasir.harga_diskon);

  // const TOTAL = order_list.reduce(
  //   (a, b) => (a = parseInt(a) + parseInt(b.hargaTotal)),
  //   0
  // );

  const handleQtyChange = (data, e) => {
    dispatch({
      type: ADD_CART,
      data: data,
      qty: e.target.value,
    });
    // dispatch({ type: CART_SUB_TOTAL });
    dispatch({ type: CART_TOTAL });
  };

  // const increment = (data) => {
  //   dispatch({ type: ADD_CART, data: data });
  // };
  const decrement = (data) => {};
  const deleteItems = (data) => {
    dispatch({ type: DEL_CART, data: data });
    // dispatch({ type: CART_SUB_TOTAL });
    dispatch({ type: CART_TOTAL });
  };

  const handleHapusOrder = (items) => {};

  return (
    <Card className="shadow" style={{ height: "75vh", maxHeight: "75vh" }}>
      <CardHeader>
        <div className="d-flex justify-content-between">
          <div>
            <InputGroup>
              <Input
                type="date"
                value={tanggal}
                onChange={(e) => setTanggal(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="float-right" style={{ position: "relative" }}>
            <IoCartOutline size={30} />
            <Badge
              color="danger"
              style={{ position: "absolute", top: -10, right: -15 }}
            >
              {order_list.length}
            </Badge>
          </div>
        </div>
      </CardHeader>
      <CardBody style={{ position: "relative" }}>
        <CustomScroll heightRelativeToParent="calc(100% - 200px)">
          <Row
            className="order"
            style={{
              width: "100%",
              paddingLeft: 10,
              paddingBottom: 20,
            }}
          >
            <TableContainer>
              {order_list.map((data) => (
                <BarangItems key={data.id}>
                  <BarangNama>
                    <p style={{ fontWeight: "bold" }}>{data.nama_brg}</p>
                    <p style={{ fontSize: 12 }}>
                      {convertRupiah.convert(data.harga_jual)}
                    </p>
                    <p style={{ fontSize: 12 }}>Diskon {data.diskon}%</p>
                  </BarangNama>
                  <ButtonCount>
                    {/* {data.quantity === 1 ? (
                      <Button
                        size="sm"
                        color="danger"
                        onClick={() => deleteItems(data)}
                      >
                        <IoTrashOutline />
                      </Button>
                    ) : (
                      <Button size="sm" onClick={() => decrement(data)}>
                        <IoRemove />
                      </Button>
                    )} */}

                    {/* <span
                      style={{
                        fontSize: 18,
                        textAlign: "center",
                        paddingRight: 10,
                      }}
                    >
                      {data.quantity}
                    </span>
                    <Button size="sm" onClick={() => increment(data)}>
                      <IoAdd />
                    </Button> */}
                    <div style={{ display: "flex" }}>
                      <Input
                        size="sm"
                        type="number"
                        min="1"
                        max="999"
                        style={{
                          width: 80,
                          marginRight: 10,
                          textAlign: "center",
                        }}
                        defaultValue={data.quantity}
                        onChange={(e) => handleQtyChange(data, e)}
                      />
                      <Button
                        size="sm"
                        color="danger"
                        onClick={() => deleteItems(data)}
                      >
                        <IoTrashOutline />
                      </Button>
                    </div>
                  </ButtonCount>
                </BarangItems>
                // <ListBarang barang={data} handleHapusOrder={handleHapusOrder} />
              ))}
            </TableContainer>
          </Row>
        </CustomScroll>

        <div
          className="pay"
          style={{
            position: "sticky",
            left: 20,
            right: 20,
            bottom: 0,
          }}
        >
          <Row className="mb-2">
            <Col>
              <Card className="p-2 shadow">
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: 10,
                  }}
                >
                  <div style={{ fontSize: 12, fontWeight: "bold" }}>
                    SUB TOTAL
                  </div>
                  <div style={{ fontSize: 12, fontWeight: "bold" }}>
                    {convertRupiah.convert(SUB_TOTAL)}
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: 10,
                  }}
                >
                  <div style={{ fontSize: 12, fontWeight: "bold" }}>
                    TOTAL DISKON
                  </div>
                  <div style={{ fontSize: 12, fontWeight: "bold" }}>
                    {convertRupiah.convert(HARGA_DISKON)}
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: 10,
                  }}
                >
                  <div style={{ fontSize: 14, fontWeight: "bold" }}>TOTAL</div>
                  <div style={{ fontSize: 25, fontWeight: "bold" }}>
                    {convertRupiah.convert(TOTAL)}
                  </div>
                </div>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                block
                color="info"
                size="lg"
                onClick={togglePilih}
                disabled={order_list.length <= 0 ? true : false}
              >
                Bayar
              </Button>
            </Col>
          </Row>
        </div>
      </CardBody>
    </Card>
  );
};

export default OrderItems;
