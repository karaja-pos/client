import { useEffect, useState } from "react";
import { IoAdd, IoRemove, IoTrashOutline } from "react-icons/io5";
import { Button } from "reactstrap";
import { BarangItems, BarangNama, ButtonCount } from "./style";

const ListBarang = ({ handleQtyChange, deleteItems }) => {
  return (
    <BarangItems key={data.id}>
      <BarangNama>{data.nama_brg}</BarangNama>
      <ButtonCount>
        {/* {data.quantity === 1 ? (
                      <Button
                        size="sm"
                        color="danger"
                        onClick={() => deleteItems(data)}
                      >
                        <IoTrashOutline />
                      </Button>
                    ) : (
                      <Button size="sm" onClick={() => decrement(data)}>
                        <IoRemove />
                      </Button>
                    )} */}

        {/* <span
                      style={{
                        fontSize: 18,
                        textAlign: "center",
                        paddingRight: 10,
                      }}
                    >
                      {data.quantity}
                    </span>
                    <Button size="sm" onClick={() => increment(data)}>
                      <IoAdd />
                    </Button> */}
        <div style={{ display: "flex" }}>
          <Input
            size="sm"
            type="number"
            style={{ width: 100, marginRight: 10 }}
            defaultValue={data.quantity}
            onChange={(e) => handleQtyChange(data, e)}
          />
          <Button size="sm" color="danger" onClick={() => deleteItems(data)}>
            <IoTrashOutline />
          </Button>
        </div>
      </ButtonCount>
    </BarangItems>
  );
};

export default ListBarang;
