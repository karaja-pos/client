import axios from "axios";
import React, { useEffect, useState } from "react";
import { IoCheckmarkSharp, IoCloseSharp } from "react-icons/io5";
import { Badge, Card, CardBody, Row, Table } from "reactstrap";
import { BASE_URL } from "utils";

const ListBarang = ({ data }) => {
  const [barang, setBarang] = useState([]);
  useEffect(() => {
    axios
      .get(`${BASE_URL}/gudang/pengiriman/items/${data.no_pengiriman}`)
      .then((items) => {
        setBarang(items.data.data);
      });
  }, []);
  return (
    <Card className="shadow">
      <CardBody>
        <Row className="list-barng">
          <Table className="align-items-center table-flush" responsive>
            <thead className="thead-light">
              <tr style={{ textAlign: "center" }}>
                <th style={{ width: 50 }} scope="col">
                  No
                </th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Jumlah</th>
                <th scope="col">TERIMA</th>
              </tr>
            </thead>
            <tbody>
              {barang.map((item, i) => (
                <tr>
                  <td>{i + 1}</td>
                  <td style={{ textAlign: "left" }}>{item.nama_brg}</td>
                  <td style={{ textAlign: "center" }}>{item.jumlah_item}</td>
                  <td style={{ textAlign: "center" }}>
                    {item.status === "N" ? (
                      <Badge color="warning">
                        <IoCloseSharp />
                      </Badge>
                    ) : (
                      <Badge color="success">
                        <IoCheckmarkSharp />
                      </Badge>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Row>
      </CardBody>
    </Card>
  );
};

export default ListBarang;
