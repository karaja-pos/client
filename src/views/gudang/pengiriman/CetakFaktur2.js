import React from "react";
import moment from "moment";
import convertRupiah from "rupiah-format";

class CetakFaktur2 extends React.PureComponent {
  render() {
    console.log(this.props.data);
    return (
      <>
        {this.props.data.length !== 0 &&
          this.props.data.map((items) => (
            <div
              style={{
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30,
              }}
            >
              <p align="center" style={{ fontWeight: "bold" }}>
                <strong>FAKTUR PENGIRIMAN</strong>
              </p>
              <table width={671} border={0} style={{ marginBottom: 20 }}>
                <tbody>
                  <tr>
                    <td>NO. PENGIRIMAN </td>
                    <td>: {items.no_pengiriman} </td>
                    <td>TGL. PENGIRIMAN </td>
                    <td>
                      :{moment(items.tgl_pengiriman).format("MM/DD/YYYY")}
                    </td>
                  </tr>
                  <tr>
                    <td>TOKO TUJUAN </td>
                    <td>: {items.nama_toko} </td>

                    <td>JAM PENGIRIMAN </td>
                    <td>: {items.jam_pengiriman}</td>
                  </tr>
                </tbody>
              </table>
              <table width="100%">
                <thead>
                  <tr
                    style={{
                      borderBottom: "1px solid #7f8c8d",
                      borderTop: "1px solid #7f8c8d",
                      backgroundColor: "rgba(38,223,246, 1)",
                      color: "#576574",
                    }}
                  >
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>NO</strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>NAMA BARANG </strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>JUMLAH DIKIRIM </strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>JUMLAH DITERIMA </strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>RETUR</strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>KETERANGAN</strong>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {items.barang.map((item, i) => (
                    <tr
                      style={{
                        borderBottom: "1px solid #c8d6e5",
                        color: "#576574",
                      }}
                    >
                      <td
                        style={{
                          padding: "10px 0px 10px 0px",
                          textAlign: "center",
                        }}
                      >
                        {i + 1}
                      </td>
                      <td style={{ padding: "10px 0px 10px 0px" }}>
                        {item.nama_brg}
                      </td>
                      <td
                        style={{
                          padding: "10px 0px 10px 0px",
                          textAlign: "center",
                        }}
                      >
                        {item.jumlah_item}
                      </td>
                      <td
                        style={{
                          padding: "10px 0px 10px 0px",
                          textAlign: "center",
                        }}
                      >
                        {item.jumlah_diterima}
                      </td>
                      <td
                        style={{
                          padding: "10px 0px 10px 0px",
                          textAlign: "center",
                        }}
                      >
                        {item.retur}
                      </td>
                      <td style={{ padding: "10px 0px 10px 0px" }}></td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <table border={0} style={{ marginTop: 20, width: 300 }}>
                <tbody>
                  <tr>
                    <td style={{ padding: 5 }}>TOTAL ITEM</td>
                    <td>:</td>
                    <td style={{ padding: 5 }}>
                      <em>
                        <strong>{items.jumlah_item} Item </strong>
                      </em>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          ))}
      </>
    );
  }
}

export default CetakFaktur2;
