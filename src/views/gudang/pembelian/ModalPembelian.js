import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Col,
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
} from "reactstrap";
import moment from "moment";
import TimeInput from "react-input-time";
import { IoAddCircleOutline, IoTrashOutline } from "react-icons/io5";
import { Typeahead } from "react-bootstrap-typeahead";
import CurrencyInput, { formatValue } from "react-currency-input-field";
import convertRupiah from "rupiah-format";
import { setNoFaktur } from "../../../utils";
import { getSupplier } from "redux/actions/Supplier";
import { simpanFakturPembelian } from "redux/actions/Gudang";
import { useFocus } from "utils/hooks";

const ModalPembelian = ({ modal, toggle, barang, count }) => {
  const [filterBy, setFilterBy] = useState("callback");
  const filterByFields = ["nama_brg", "kode_brg"];
  const dispatch = useDispatch();
  // const jumlahRef = useRef();
  const [jumlahRef, setJumlahFocus] = useFocus();
  const selectRef = useRef();
  const dataSupplier = useSelector((data) => data.supplier.supplier);
  const [jumlah, setJumlah] = useState("");
  const [retur, setRetur] = useState(0);
  const [harga, setHarga] = useState(0);
  const [tampung, setTampung] = useState([]);
  const [selected, setSelected] = useState(null);

  // data faktur
  const [tglPenerimaan, setTglPenerimaan] = useState("");
  const [tglPenerimaanErr, setTglPenerimaanErr] = useState(false);
  const [jam, setJam] = useState("");
  const [jamErr, setJamErr] = useState("");
  const [noPenerimaan, setNoPenerimaan] = useState("");
  const [kodespl, setKodespl] = useState("");
  const [kodesplErr, setKodesplErr] = useState("Supplier");
  const [noFaktur, setnofaktur] = useState("");
  const [noFakturErr, setNoFakturErr] = useState(false);
  const [tglFaktur, setTglFaktur] = useState("");
  const [tglFakturErr, setTglFakturErr] = useState(false);

  const handleChangeBarang = (data) => {
    setSelected(data[0]);
    setJumlahFocus();
  };

  const handleAddBarang = () => {
    const data = tampung.find((x) => x.kode === selected.id);
    if (!data) {
      setTampung([
        ...tampung,
        {
          kode: selected.id,
          nama: selected.nama_brg,
          jumlah: jumlah,
          satuan: selected.nama_satuan,
          retur: retur,
          harga: harga,
          total: jumlah * harga,
          jumlah_item: jumlah - retur,
        },
      ]);
    } else {
      tampung.find((x) => {
        if (x.kode === data.kode) {
          let a = parseInt(x.jumlah) + parseInt(jumlah);
          let b = parseInt(x.retur) + parseInt(retur);
          x.jumlah = a;
          x.retur = b;
          x.total = a * x.harga;
          x.jumlah_item = x.jumlah_item - b;
        }
      });
    }

    kosongkan();
  };

  const kosongkanFaktur = () => {
    setJam("");
    setNoPenerimaan("");
    setKodespl("");
    setnofaktur("");
    setTglFaktur("");
    setTglPenerimaan("");
  };

  const kosongkan = () => {
    setHarga(0);
    setJumlah("");
    setRetur(0);
    setSelected(null);
    selectRef.current.clear();
  };

  const clearTampung = () => {
    setTampung([]);
  };

  const handleClose = () => {
    toggle();
    clearTampung();
    kosongkanFaktur();
    kosongkan();
  };

  const handleDelItem = (id) => {
    const data = [...tampung];
    data.splice(id, 1);
    setTampung(data);
  };

  const filterByCallback = (option, props) =>
    option.nama_brg.toLowerCase().indexOf(props.text.toLowerCase()) !== -1 ||
    option.kode_brg.toLowerCase().indexOf(props.text.toLowerCase()) !== -1;

  const handleSimpan = () => {
    noFaktur === "" ? setNoFakturErr(true) : setNoFakturErr(false);
    tglFaktur === "" ? setTglFakturErr(true) : setTglFakturErr(false);
    kodespl === 0 ? setKodesplErr(true) : setKodesplErr(false);
    tglPenerimaan === ""
      ? setTglPenerimaanErr(true)
      : setTglPenerimaanErr(false);
    jam === "" ? setJamErr(true) : setJamErr(false);
    if (
      noFaktur !== "" &&
      tglFaktur !== "" &&
      kodespl !== 0 &&
      tglPenerimaan !== "" &&
      jam !== ""
    ) {
      const data = {
        no_fak: noFaktur,
        id_supplier: kodespl,
        no_penerimaan: noPenerimaan,
        tgl_fak: tglFaktur,
        tgl_penerimaan: tglPenerimaan,
        jam: jam,
      };
      console.log(data);
      toggle();
      dispatch(simpanFakturPembelian(data, tampung));
      clearTampung();
      kosongkanFaktur();
      kosongkan();
    }
  };

  const handleChageSpl = (e) => {
    if (e.target.value !== 0) {
      setNoPenerimaan(
        `${setNoFaktur(count)}/${e.target.value}/${moment().format(
          "MM"
        )}/${moment().format("yyyy")}`
      );
    } else {
      setNoPenerimaan("");
    }

    setKodespl(e.target.value);
  };

  useEffect(() => {
    setJam(moment().format("HH:mm"));
    setTglPenerimaan(moment().format("yyyy-MM-DD"));
    setTglFaktur(moment().format("yyyy-MM-DD"));
    dispatch(getSupplier());
    setNoFaktur("");
  }, [modal]);
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      fade={true}
      backdrop="static"
      size="xl"
      // ref={modalRef}
      centered={true}
    >
      <ModalHeader slot="header">
        <h5 className="modal-title" id="exampleModalLabel">
          Faktur Pembelian
        </h5>
      </ModalHeader>
      <ModalBody>
        <Row className="mb-4">
          <Col md={4}>
            <FormGroup row>
              <Label for="faktur" sm={4} size="sm">
                No Faktur
              </Label>
              <Col sm={8}>
                <Input
                  invalid={noFakturErr}
                  value={noFaktur}
                  onChange={(e) => setnofaktur(e.target.value)}
                  size="sm"
                  type="text"
                  name="faktur"
                  id="faktur"
                  placeholder="No Faktur"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="tanggal" sm={4} size="sm">
                Tgl. Faktur
              </Label>
              <Col sm={8}>
                <Input
                  invalid={tglFakturErr}
                  value={tglFaktur}
                  onChange={(e) => setTglFaktur(e.target.value)}
                  size="sm"
                  type="date"
                  name="date"
                  id="exampleDate"
                  placeholder="date placeholder"
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="exampleSelect" sm={4} size="sm">
                Supplier
              </Label>
              <Col sm={8}>
                <Input
                  invalid={kodesplErr}
                  value={kodespl}
                  onChange={(e) => handleChageSpl(e)}
                  type="select"
                  name="select"
                  id="exampleSelect"
                  size="sm"
                >
                  <option value={0}>-- pilih supplier --</option>
                  {dataSupplier.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.nama_supplier}
                    </option>
                  ))}
                </Input>
              </Col>
            </FormGroup>
          </Col>
          <Col md={4} />
          <Col md={4}>
            <FormGroup row>
              <Label for="faktur" sm={4} size="sm">
                No Penerimaan
              </Label>
              <Col sm={8}>
                <Input
                  value={noPenerimaan}
                  size="sm"
                  type="faktur"
                  name="faktur"
                  id="faktur"
                  placeholder="No Faktur"
                  readOnly
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="tanggal" sm={4} size="sm">
                Tgl. Penerimaan
              </Label>
              <Col sm={8}>
                <Input
                  invalid={tglPenerimaanErr}
                  value={tglPenerimaan}
                  onChange={(e) => setTglPenerimaan(e.target.value)}
                  size="sm"
                  type="date"
                  name="date"
                  // id="exampleDate"
                  placeholder="date placeholder"
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="exampleSelect" sm={4} size="sm">
                Jam Penerimaan
              </Label>
              <Col sm={8}>
                <TimeInput
                  className="input-time form-control form-control-sm"
                  value={jam}
                  defaultValue={moment().format("HH:mm")}
                  id="example-time-input"
                  initialTime={moment().format("HH:mm")}
                  onChange={(e) => setJam(e.target.value)}
                />
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <Label size="sm">Barang</Label>
            {/* <Input type="text" placeholder="Nama Barang" size="sm" /> */}
            <Typeahead
              id="input-barang"
              size="sm"
              onFocus={() => setFilterBy("callback")}
              onBlur={() => setFilterBy("notcallback")}
              ref={selectRef}
              filterBy={
                filterBy === "callback" ? filterByCallback : filterByFields
              }
              labelKey="nama_brg"
              onChange={(e) => handleChangeBarang(e)}
              options={barang}
              placeholder="Nama Barang / Kode"
              renderMenuItemChildren={(option) => (
                <div>
                  {option.nama_brg}
                  <div>
                    <small>{option.kode_brg}</small>
                  </div>
                </div>
              )}
            />
          </Col>
          <Col md={1}>
            <Label size="sm">Jumlah</Label>
            <input
              className="form-control form-control-sm"
              ref={jumlahRef}
              type="number"
              min="0"
              value={jumlah}
              onChange={(e) => setJumlah(e.target.value)}
              placeholder="0"
            />
            {/* <Input
              type="number"
              min="0"
              ref={jumlahRef}
              value={jumlah}
              onChange={(e) => setJumlah(e.target.value)}
              placeholder="0"
              size="sm"
            /> */}
          </Col>
          <Col md={1}>
            <Label size="sm">Satuan</Label>
            <Input
              type="text"
              value={selected && selected !== null ? selected.nama_satuan : ""}
              placeholder=""
              size="sm"
              readOnly
            />
          </Col>
          <Col md={1}>
            <Label size="sm">Retur</Label>
            <Input
              value={retur}
              onChange={(e) => setRetur(e.target.value)}
              type="number"
              min="0"
              placeholder="0"
              size="sm"
            />
          </Col>
          {/* <Col md={1}>
            <Label size="sm">Diterima</Label>
            <Input type="number" min="0" placeholder="0" size="sm" />
          </Col> */}
          <Col md={3}>
            <Label size="sm">Harga Satuan</Label>
            <CurrencyInput
              id="input-example"
              name="input-name"
              className="form-control form-control-sm"
              placeholder="Rp.0"
              defaultValue={harga}
              prefix="Rp "
              value={harga}
              // decimalsLimit={2}
              onValueChange={(value, name) => setHarga(value)}
            />
            {/* <Input
              value={harga}
              onChange={(e) => setHarga(e.target.value)}
              type="number"
              min="0"
              placeholder="Rp.0"
              size="sm"
            /> */}
          </Col>
          <Col md={2}>
            <Label size="sm" className="text-center">
              tekan [Enter]
            </Label>

            <br />
            <Button
              size="sm"
              color="success"
              block
              disabled={selected ? false : true}
              onClick={handleAddBarang}
              onKeyPress={(e) => console.log(e.key)}
            >
              <IoAddCircleOutline /> Add
            </Button>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <Table
              className="align-items-center table-flush"
              responsive
              hover
              size="sm"
            >
              <thead className="thead-dark text-center">
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Barang</th>
                  <th scope="col">Jumlah</th>
                  <th scope="col">Satuan</th>
                  <th scope="col">Retur</th>
                  {/* <th scope="col">Jumlah Diterima</th> */}
                  <th scope="col">Harga satuan</th>
                  <th scope="col">Jumlah (Rp)</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tfoot className="thead-dark text-center">
                <tr style={{ fontWeight: "bold" }}>
                  <th scope="col" className="text-center" colSpan={2}>
                    TOTAL
                  </th>
                  <th scope="col">
                    {tampung.reduce(
                      (a, b) => (a = parseInt(a) + parseInt(b.jumlah)),
                      0
                    )}
                  </th>
                  <th scope="col"></th>
                  <th scope="col">
                    {tampung.reduce(
                      (a, b) => (a = parseInt(a) + parseInt(b.retur)),
                      0
                    )}
                  </th>
                  {/* <th scope="col">Jumlah Diterima</th> */}
                  <th scope="col" className="text-right">
                    {convertRupiah.convert(
                      tampung.reduce(
                        (a, b) => (a = parseInt(a) + parseInt(b.harga)),
                        0
                      )
                    )}
                  </th>
                  <th scope="col" className="text-right">
                    {convertRupiah.convert(
                      tampung.reduce(
                        (a, b) => (a = parseInt(a) + parseInt(b.total)),
                        0
                      )
                    )}
                  </th>
                  <th scope="col"></th>
                </tr>
              </tfoot>
              <tbody>
                {tampung.map((data, i) => (
                  <tr>
                    <td className="text-center">{i + 1}</td>
                    <td className="text-left">{data.nama}</td>
                    <td className="text-center">{data.jumlah}</td>
                    <td className="text-center">{data.satuan}</td>
                    <td className="text-center">{data.retur}</td>
                    <td className="text-right">
                      {convertRupiah.convert(parseInt(data.harga))}
                    </td>
                    <td className="text-right">
                      {convertRupiah.convert(data.total)}
                    </td>
                    <td className="text-center">
                      <Button
                        size="sm"
                        color="danger"
                        onClick={() => handleDelItem(i)}
                      >
                        <IoTrashOutline />
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter slot="footer">
        <Button color="secondary" onClick={handleClose}>
          Batal
        </Button>
        <Button color="primary" onClick={handleSimpan}>
          Tambahkan
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalPembelian;

// 001/KDSPL/01/2021
