import { Tooltip } from "bootstrap";
import React, { useState } from "react";
import { IoPencil, IoPrint, IoTrashOutline } from "react-icons/io5";
import { Button } from "reactstrap";

const ButtonTooltips = ({ color, size, children, message }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);

  const toggle = () => setTooltipOpen(!tooltipOpen);

  return (
    <>
      <Button size={size} color={color} id="TooltipExample">
        {children}
      </Button>
      {/* <Tooltip
        placement="top"
        isOpen={tooltipOpen}
        target="TooltipExample"
        toggle={toggle}
      >
        {message}
      </Tooltip> */}
    </>
  );
};

export default ButtonTooltips;
