import React from "react";
import moment from "moment";
import convertRupiah from "rupiah-format";

class CetakFaktur extends React.PureComponent {
  render() {
    return (
      <>
        {this.props.data.map((data) => (
          <>
            <div
              style={{
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 30,
              }}
            >
              <p align="center" style={{ fontWeight: "bold" }}>
                <strong>FAKTUR PEMBELIAN</strong>
              </p>
              <table width={671} border={0} style={{ marginBottom: 20 }}>
                <tbody>
                  <tr>
                    <td>NO. FAKTUR </td>
                    <td>: {data.no_faktur} </td>
                    <td>NO. PENERIMAAN </td>
                    <td>: {data.no_penerimaan} </td>
                  </tr>
                  <tr>
                    <td>TGL. FAKTUR </td>
                    <td>
                      : {moment(data.tanggal_faktur).format("MM/DD/YYYY")}{" "}
                    </td>
                    <td>TGL. PENERIMAAN </td>
                    <td>
                      : {moment(data.tanggal_penerimaan).format("MM/DD/YYYY")}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td>SUPLIER</td>
                    <td>: {data.nama_supplier} </td>
                    <td>JAM PENERIMAAN </td>
                    <td>: {moment(data.created_at).format("hh:mm:ss")} </td>
                  </tr>
                </tbody>
              </table>
              <table width="100%">
                <thead>
                  <tr
                    style={{
                      borderBottom: "1px solid #7f8c8d",
                      borderTop: "1px solid #7f8c8d",
                      backgroundColor: "rgba(38,223,246, 1)",
                      color: "#576574",
                    }}
                  >
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>NO</strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>NAMA BARANG </strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>JML. PEMBELIAN </strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>RETUR</strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>SATUAN</strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>HRG. SATUAN (Rp) </strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>JUMLAH (Rp) </strong>
                      </div>
                    </th>
                    <th style={{ padding: "10px 0px 10px 0px" }}>
                      <div align="center">
                        <strong>KETERANGAN</strong>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {data.items.map((item, i) => (
                    <tr
                      style={{
                        borderBottom: "1px solid #c8d6e5",
                        color: "#576574",
                        padding: "10px 0px 10px 0px",
                      }}
                      // style={{ padding: "10px 0px 10px 0px" }}
                    >
                      <td
                        style={{
                          textAlign: "center",
                          padding: "10px 0px 10px 0px",
                        }}
                      >
                        {i + 1}
                      </td>
                      <td
                        style={{
                          paddingLeft: 10,
                          padding: "10px 0px 10px 0px",
                        }}
                      >
                        {item.nama_brg}
                      </td>
                      <td
                        style={{
                          textAlign: "center",
                          padding: "10px 0px 10px 0px",
                        }}
                      >
                        {item.barang_diterima}
                      </td>
                      <td
                        style={{
                          textAlign: "center",
                          padding: "10px 0px 10px 0px",
                        }}
                      >
                        {item.barang_retur}
                      </td>
                      <td
                        style={{
                          textAlign: "center",
                          padding: "10px 0px 10px 0px",
                        }}
                      >
                        {item.nama_satuan}
                      </td>
                      <td
                        style={{
                          textAlign: "right",
                          paddingRight: 10,
                          padding: "10px 0px 10px 0px",
                        }}
                      >
                        {convertRupiah.convert(item.harga_beli)}
                      </td>
                      <td
                        style={{
                          textAlign: "right",
                          paddingRight: 10,
                          padding: "10px 0px 10px 0px",
                        }}
                      >
                        {convertRupiah.convert(item.jumlah_harga)}
                      </td>
                      <td style={{ padding: "10px 0px 10px 0px" }}>&nbsp;</td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <table border={0} style={{ marginTop: 20, width: 300 }}>
                <tbody>
                  <tr>
                    <td style={{ padding: 5 }}>TOTAL (Rp)</td>
                    <td>:</td>
                    <td style={{ paddingLeft: 10 }}>
                      <em>
                        <strong>
                          {convertRupiah.convert(data.total_harga_pembelian)}
                        </strong>
                      </em>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ padding: 5 }}>TOTAL ITEM</td>
                    <td>:</td>
                    <td style={{ padding: 5 }}>
                      <em>
                        <strong>{data.barang_diterima} Item </strong>
                      </em>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ padding: 5 }}>TOTAL RETUR</td>
                    <td>:</td>
                    <td style={{ padding: 5 }}>
                      <em>
                        <strong>{data.barang_retur} Item</strong>
                      </em>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </>
        ))}
      </>
    );
  }
}

export default CetakFaktur;
