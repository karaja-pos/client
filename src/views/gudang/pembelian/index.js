import React, { useEffect, useRef, useState } from "react";
import {
  IoSearchSharp,
  IoPrint,
  IoTrashOutline,
  IoPencil,
} from "react-icons/io5";
import moment from "moment";
import DataTable from "react-data-table-component";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Button,
} from "reactstrap";
import convertRupiah from "rupiah-format";
import ReactToPrint from "react-to-print";
import { NoDataBarang } from "utils/component";
import { useDispatch, useSelector } from "react-redux";
import { getFakturPembelian } from "redux/actions/Gudang";
import ModalPembelian from "./ModalPembelian";
import ButtonTooltips from "./ButtonTooltips";
import { getBarang } from "redux/actions/Barang";
import CetakFaktur from "./CetakFaktur";
import { getFakturByNomor } from "redux/actions/Gudang";
import { hapusFakturById } from "redux/actions/Gudang";
import Swal from "sweetalert2";

const Header = React.lazy(() => import("../../../components/Headers/Header"));

const Pembelian = () => {
  const dispatch = useDispatch();
  const cetakRef = useRef();
  const dataFaktur = useSelector((data) => data.gudang.faktur_pembelian);
  const cetakFaktur = useSelector((data) => data.gudang.cetak);
  const dataBarang = useSelector((data) => data.barang.barang);
  const [modal, setModal] = useState(false);

  // const handlePrint = useReactToPrint({
  //   content: () => cetakRef.current,
  //   onBeforePrint: () => {
  //     dispatch(getFakturByNomor());
  //   },
  // });

  const handleHapusFaktur = (no_faktur) => {
    Swal.fire({
      title: `Apakah anda ingin menghapus Nomor Faktur ${no_faktur} ?`,
      showCancelButton: true,
      cancelButtonText: "Batal",
      confirmButtonText: "Hapus",
      confirmButtonColor: "#e74c3c",
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        dispatch(hapusFakturById(no_faktur));
      }
    });
  };

  const toggle = () => setModal(!modal);
  const columns = [
    {
      name: "NO FAKTUR",
      selector: "no_faktur",
      sortable: true,
      width: "150px",
    },
    {
      name: "TANGGL",
      selector: "tanggal_faktur",
      sortable: true,
      width: "150px",
      cell: (data) => moment(data.tanggal_faktur).format("DD/MM/yyyy"),
    },
    {
      name: "BARANG DITERIMA",
      selector: "barang_diterima",
      sortable: true,
      center: true,
    },

    {
      name: "RETUR",
      selector: "barang_retur",
      sortable: true,
      center: true,
      width: "100px",
    },

    {
      name: "TOTAL PEMBELIAN",
      selector: "total_harga_pembelian",
      sortable: true,
      center: true,
      cell: (data) => convertRupiah.convert(data.total_harga_pembelian),
    },

    {
      name: "Aksi",
      selector: "kategori",
      sortable: true,
      center: true,
      width: "120px",
      cell: (data) => (
        <>
          <ReactToPrint
            trigger={() => (
              <Button size="sm" color="info">
                <IoPrint />
              </Button>
            )}
            content={() => cetakRef.current}
            // onBeforePrint={() => )}
            onBeforeGetContent={() =>
              dispatch(getFakturByNomor(data.no_faktur))
            }
          />

          <Button
            size="sm"
            color="danger"
            onClick={() => handleHapusFaktur(data.no_faktur)}
          >
            <IoTrashOutline />
          </Button>
        </>
      ),
    },
  ];

  useEffect(() => {
    dispatch(getFakturPembelian());
    dispatch(getBarang());
  }, []);
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Faktur Pembelian</h3>
                  </div>
                  <div className="col text-right">
                    <Button color="success" onClick={toggle}>
                      Tambah
                    </Button>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Row className="mb-3">
                  <Col xs={12} md={3} className="ml-auto">
                    <InputGroup>
                      <Input
                        type="text"
                        //   onChange={(e) => setCari(e.target.value)}
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <IoSearchSharp className="text-info" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
                <Row className="info-toko">
                  <DataTable
                    data={dataFaktur}
                    columns={columns}
                    pagination
                    noDataComponent={<NoDataBarang />}
                  />
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      <ModalPembelian
        modal={modal}
        toggle={toggle}
        barang={dataBarang}
        count={dataFaktur.length + 1}
      />

      <div style={{ display: "none" }}>
        <CetakFaktur ref={cetakRef} data={cetakFaktur} />
      </div>
    </>
  );
};

export default Pembelian;
