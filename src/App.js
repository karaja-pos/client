import React, { useEffect } from "react";
import { HashRouter, Switch } from "react-router-dom";
import useSound from "use-sound";
import { css } from "@emotion/react";
import BounceLoader from "react-spinners/BounceLoader";
import io from "socket.io-client";
import { SOCKET_URL } from "utils";
import { checkUser } from "redux/actions/Auth";
import { useDispatch } from "react-redux";
import jwt from "jsonwebtoken";
import { userLogOut } from "redux/actions/Auth";
import { getPenarikan } from "redux/actions/Penarikan";
import notif from "./assets/sound/notify.ogg";

const socket = io(SOCKET_URL);

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 99999999;
`;

const loading = (
  <BounceLoader css={override} color="#26dff6" size={150} loading={true} />
);

const DashboardRoute = React.lazy(() => import("./utils/DashboardRoute"));
const AuthRoute = React.lazy(() => import("./utils/AuthRoute"));
// const AdminLayout = React.lazy(() => import("./layouts/Admin"));
const AuthLayout = React.lazy(() => import("./layouts/Auth"));

function App() {
  const [play] = useSound(notif);
  const dispatch = useDispatch();

  if (localStorage.getItem("session")) {
    jwt.verify(localStorage.getItem("session"), "7qvt6t2738", (err, decode) => {
      if (err) {
        dispatch(userLogOut());
      } else {
        dispatch(checkUser(localStorage.getItem("session")));
      }
    });
  }
  useEffect(() => {
    if (localStorage.getItem("session")) {
      socket.on("penarikan-barang", () => {
        play();
        dispatch(getPenarikan());
      });
    }
    socket.on("kasir-login", () => {
      console.log("Kasir Login");
    });
  });
  return (
    <HashRouter>
      <React.Suspense fallback={loading}>
        <Switch>
          <AuthRoute path="/login" component={AuthLayout} />
          <DashboardRoute path="/" name="Home" />
        </Switch>
      </React.Suspense>
      <BounceLoader css={override} color="#26dff6" size={150} loading={false} />
    </HashRouter>
  );
}

export default App;

// 6962
