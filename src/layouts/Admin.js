import React, { Suspense, useEffect } from "react";
import { useLocation, Route, Switch, Redirect } from "react-router-dom";
// reactstrap components
import { Container } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";

// import { Offline, Online } from "react-detect-offline";
// import { FiWifi, FiWifiOff } from "react-icons/fi";
// core components
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import AdminFooter from "components/Footers/AdminFooter.js";
import Sidebar from "components/Sidebar/Sidebar.js";

import { routeAdmin, routeToko } from "../routes";
import { getToko } from "../redux/actions/Toko";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const Admin = (props) => {
  const mainContent = React.useRef(null);
  const location = useLocation();
  const dispatch = useDispatch();
  const toggle = useSelector((data) => data.app.sidebar);
  const currentUser = useSelector((data) => data.auth.currentUser);

  React.useEffect(() => {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    mainContent.current.scrollTop = 0;
  }, [location]);

  const getBrandText = (path) => {
    if (currentUser.role == "admin") {
      for (let i = 0; i < routeAdmin.length; i++) {
        if (
          props.location.pathname.indexOf(
            routeAdmin[i].layout + routeAdmin[i].path
          ) !== -1
        ) {
          return routeAdmin[i].name;
        }
      }
    }

    if (currentUser.role == "toko") {
      for (let i = 0; i < routeToko.length; i++) {
        if (
          props.location.pathname.indexOf(
            routeToko[i].layout + routeToko[i].path
          ) !== -1
        ) {
          return routeToko[i].name;
        }
      }
    }

    return "Brand";
  };

  useEffect(() => {
    dispatch(getToko());
  }, []);

  return (
    <>
      {toggle && (
        <>
          {currentUser.role == "admin" && (
            <Sidebar
              {...props}
              routes={routeAdmin}
              logo={{
                innerLink: "/admin/index",
                imgSrc: require("../assets/img/brand/karaja.png").default,
                imgAlt: "...",
              }}
            />
          )}

          {currentUser.role == "toko" && (
            <Sidebar
              {...props}
              routes={routeToko}
              logo={{
                innerLink: "/admin/index",
                imgSrc: require("../assets/img/brand/karaja.png").default,
                imgAlt: "...",
              }}
            />
          )}
        </>
      )}

      <div className="main-content" ref={mainContent}>
        <AdminNavbar
          {...props}
          brandText={getBrandText(props.location.pathname)}
        />

        <Switch>
          <Suspense fallback={loading}>
            {currentUser.role === "admin" && (
              <>
                {routeAdmin.map((route, idx) => (
                  <>
                    {route.layout === "/admin" && (
                      <Route
                        key={route.layout.id}
                        path={route.layout + route.path}
                        exact={route.exact}
                        name={route.name}
                        render={(props) => (
                          <>
                            <route.component {...props} />
                          </>
                        )}
                      />
                    )}
                  </>
                ))}
                <Redirect from="*" to="/admin/index" />
              </>
            )}

            {currentUser.role === "toko" && (
              <>
                {routeToko.map((route, idx) => (
                  <>
                    {route.layout === "/toko" && (
                      <Route
                        key={route.layout.id}
                        path={route.layout + route.path}
                        exact={route.exact}
                        name={route.name}
                        render={(props) => (
                          <>
                            <route.component {...props} />
                          </>
                        )}
                      />
                    )}
                  </>
                ))}
                <Redirect from="*" to="/toko/index" />
              </>
            )}

            {/* {getRoutes(routes)} */}
          </Suspense>
        </Switch>
        <Container fluid>
          <AdminFooter />
        </Container>
      </div>
      {/* <div style={{ position: "sticky", top: 0, left: 0 }}>
        <Online>
          <div className="icon icon-shape bg-success text-white rounded-circle shadow">
            <FiWifi />
          </div>
        </Online>
        <Offline>
          <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
            <FiWifiOff />
          </div>
        </Offline>
      </div> */}
    </>
  );
};

export default Admin;
